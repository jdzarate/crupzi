import datetime
import locale
import operator
import unicodedata
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson, timezone
from django.utils.encoding import smart_str, smart_unicode
from django.db.models import Q
from users.models import Profile, Country
from deals.models import Deal, DealResource, UserDeal, DealVisit, UserDealRemoved, DealBranch, ViewTopResults, ViewTodayResults, DealSaved, DealRemoved
from companies.models import Company, CompanyCategory, Mall, Branch, CompanyUser, CompanySubscriber
from resources.models import ResourceEntity, Resource
from crupzi.utils import get_schedule, common_list, calculate_age, iso_week_number
from deals.utils import save_visit, save_print, deal_status
from urlparse import urlparse


# tab1: top, tab2: today, tab3: all
@csrf_exempt
def get_public_data(request, category, tab):
	regs_limit = 70
	paginator_limit = 10
	user_country = request.GET.get('sel_country')
	deals_list = list()
	today_init = timezone.now().strftime("%Y-%m-%d %H:%M")
	today_end = (timezone.now() - datetime.timedelta(hours=6)).strftime("%Y-%m-%d %H:%M")

	if category=='0':
		#top: descuentos + clickeados del dia/semana
		if tab=='1':
			deals = ViewTopResults.objects.values('id','name','description','schedule', 'company_id','slug', 'company_ext_id').filter(country_id=user_country)[:regs_limit]
		if tab=='2':	
			deals = ViewTodayResults.objects.values('id','name','description','schedule', 'company_id','slug','company_ext_id').filter(country_id=user_country)[:regs_limit]
		if tab=='3':
			extra_where = [" ( (init_date<='"+str(today_init)+"' and end_date>='"+str(today_end)+"') or ('"+str(today_init)+"' >= init_date and end_date is null))"]
			deals = Deal.objects.values('id','name','description','schedule', 'company_id', 'company_ext_id','slug').filter(country_id=user_country, valid=True, deleted=False, company__valid=True).extra(where=extra_where).order_by('-created_at','-saves_count','-click_count')[:regs_limit]
		#paginator.
		paginator = Paginator(deals,paginator_limit)
		try:
			next_page = int(request.GET.get('page'))
			page_deals = paginator.page(next_page)
			next_page = operator.add(next_page,1) if page_deals.has_next() else False
			count = len(page_deals)
			i = 0
			for deal in page_deals:
				set_company = deal['company_ext_id'] if deal.get('company_ext_id') else deal['company_id']
				company = Company.objects.values('name','logo','slug').filter(pk=set_company)[0]
				category = CompanyCategory.objects.values_list('category_id', flat=True).filter(company_id=set_company)
				schedule = get_schedule(deal['schedule'])
				schedule_str = ', '.join(schedule)
				deal_next = next_page if i == (count-1) else False
				deals_list.append(
					dict([('id',deal['id']),('name',deal['name']), 
						('description',deal['description']),('schedule',schedule_str),
						('company',company.get('name')),('category',category), ('company_logo', company.get('logo')),
						('slug',deal['slug']),('company_slug',company.get('slug')),('next_page',deal_next)])
				)
				i = i +1
				save_print(request, deal['id'], deal['company_id'])
		except (ValueError, EmptyPage, InvalidPage):
			pass
	else:
		#al no tener categoria por descuento, se procedera a obtener la categoria de la empresa.
		category_companies = CompanyCategory.objects.values_list('company_id',flat=True).filter(category_id=category)
		if tab=='1':
			deals = ViewTopResults.objects.values('id','name','description','schedule', 'company_id','slug', 'company_ext_id').filter(( Q(company_id__in=category_companies) | Q(company_ext_id__in=category_companies) ), country_id=user_country)[:regs_limit]
		if tab=='2':				
			deals = ViewTodayResults.objects.values('id','name','description','schedule', 'company_id','slug','company_ext_id').filter(( Q(company_id__in=category_companies) | Q(company_ext_id__in=category_companies) ),country_id=user_country)[:regs_limit]
		if tab=='3':
			extra_where = [" ( (init_date<='"+str(today_init)+"' and end_date>='"+str(today_end)+"') or ('"+str(today_init)+"' >= init_date and end_date is null))"]
			deals = Deal.objects.values('id','name','description','schedule', 'company_id','slug','company_ext_id').filter(( Q(company_id__in=category_companies) | Q(company_ext_id__in=category_companies) ), country_id=user_country,  valid=True, deleted=False, company__valid=True).extra(where=extra_where).order_by('-created_at','-saves_count','-click_count')[:regs_limit]
		
		#paginator.
		paginator = Paginator(deals,paginator_limit)
		try:
			next_page = int(request.GET.get('page'))
			page_deals = paginator.page(next_page)
			next_page = operator.add(next_page,1) if page_deals.has_next() else False
			count = len(page_deals)
			i = 0
			for deal in page_deals:
				set_company = deal['company_ext_id'] if deal.get('company_ext_id') else deal['company_id']
				company = Company.objects.values('name','logo','slug').filter(pk=set_company)[0]
				category = CompanyCategory.objects.values_list('category_id', flat=True).filter(company_id=set_company)
				schedule = get_schedule(deal['schedule'])
				schedule_str = ', '.join(schedule)
				deal_next = next_page if i == (count-1) else False
				deals_list.append(
					dict([('id',deal['id']),('name',deal['name']), 
						('description',deal['description']),('schedule',schedule_str),
						('company',company.get('name')),('category',category), ('company_logo', company.get('logo')),
						('slug',deal.get('slug')),('company_slug',company.get('slug') )])
				)
				i = i +1
				save_print(request,deal['id'], deal['company_id'])
		except (ValueError, EmptyPage, InvalidPage):
			pass
	return render_to_response('app/deals/deal-card.html',{'deals':deals_list,'next_page': next_page, 'page':'home', 'user_deal':False}, RequestContext(request))

@csrf_exempt
def get_deal(request, deal_id, user_deal=False):
	try:
		not_user = False if request.user.is_authenticated() else True
		deal = Deal.objects.get(pk=int(deal_id))
		if not_user:
			subscription = False
		else:
			subscription = True if (CompanySubscriber.objects.filter(company=(deal.company_ext or deal.company), user=request.user, valid=True).exists() and not not_user) else False
		schedule = get_schedule(deal.schedule)
		schedule_str = ', '.join(schedule)
		deal_resource = DealResource.objects.filter(deal=deal)
		set_company = deal.company_ext if deal.company_ext else deal.company
		deal_category = CompanyCategory.objects.values_list("category__name",flat=True).filter(company=set_company).order_by('-id')
		deal_branch = DealBranch.objects.filter(deal=deal)
		if urlparse(request.META['HTTP_REFERER']).path == '/mis-descuentos/':
			referer = 'user'
		else:
			referer = 'else'
		if save_visit(request, deal):
			locale.setlocale(locale.LC_TIME,'es_ES')		
			if deal.end_date:
				end_date = deal.end_date.strftime("%d de %B de %Y")
			else:
				end_date = None
			init_date = deal.init_date.strftime("%d de %B de %Y")
			if request.flavour == 'mobile':
				return render_to_response('app/deals/deal-data.html',{'deal':deal, 'end_date':end_date, 'init_date': init_date ,'schedule': schedule_str,'resources': deal_resource,'categories': deal_category,'branches': deal_branch,'referer': referer, 'not_user': not_user, 'remove_deal': user_deal, 'subscription': subscription,'set_company': set_company.id}, RequestContext(request))
			else:
				return render_to_response('app/deals/deal-info.html',{'deal':deal, 'end_date':end_date, 'init_date': init_date,'schedule': schedule_str,'resources': deal_resource,'categories': deal_category,'branches': deal_branch,'referer': referer, 'not_user': not_user, 'subscription': subscription, 'set_company': set_company.id}, RequestContext(request))
		else:
			return render_to_response('app/deals/deal-info.html',{'deal':None,}, RequestContext(request))
	except Exception,e:
		print e
		return render_to_response('app/deals/deal-info.html',{'deal':None,}, RequestContext(request))
	
@csrf_exempt
def get_user_deal(request, deal_id):
	return get_deal(request, deal_id, True)

@csrf_exempt
def get_social(request, deal_id):
	try:
		deal = Deal.objects.get(pk=int(deal_id))
	except Exception, e:
		deal = None
	return render_to_response('app/deals/deal-socialbuttons.html',{'deal': deal}, RequestContext(request))


'''
deal_id = models.IntegerField()
	user_id = models.IntegerField()
	save_time = models.DateTimeField(auto_now_add=True)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	gender = models.CharField(max_length=1)
	age = models.SmallIntegerField()
	city_id = models.IntegerField()
'''
@csrf_exempt
def save_deal(request, deal_id):
	params = dict()
	if request.user.is_authenticated():
		try:
			deal = Deal.objects.get(pk=int(deal_id))
			try:
				user_deal = UserDeal.objects.get(user=request.user, deal=deal)
				params = {'message': 'El descuento seleccionado ya ha sido guardado anteriormente.'}
			except UserDeal.DoesNotExist:
				user_deal = UserDeal(user=request.user, deal=deal, country = deal.country)
				user_deal.save()
				today = datetime.datetime.today()
				profile = Profile.objects.get(user=request.user)
				deal_save = DealSaved(deal_id=deal.id, company_id=deal.company.id,user_id=request.user.id, year=today.year, 
					week=iso_week_number(today), gender=profile.gender, age=calculate_age(profile.birthday), 
					city_id=profile.city.id, weekday=today.isoweekday())
				deal_save.save()
				setattr(deal,'saves_count',operator.add(deal.saves_count,1))
				deal.save()

		except Exception, e:
			print e
	return render_to_response('app/deals/deal-save.html',params, RequestContext(request))


@csrf_exempt
def remove_deal(request, deal_id):
	params = dict()
	if request.user.is_authenticated():
		try:
			deal = Deal.objects.get(pk=int(deal_id))
			try:
				user_deal = UserDeal.objects.get(user=request.user, deal=deal)
				user_deal_rm = UserDealRemoved(user=user_deal.user, deal=user_deal.deal, country=user_deal.country)
				user_deal_rm.save()

				profile = Profile.objects.get(user=request.user)
				today = datetime.datetime.today()
				deal_removed = DealRemoved(deal_id=deal.id, company_id=deal.company.id,user_id=request.user.id, year=today.year, 
					week=iso_week_number(today), gender=profile.gender, age=calculate_age(profile.birthday), 
					city_id=profile.city.id, weekday=today.isoweekday())
				deal_removed.save()

				user_deal.delete()
			except UserDeal.DoesNotExist:
				params = {'message': 'El descuento seleccionado ya ha sido removido anteriormente.'}
		except Exception, e:
			print e
	return render_to_response('app/deals/deal-save.html',params, RequestContext(request))


def deal_search(request):
	if request.user.is_authenticated():

		malls = Mall.objects.filter(country_id=1).order_by('name')
		companies = Company.objects.filter(country_id=1).order_by('name')
		entities = ResourceEntity.objects.filter(country_id=1).order_by('name')

		return render_to_response('deals/deal-search.html',{'malls':malls,'companies':companies, 'entities': entities}, RequestContext(request))
	else:
		return HttpResponseRedirect('/')


@csrf_exempt
def search_box_input(request, what):
	json = None
	if what=='resources':
		country_id = request.GET.get('country_id')
		entity_id = request.GET.get('entity_id')
		resources = Resource.objects.filter(entity_id=entity_id, country_id=country_id)
		json = serializers.serialize("json", resources)
	return HttpResponse(json, content_type='application/javascript')

@csrf_exempt
def search_get(request, page, return_list=False, order_by_date=False):
	paginator_limit = 10
	if request.method=='GET':
		description = smart_unicode(request.GET.get('search-desc'))
		description = unicodedata.normalize('NFKD', description).encode('ascii', 'ignore')
		mall_id = request.GET.get('search-mall')
		company_ids = request.GET.getlist('search-company')
		category_ids = request.GET.getlist('search-category')
		entity_id = request.GET.get('search-entity')
		resource_ids = request.GET.getlist('search-resource')
		isset_user_deals = True if request.GET.get('hid-user') == '1' else False
		#mall: companies with establishments in selected mall.
		#companies: selected companies.
		#categories: deal categories - company category
		#entity:
		company_param = False
		if company_ids is not None and len(company_ids)>0:
			company_param = True
			new_list = list()
			for item in company_ids:
				new_list.append(int(item))
			company_ids = new_list

		companies = list()
		deals = list()
		#companies in mall.
		if mall_id is not None and mall_id != '':
			company_param = True
			company_mall = Branch.objects.values_list('company_id', flat=True).filter(mall_id=mall_id)
			companies.extend(company_mall)

			company_mall = Company.objects.values_list('id',flat=True).filter(mall_id=mall_id)
			companies.extend(company_mall)				

		#companies in list.
		if company_ids is not None and len(company_ids)>0:
			company_param = True
			if len(companies) == 0:
				companies.extend(company_ids)
			else:
				companies = common_list(companies, company_ids)
		
		#deal by company categories... get more companies.
		if category_ids is not None and len(category_ids)>0:
			company_param = True
			company_category = CompanyCategory.objects.values_list('company_id', flat=True).filter(category_id__in=category_ids)
			if len(companies) == 0:
				companies.extend(company_category)
			else:
				companies = common_list(companies, company_category)

		if len(companies)==0 and company_param:
			companies = [-1,]

		#get deals from resources.
		if resource_ids is not None:
			if u'0' in resource_ids: # 0 means "no card"
				resource_ids.remove(u'0')
				deals_no_resource = Deal.objects.values_list('id',flat=True).filter(needs_resource=False, country_id=1)
				deals.extend(deals_no_resource)
			deal_resource = DealResource.objects.values_list('deal_id',flat=True).filter(resource_id__in=resource_ids)
			deals.extend(deal_resource)

		if isset_user_deals:
			deal_user = UserDeal.objects.values_list('deal_id', flat=True).filter(user_id=request.user.id)
			if len(deals)>0:
				deals = common_list(deals, deal_user)
			else:
				deals.extend(deal_user)

		kwargs = dict()
		search_deals = list()
		today_init = timezone.now().strftime("%Y-%m-%d %H:%M")
		today_end = (timezone.now() - datetime.timedelta(hours=6)).strftime("%Y-%m-%d %H:%M")
		extra_where_date = " ( (init_date<='"+str(today_init)+"' and end_date>='"+str(today_end)+"') or ('"+str(today_init)+"' >= init_date and end_date is null))"
		extra_where = [extra_where_date]
		extra_params = None
		if description is not None and description != '':
			order_by_date = True if "padre" in description else order_by_date
			extra_where_desc = "to_tsvector('spanish', description|| ' '|| deals_deal.name ||' '|| coalesce(tags,'')) @@ plainto_or_tsquery(%s,'spanish')"
			#extra_where_desc = "to_tsvector('spanish', description|| ' '|| deals_deal.name ||' '|| coalesce(tags,'')) @@ plainto_tsquery('spanish',%s)"
			extra_where = [extra_where_date, extra_where_desc]
			extra_params = [str(description)]
		try:
			if len(deals) > 0:
				if len(companies) > 0:
					if order_by_date:
						search_deals = Deal.objects.filter( ( Q(company_id__in=companies) | Q(company_ext_id__in=companies) ), id__in=deals, valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-created_at','-click_count','-saves_count','name')
					else:
						search_deals = Deal.objects.filter(( Q(company_id__in=companies) | Q(company_ext_id__in=companies) ), id__in=deals, valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-click_count','-saves_count','name')
				else:
					if order_by_date:
						search_deals = Deal.objects.filter(id__in=deals, valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-created_at','-click_count','-saves_count','name')
					else:
						search_deals = Deal.objects.filter(id__in=deals, valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-click_count','-saves_count','name')
			else:
				if isset_user_deals:
					if len(companies)>0:
						search_deals = Deal.objects.filter(( Q(company_id__in=companies) | Q(company_ext_id__in=companies) ), id__in=deals, valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-click_count','-saves_count','name')
					else:
						search_deals = Deal.objects.filter(id__in=deals, valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-click_count','-saves_count','name')
				else:
					if len(companies)>0:
						if order_by_date:
							search_deals = Deal.objects.filter(( Q(company_id__in=companies) | Q(company_ext_id__in=companies) ), valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-created_at','-click_count','-saves_count','name')
						else:
							search_deals = Deal.objects.filter(( Q(company_id__in=companies) | Q(company_ext_id__in=companies) ), valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-click_count','-saves_count','name')
					else:
						if order_by_date:
							search_deals = Deal.objects.filter(valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-created_at','-click_count','-saves_count','name')
						else:
							search_deals = Deal.objects.filter(valid=True, deleted=False, company__valid=True, country_id=1).extra(where=extra_where, params=extra_params).order_by('-click_count','-saves_count','name')
		except Exception, e:
			print e
			pass

		paginator = Paginator(search_deals,paginator_limit)
		deals_list = list()
		try:
			next_page = int(page)
			page_deals = paginator.page(next_page)
			next_page = operator.add(next_page,1) if page_deals.has_next() else False
			for deal in page_deals:
				set_company = deal.company_ext if deal.company_ext else deal.company
				company = Company.objects.values('name','logo','slug').filter(pk=set_company.id)[0]
				category = CompanyCategory.objects.values_list('category_id',flat=True).filter(company=set_company)
				schedule = get_schedule(deal.schedule)
				schedule_str = ', '.join(schedule)
				deals_list.append(
					dict([('id',deal.id),('name',deal.name), ('slug', deal.slug),
						('description',deal.description),('schedule',schedule_str),
						('company',company.get('name')),('category',category),('company_logo', company.get('logo')),
						('company_slug', company.get('slug')) ])
				)
		except (ValueError, EmptyPage, InvalidPage, Exception):
			pass
		if return_list:
			return deals_list, next_page
		else:
			return render_to_response('app/deals/deal-card.html',{'deals':deals_list, 'next_page': next_page, 'user_deal':isset_user_deals, 'page':'search'}, RequestContext(request))


def single_deal(request,slug):
	try:
		if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
			profile = Profile.objects.get(user=request.user)
			display = profile.display
		else:
			display = None		
		deal = Deal.objects.get(slug=slug)
		set_company = deal.company_ext if deal.company_ext else deal.company
		not_user = False if request.user.is_authenticated() else True
		if not_user:
			subscription = False
		else:
			subscription = True if (CompanySubscriber.objects.filter(company=(deal.company_ext or deal.company), user=request.user, valid=True).exists() and not not_user) else False
		categories = CompanyCategory.objects.values_list("category__name",flat=True).filter(company=deal.company_ext or deal.company).order_by('-id')
		deal_resources = DealResource.objects.filter(deal=deal)
		deal_branch = DealBranch.objects.filter(deal=deal)
		schedule = get_schedule(deal.schedule)
		schedule_str = ', '.join(schedule)
		success = save_visit(request, deal)
		status = deal_status(deal)
		valid = True if status=='A' else False
		locale.setlocale(locale.LC_TIME,'es_ES')
		if deal.end_date:
			end_date = deal.end_date.strftime("%d de %B de %Y")
		else:
			end_date = None
		init_date = deal.init_date.strftime("%d de %B de %Y")
		if success:
			malls = Mall.objects.filter(country_id=1).order_by('name')
			companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
			resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
			return render_to_response('app/deals/deal-page.html',{'deal':deal,'end_date':end_date, 'init_date': init_date,'categories': categories,
				'schedule': schedule_str,'deal_resources': deal_resources, 'branches': deal_branch,
				'malls':malls,'companies':companies, 'resources': resources,
				'not_user':not_user,'display':display, 'valid': valid, 'subscription': subscription, 'set_company': set_company.id}, RequestContext(request))
		else:
			return HttpResponseRedirect('/')
	except Deal.DoesNotExist:
		raise Http404
	
def mobilesearch(request):
	request_page = request.GET.get("page")
	page = request_page if request_page else 1
	deals_list, next_page = search_get(request, page, True)
	malls = Mall.objects.filter(country_id=1).order_by('name')
	companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
	resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
	return render_to_response('app/search-page.html',{'malls':malls,'companies':companies, 'resources': resources, 'is_tablet': request.is_tablet,
		'page':'search', 'deals': deals_list, 'next_page': next_page}, RequestContext(request))

def search_query(request):
	page = request.GET.get("page")
	order_by_date = request.GET.get("order_by_date")
	deals_list, next_page = search_get(request, page, True, order_by_date)
	return render_to_response('app/deals/deal-card.html',{'deals':deals_list,'next_page': next_page}, RequestContext(request))

def mobileuser(request):
	if request.user.is_authenticated():
		if CompanyUser.objects.filter(user=request.user).exists():
			return HttpResponseRedirect('/comercios/dashboard/inicio')
		countries = Country.objects.values('id','name')
		malls = Mall.objects.filter(country_id=1).order_by('name')
		companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
		resources = Resource.objects.filter(country_id=1,valid=True).order_by('entity')
		return render_to_response('mobile/user-home.html',{'page':'user_home','countries':countries, 'malls':malls,'companies':companies, 'resources': resources}, RequestContext(request))
	else:
		return HttpResponseRedirect('/')