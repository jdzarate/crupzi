# -*- coding: utf-8 -*-
import urllib
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from companies.models import Category, Company, Branch, CompanyUser
from resources.models import Resource
from users.models import Country
from crupzi.utils import slugify_unique
# Create your models here.

class Deal(models.Model):
	name = models.CharField(max_length=120)
	description = models.CharField(max_length=175)
	schedule = models.CharField(max_length=7)
	click_count = models.IntegerField(default=0)
	saves_count = models.IntegerField(default=0)
	created_at = models.DateTimeField(auto_now_add=True)
	company = models.ForeignKey(Company)
	company_branch = models.CharField(max_length=1, choices=(('C','Compañía'),('B','Sucursales')))
	percent = models.IntegerField(null=True, blank=True)
	amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
	init_date = models.DateTimeField(null=True)
	end_date = models.DateTimeField(null=True, blank=True)
	restrictions = models.CharField(max_length=255, null=True, blank=True)
	restrictions2 = models.CharField(max_length=255, null=True, blank=True)
	valid = models.BooleanField(default=True)
	country = models.ForeignKey(Country)
	slug = models.CharField(max_length=200, null=True, blank=True)
	verified = models.BooleanField(default=False)
	needs_resource = models.BooleanField(default=False)
	deleted = models.BooleanField(default=False)
	tags = models.CharField(max_length=255, blank=True, null=True)
	company_ext = models.ForeignKey(Company, blank=True, null=True, related_name='company_external')
	limited_time = models.BooleanField(default=False)


	def save(self, *args, **kwargs):
		if not self.restrictions:
			self.restrictions = None
		if not self.restrictions2:
			self.restrictions2 = None
		if self.slug is None or self.slug is '':
			if self.company_ext:
				slug = slugify_unique(self.company_ext.name+'-'+self.name, Deal)
			else:	
				slug = slugify_unique(self.company.name+'-'+self.name, Deal)
			self.slug = slug
		super(Deal,self).save(*args,**kwargs)

	def __unicode__(self):
		return self.name +' ( '+self.company.name+')'


class DealSchedule(models.Model):
	deal = models.ForeignKey(Deal, unique=True, primary_key=True)
	country = models.ForeignKey(Country)
	monday = models.CharField(max_length=1, null=True,choices=(('S','Sí'),('N','No')))
	tuesday = models.CharField(max_length=1, null=True,choices=(('S','Sí'),('N','No')))
	wednesday = models.CharField(max_length=1, null=True,choices=(('S','Sí'),('N','No')))
	thursday = models.CharField(max_length=1, null=True,choices=(('S','Sí'),('N','No')))
	friday = models.CharField(max_length=1, null=True,choices=(('S','Sí'),('N','No')))
	saturday = models.CharField(max_length=1, null=True,choices=(('S','Sí'),('N','No')))
	sunday = models.CharField(max_length=1, null=True,choices=(('S','Sí'),('N','No')))

class DealBranch(models.Model):
	deal = models.ForeignKey(Deal)
	branch = models.ForeignKey(Branch, on_delete=models.PROTECT)
	added_at = models.DateTimeField(auto_now_add=True)

class DealCategory(models.Model):
	deal = models.ForeignKey(Deal)
	category = models.ForeignKey(Category)

class DealResource(models.Model):
	deal = models.ForeignKey(Deal)
	resource = models.ForeignKey(Resource)

class UserDeal(models.Model):
	ACTIVE_CHOICES = (('A','Activo'),('N','No activo'))
	deal = models.ForeignKey(Deal)
	user = models.ForeignKey(User)
	created_at = models.DateTimeField(auto_now_add=True)
	active = models.CharField(max_length=1,choices=ACTIVE_CHOICES)
	country = models.ForeignKey(Country, null=True)

class UserDealRemoved(models.Model):
	deal = models.ForeignKey(Deal)
	user = models.ForeignKey(User)
	removed_at = models.DateTimeField(auto_now_add=True)
	country = models.ForeignKey(Country, null=True)


'''---------
	Admin
----------'''
class ResourceInline(admin.TabularInline):
    model = DealResource

class BranchInline(admin.TabularInline):
    model = DealBranch

class DealAdmin(admin.ModelAdmin):
    inlines = [ResourceInline, BranchInline]
    list_filter = ('company',)
    search_fields = ['name','description','company__name']
    def get_form(self, request, obj=None, **kwargs):
    	self.exclude = ['slug']
    	return super(DealAdmin, self).get_form(request, obj, **kwargs)

'''---------
	Stats
----------'''
class DealSaved(models.Model):
	deal_id = models.IntegerField()
	company_id = models.IntegerField()
	user_id = models.IntegerField()
	save_time = models.DateTimeField(auto_now_add=True)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	gender = models.CharField(max_length=1)
	age = models.SmallIntegerField()
	city_id = models.IntegerField()

class DealRemoved(models.Model):
	deal_id = models.IntegerField()
	company_id = models.IntegerField()
	user_id = models.IntegerField()
	remove_time = models.DateTimeField(auto_now_add=True)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	gender = models.CharField(max_length=1)
	age = models.SmallIntegerField()
	city_id = models.IntegerField()

class DealVisit(models.Model):
	deal_id = models.IntegerField()
	company_id = models.IntegerField()
	user_id = models.IntegerField()
	visit_time= models.DateTimeField(auto_now_add=True)
	referer = models.CharField(max_length=150, null=True)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	gender = models.CharField(max_length=1)
	age = models.SmallIntegerField()
	city_id = models.IntegerField()

class AnonymousVisit(models.Model):
	deal_id = models.IntegerField()
	company_id = models.IntegerField()
	visit_time= models.DateTimeField(auto_now_add=True)
	referer = models.CharField(max_length=150, null=True)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()

class DealPrint(models.Model):
	deal_id = models.IntegerField()
	company_id = models.IntegerField()
	print_time = models.DateTimeField(auto_now_add=True)
	user_id = models.IntegerField(null=True)
	referer = models.CharField(max_length=150, null=True)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	gender = models.CharField(max_length=1)
	age = models.SmallIntegerField()
	city_id = models.IntegerField()


class Notification(models.Model):
	''' Added October, 15th 2013.	'''
	status_choices = (('P','Pending'),('S','Sent'),('I','Incomplete'))
	deal = models.ForeignKey(Deal)
	short_url = models.CharField(max_length=100, null=True)
	to_send = models.IntegerField(null=True) #
	recipients = models.IntegerField(null=True) #recipients count.
	status = models.CharField(max_length=1, choices=status_choices)


# class NotificationQueue(models.Model):
# 	''' Added October, 19th 2013.	'''
# 	notification = models.ForeignKey(Notification)
# 	recipient = models.CharField(max_length=255)
# 	sent = models.BooleanField(default=False)


'''------
  Views
------'''
class ViewTopResults(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=120)
	description = models.CharField(max_length=150)
	schedule=models.CharField(max_length=7)
	company_id = models.IntegerField()
	country_id = models.IntegerField()
	slug = models.CharField(max_length=120)
	company_ext_id = models.IntegerField()

	class Meta:
		db_table = 'view_top_results'
		managed=False

class ViewTodayResults(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=120)
	description = models.CharField(max_length=150)
	schedule=models.CharField(max_length=7)
	company_id = models.IntegerField()
	country_id = models.IntegerField()
	slug = models.CharField(max_length=120)
	company_ext_id = models.IntegerField()

	class Meta:
		db_table = 'view_today_results'
		managed=False

class ViewSaveResults(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=120)
	description = models.CharField(max_length=150)
	schedule=models.CharField(max_length=7)
	company_id = models.IntegerField()
	country_id = models.IntegerField()
	slug = models.CharField(max_length=120)
	company_ext_id = models.IntegerField()

	class Meta:
		db_table = 'view_save_results'
		managed=False


'''-----------
	Signals
------------'''
@receiver(post_save, sender=Deal)
def deal_save(sender, instance, created,**kwargs):
	if created:
		#set slug and make curl request
		try:
			# params = {}
			# params['id'] = 'http://www.crupzi.com/descuentos/'+instance.slug+'/'
			# params['scrape'] = 'true'
			# params = urllib.urlencode(params)
			# f = urllib.urlopen("https://graph.facebook.com", params)
			# f.read()
			
			#create Notification.
			notification = Notification(deal=instance, status='P')
			notification.save()
			pass
		except Exception,e:
			print e
			pass

@receiver(post_save, sender=CompanyUser)
def company_user_save(sender, instance, created, **kwargs):
	if created:
		Deal.objects.filter(company=instance.company).update(verified=True)