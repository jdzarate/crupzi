import datetime
import operator
import urllib
from deals.models import DealVisit, AnonymousVisit, DealPrint, Deal
from companies.models import CompanyUser
from users.models import Profile
from crupzi.utils import calculate_age, iso_week_number
from django.utils import timezone

def save_visit(request, deal):
	today = datetime.datetime.today()
	if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
		profile = Profile.objects.get(user=request.user)
		visit = DealVisit(deal_id=deal.id,company_id = deal.company.id, user_id=request.user.id, referer=request.META.get('HTTP_REFERER'),
			year=today.year, week=iso_week_number(today), gender=profile.gender, age=calculate_age(profile.birthday), 
			city_id=profile.city.id, weekday=today.isoweekday())
		query_set = DealVisit.objects.filter(deal_id=deal.id, user_id=request.user.id).order_by('id')[:1]
		try:
			save = True
			if query_set:
				date_diff = datetime.datetime.utcnow().replace(tzinfo=None) - query_set[0].visit_time.replace(tzinfo=None)
				if date_diff.seconds <= 7200: #max una visita en 2 horas.
					save = False
			if save:
				visit.save()
				setattr(deal,'click_count',operator.add(deal.click_count,1))
				deal.save()
			return True
		except Exception, e:
			return False
	else:
		try:
			today = datetime.datetime.today()
			anonymous = AnonymousVisit(deal_id=deal.id, company_id=deal.company.id,
				year=today.year, week=iso_week_number(today), referer=request.META.get('HTTP_REFERER'),
				weekday=today.isoweekday())
			anonymous.save()
			setattr(deal,'click_count',operator.add(deal.click_count,1))
			deal.save()
			return True
		except Exception,e:
			return False

def save_print(request, deal_id, company_id):
	if request.user.is_authenticated():
		try:
			profile = Profile.objects.get(user=request.user)
			today = datetime.datetime.today()
			deal = Deal.objects.get(pk=deal_id)
			deal_print = DealPrint(deal_id=deal_id, company_id=company_id,user_id=request.user.id, referer=request.META.get('HTTP_REFERER'),
				year=today.year, week=iso_week_number(today), gender=profile.gender, age=calculate_age(profile.birthday), 
				city_id=profile.city.id, weekday=today.isoweekday())
			deal_print.save()
		except Exception, e:
			print e
			return False
	return True

'''
deal_status
-----------
Check the deal status according to some conditions. 
Output: char(1) - A-Active(vigente), E-Expired(Expirado), N-Not valid (Invalidado)
If html is True, the output will be a label, this is used for datatables.
'''
def deal_status(deal, html=False, discard=False):
	status = None
	today_normal = timezone.now()
	today_fixed = today_normal - datetime.timedelta(hours=6) #fix to "convert" to El Salvador Timezone.
	if deal.company.valid==True or discard==True:
		if deal.deleted is False:
			if deal.valid is True:
				if deal.end_date is None or (today_normal>=deal.init_date and today_fixed<=deal.end_date):
					status = 'A'
				else:
					status = 'E'
			else:
				status = 'N'
		else:
			status = 'D'
	else:
		status = 'N'
	if html:
		label = {
			'A':'<span class="label label-success">Vigente</span>',
			'E':'<span class="label label-important">Expirado</span>',
			'N':'<span class="label">Invalidado</span>',
			'D':'<span class="label">Eliminado</span>'
		}[status]
		return label
	else:
		return status

'''
fb-request
----------
The purpose of this thing is send a curl request to fb so people can use the send and like button.
'''

def fb_request(deal):
	params = {}
	params['id'] = 'http://www.crupzi.com/descuentos/'+deal.slug+'/'
	params['scrape'] = 'true'
	params = urllib.urlencode(params)
	f = urllib.urlopen("https://graph.facebook.com", params)
	f.read()