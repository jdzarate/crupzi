from django.contrib import admin
from deals.models import Deal, DealAdmin

admin.site.register(Deal, DealAdmin)