# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from django.contrib.auth.models import User
from companies.models import Subscriber, CompanySubscriber

class Command(BaseCommand):
	help = "Sends pending deal notifications to subscribers. (no args, yet)"
	def handle(self, *args, **options):
		cursor = connections['sendy'].cursor()
		query = "select email, bounced, complaint, timestamp from subscribers where list=5 and (bounced=1 or complaint=1);"
		cursor.execute(query)
		result = cursor.fetchall()
		for item in result:
			try:
				user_id = User.objects.values_list('id',flat=True).filter(email=item[0])[0]
				subscriber = Subscriber.objects.get(user_id=user_id)
				if subscriber.valid:
					subscriber.valid = False
					if str(item[1]==1): # bounced
						subscriber.bounced = True
					else:
						subscriber.complaint=True
					subscriber.save()
					CompanySubscriber.objects.filter(user_id=user_id).update(valid=False)
			except:
				pass