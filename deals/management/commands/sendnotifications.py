# -*- coding: utf-8 -*-
import datetime
import bitly_api
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail, EmailMultiAlternatives
from django.contrib.auth.models import User
from django.conf import settings
from django.utils import timezone
from deals.models import Deal, Notification
from deals.utils import deal_status
from companies.models import CompanySubscriber
from crupzi.utils import common_list
from template_email import TemplateEmail

def bitly_shorten(bitly, long_url):
	data = bitly.shorten(long_url)
	assert data is not None
	assert data["long_url"] == long_url
	assert data["hash"] is not None
	return data["url"]

def get_message(company, company_ext=None):
	if company_ext:
		msg = "Buenas noticias: gracias a nuestra red de lechuzas descubrimos un nuevo descuento para el comercio %s via %s:" % (company_ext.name, company.name)
	else:
		msg = "Buenas noticias: gracias a nuestra red de lechuzas descubrimos un nuevo descuento para el comercio %s:" % (company.name)
	return msg

class Command(BaseCommand):
	help = "Sends pending deal notifications to subscribers. (no args, yet)"

	def handle(self, *args, **options):
		now = datetime.datetime.now()
		#try to send only if email is between 6am and 11pm
		#important.
		#verificar que el descuento sea válido. verificar que la fecha de inicio del descuento sea menor o igual a la fecha actual.		
		if now > now.replace(hour=6,minute=0,second=0,microsecond=0) and now < now.replace(hour=23,minute=50,second=0,microsecond=0):
			notifications = Notification.objects.filter(status='P')
			if notifications:
				bitly = bitly_api.Connection(access_token=settings.BITLY_ACCESS_TOKEN)
				for notif in notifications:
					if deal_status(notif.deal) == "A":
						#get list of subscribers, if there is not any delete the notification, else proceed.
						subscribers = list()
						subscribers.extend(CompanySubscriber.objects.values_list('user__email',flat=True).filter(company=notif.deal.company, valid=True, unsubscribed=False))
						if notif.deal.company_ext:
							subscribers.extend(CompanySubscriber.objects.values_list('user__email',flat=True).filter(company=notif.deal.company_ext, valid=True))
							seen = set()
							subscribers = [ x for x in subscribers if x not in seen and not seen.add(x)]
						if len(subscribers)>0:
							long_url = "http://www.crupzi.com/descuentos/"+notif.deal.slug+"/"
							try:
								short_url = bitly_shorten(bitly,long_url)
								setattr(notif,'short_url', short_url)
							except Exception, e:
								short_url = long_url
							setattr(notif,'status','I') #Incomplete (in process)...
							setattr(notif,'to_send', len(subscribers))
							notif.save()
							count = 0
							msg = get_message(notif.deal.company, notif.deal.company_ext)
							if notif.deal.company_ext:
								subject = "¡Eureka! Tienes un nuevo descuento para ".decode('utf-8')+notif.deal.company_ext.name+" via "+notif.deal.company.name
							else:	
								subject = "¡Eureka! Tienes un nuevo descuento para ".decode('utf-8')+notif.deal.company.name
							for email in subscribers:
								email = TemplateEmail(template='mails/subscription.html', context={'email_subject': subject, 'email_message': msg, 'url': short_url, 'deal_name': notif.deal.name }, to=[email], from_email="Crupzi <info@crupzi.com>")
								email.send()
								#send_mail(subject, msg, 'info@crupzi.com', [email], fail_silently=False)
								count = count + 1
							setattr(notif,'recipients', count)
							setattr(notif,'status','S') #Sent!
							notif.save()
						else:
							notif.delete()