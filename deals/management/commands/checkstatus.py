# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db import connections

class Command(BaseCommand):
	help = "Sends pending deal notifications to subscribers. (no args, yet)"
	def handle(self, *args, **options):
		cursor = connections['sendy'].cursor()
		query = "select email, bounced, complaint, timestamp from subscribers where list=5 and (bounced=1 or complaint=1);"
		cursor.execute(query)
		result = cursor.fetchall()
		for item in result:
			print "email: "+str(item[0])+", bounced: "+str(item[1])