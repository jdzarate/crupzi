from django.conf.urls import patterns, include, url
from deals.views import get_public_data, get_deal, save_deal, remove_deal, deal_search, search_box_input, search_get, single_deal, get_social, search_query, get_user_deal

urlpatterns = patterns('',
	url(r'^busqueda/', deal_search, name='deal_search'),
	url(r'^descuentos/(?P<slug>[-\w]+)/', single_deal, name='single_deal'),
	url(r'^search-box/get/(?P<what>\w+)/', search_box_input, name='search_box_input'),
	url(r'^search/query/', search_query, name='search_query'),
	url(r'^search/get/(?P<page>\w+)/', search_get, name='search_get'),
	url(r'^user-deals/get/code/(?P<deal_id>\w+)/', get_user_deal, name='get_user_deal'),
	url(r'^deals/get/code/(?P<deal_id>\w+)/', get_deal, name='get_deal'),
	url(r'^deals/get/social/(?P<deal_id>\w+)/', get_social, name='get_social'),
	url(r'^deals/get/(?P<category>\w+)/(?P<tab>\w+)/', get_public_data, name='get_public_data'),
	url(r'^deals/save/(?P<deal_id>\w+)/', save_deal, name='save_deal'),
	url(r'^deals/remove/(?P<deal_id>\w+)/', remove_deal, name='remove_deal'),
)