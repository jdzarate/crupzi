# -*- coding: utf-8 -*-
import os, binascii, time, datetime, locale
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import timezone, simplejson
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from companies.models import Company, CompanyUser, CompanyCategory, Branch, CompanySubscriber, Subscriber
from deals.models import Deal, DealSchedule, DealBranch
from users.models import Profile
from crupzi.utils import get_schedule, last_day_iso_week, first_day_iso_week, iso_week_number, iso_weekday_name
from deals.utils import save_print
from stats.utils import get_datadiff_percent, get_weekly_general_data
from companies.dashboardviews import *
from companies.dashboardstats import *


def companies_hub(request):
	if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
		display = Profile.objects.values_list('display', flat=True).get(user=request.user)
		user_profile = True
	else:
		display = None
		user_profile = False
	if request.method =='GET':
		return render_to_response('app/companies/companies_hub.html', {'display': display, 'user_profile': user_profile},RequestContext(request))
	else:
		return HttpResponseRedirect('/')

def companies_list(request, category):
	companies = None
	if request.method=='GET':
		country = 1
		if category == '0':
			companies = Company.objects.filter(country_id=country, valid=True).order_by('name')
		else:
			company_ids = CompanyCategory.objects.values_list('company_id',flat=True).filter(category_id=category)
			companies = Company.objects.filter(country_id=country, id__in=company_ids, valid=True).order_by('name')
	return render_to_response('app/companies/companies_list.html',{'companies':companies}, RequestContext(request))


def companies_home(request,slug, mobile=False):
	try:
		not_user = False if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists() else True
		company = Company.objects.get(slug=slug, country_id=1)
		if not_user:
			subscription = False
		else:
			subscription = True if (CompanySubscriber.objects.filter(company=company, user=request.user, valid=True).exists() and not not_user) else False
		branches = Branch.objects.filter(company=company)
		categories = CompanyCategory.objects.values_list('category_id',flat=True).filter(company=company)
		today_day = datetime.date.today().isoweekday()
		today_init = timezone.now().strftime("%Y-%m-%d %H:%M")
		today_end = (timezone.now() - datetime.timedelta(hours=6)).strftime("%Y-%m-%d %H:%M")
		malls = Mall.objects.filter(country_id=1).order_by('name')
		companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
		resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
		extra_where = [" ( (init_date<='"+str(today_init)+"' and end_date>='"+str(today_end)+"') or ('"+str(today_init)+"' >= init_date and end_date is null))"]
		#dealList = Deal.objects.values_list('id','name','description','schedule','slug','company_branch').filter(company=company, valid=True, deleted=False).extra(where=extra_where).order_by('-created_at')[:3]
		dealList = Deal.objects.values_list('id','name','description','schedule','slug','company_branch','company_ext_id').filter( ( Q(company=company) | Q(company_ext=company) ) , valid=True, deleted=False).extra(where=extra_where).order_by('-created_at')
		deals = list()
		for item in dealList:
			schedule = get_schedule(item[3])
			schedule_str = ', '.join(schedule)
			today = True if item[3][today_day-1]=='1' else False
			if item[6]:
				company_ext = Company.objects.values('name','logo','slug').filter(pk=item[6])[0]
				categories_ext = CompanyCategory.objects.values_list('category_id',flat=True).filter(company=item[6])
				deals.append(dict(id=item[0],name=item[1], description=item[2], schedule=schedule_str,
				slug=item[4], today = today, company_branch=item[5], category=categories_ext, company_deal=True, 
				company_logo=company_ext.get('logo'), 
				company=company_ext.get('name'), 
				company_slug=company_ext.get('slug')))
			else:		
				deals.append(dict(id=item[0],name=item[1], description=item[2], schedule=schedule_str,
					slug=item[4], today = today, company_branch=item[5], category=categories, company_deal=True, 
					company_logo=company.logo, 
					company=company.name, 
					company_slug=company.slug))
			save_print(request,item[0], company.id)
		if not_user:
			display = None
			user_profile = False
		else:
			display = Profile.objects.values_list('display', flat=True).get(user=request.user)
			user_profile = True
		verified = True if CompanyUser.objects.filter(company=company).exists() else False
		company_categories = CompanyCategory.objects.values_list("category__name",flat=True).filter(company=company).order_by('-id')
		return render_to_response('app/companies/companies_home.html',{'page':'company_home','company':company, 'branches': branches, 'is_tablet': request.is_tablet,
			'categories': categories, 'company_categories': company_categories, 'verified': verified,'deals': deals, 'display':display, 'user_profile':user_profile, 'malls':malls,'companies':companies, 'resources': resources, 'not_user': not_user, 'subscription':subscription, 'set_company': company.id}, RequestContext(request))

	except Exception,e:
		print e
		raise Http404


def mobile_home(request, slug):
	return companies_home(request, slug, True)

@csrf_exempt
def company_subscribe(request, subscribe=True):
	error_list = list()
	if request.method == 'POST':
		if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
			if subscribe:
				try:
					if Subscriber.objects.filter(user=request.user).exists():
						subscriber = Subscriber.objects.get(user=request.user)						
					else:
						subscriber = Subscriber(user=request.user)
						subscriber.save()
					if subscriber.valid:
						company = Company.objects.get(id=request.POST.get('company_id'))
						if CompanySubscriber.objects.filter(company=company, user=request.user).exists():
							company_subscriber = CompanySubscriber.objects.get(company=company, user=request.user)
							if company_subscriber.valid and not company_subscriber.unsubscribed:
								error_list.append("Hey, parece que ya estas suscrito.")
							else:					
								setattr(company_subscriber, 'valid', True)
								setattr(company_subscriber, 'unsubscribed', False)
								company_subscriber.save()
						else:
							company_subscriber = CompanySubscriber(company=company, user=request.user)
							company_subscriber.save()
					else:
						if subscriber.complaint:
							error_list.append("No es posible suscribirte porque marcaste nuestros correos como spam.")
						else:
							error_list.append("No es posible suscribirte porque tu correo no es válido.")
				except Company.DoesNotExist:
					error_list.append("No hemos encontrado el comercio al que intentas suscribirte.")
			else:
				try:
					subscriber = CompanySubscriber.objects.get(company_id=request.POST.get('company_id'), user=request.user)
					setattr(subscriber, 'unsubscribed', True)
					subscriber.save()
				except CompanySubscriber.DoesNotExist:
					error_list.append("No hemos encontrado tu suscripción al comercio seleccionado.")
			response = [dict(success=(len(error_list)==0), errors=list(error_list))]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

@csrf_exempt
def company_unsubscribe(request):
	return company_subscribe(request, False)