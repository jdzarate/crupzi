# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.syndication.views import Feed, FeedDoesNotExist
from django.core.mail import send_mail
from django.dispatch import receiver
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_delete, post_save
from django.shortcuts import get_object_or_404
from users.models import Country, City
from crupzi.utils import slugify_unique


# Create your models here.
class Mall(models.Model):
	name = models.CharField(max_length=100)
	country = models.ForeignKey(Country)
	def __unicode__(self):
		return self.name

class Company(models.Model):
	name = models.CharField(max_length=150)
	country = models.ForeignKey(Country)
	address = models.CharField(max_length=200, null=True, blank=True)
	longitude = models.DecimalField(max_digits=7, decimal_places=4, null=True, blank=True)
	latitude = models.DecimalField(max_digits=6, decimal_places=4, null=True, blank=True)
	phone_number = models.CharField(max_length=20, null=True, blank=True)
	website = models.URLField(max_length=200, null=True, blank=True)
	mall = models.ForeignKey(Mall, null=True, blank=True)
	logo = models.CharField(max_length=150, null=True)
	slug = models.CharField(max_length=100,null=True, blank=True)
	valid = models.BooleanField(default=True)
	
	class Meta:
		ordering = ['name']
		verbose_name_plural = 'Companies'

	def save(self, *args, **kwargs):
		if not self.address:
			self.address = None
		if not self.phone_number:
			self.phone_number = None
		super(Company,self).save(*args,**kwargs)

	def __unicode__(self):
		return self.name

class Branch(models.Model):
	name = models.CharField(max_length=150)
	company = models.ForeignKey(Company)
	address = models.CharField(max_length=200, null=True, blank=True)
	mall = models.ForeignKey(Mall, null=True, blank=True)
	longitude = models.DecimalField(max_digits=7, decimal_places=4, null=True, blank=True)
	latitude = models.DecimalField(max_digits=6, decimal_places=4, null=True, blank=True)
	phone_number = models.CharField(max_length=20, null=True, blank=True)
	valid = models.BooleanField(default=True)

	def __unicode__(self):
		return self.company.name+' - '+self.name

class DeletedBranch(models.Model):
	name = models.CharField(max_length=150)
	company = models.ForeignKey(Company)
	address = models.CharField(max_length=200, null=True)
	mall = models.ForeignKey(Mall, null=True)
	longitude = models.DecimalField(max_digits=7, decimal_places=4, null=True)
	latitude = models.DecimalField(max_digits=6, decimal_places=4, null=True)
	phone_number = models.CharField(max_length=20, null=True)
	deleted_at = models.DateTimeField(auto_now_add=True)

class CompanyLogin(models.Model):
	company = models.ForeignKey(Company)
	password = models.CharField(max_length=128)

class Category(models.Model):
	name = models.CharField(max_length=60)
	logo = models.ImageField(upload_to='/static/', null=True)
	def __unicode__(self):
		return self.name

class CompanyCategory(models.Model):
	company = models.ForeignKey(Company)
	category = models.ForeignKey(Category)

class CompanyUser(models.Model):
	company = models.ForeignKey(Company)
	user = models.ForeignKey(User)

	class Meta:
		ordering = ['company']
		verbose_name_plural = 'Company users'

	def __unicode__(self):
		return self.company.name

class SignupForm(models.Model):
	# This model stores data from the sign up form for companies. #
	name = models.CharField(max_length=45)
	contact = models.CharField(max_length=45)
	email = models.EmailField(max_length=45)
	phone_number = models.CharField(max_length=50)
	website = models.URLField(blank=True, null=True)
	category = models.CharField(max_length=40)
	other_category = models.CharField(max_length=150, blank=True, null=True)
	validated = models.BooleanField(default=False)
	account_created = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)


class Subscriber(models.Model):
	user = models.ForeignKey(User)
	valid = models.BooleanField(default=True)
	bounced = models.BooleanField(default=False)
	complaint = models.BooleanField(default=False) #marked as spam.

class CompanySubscriber(models.Model):
	''' Added October, 15th 2013.	'''
	company = models.ForeignKey(Company)
	user = models.ForeignKey(User)
	valid = models.BooleanField(default=True)
	unsubscribed = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)

class CompanyFeed(Feed):
	description_template = "feeds/company_feed_description.html"
	
	def get_object(self, request, slug):
		return get_object_or_404(Company, slug=slug)

	def title(self, obj):
		return "Crupzi.com: Descuentos para el comercio %s" % obj.name

	def link(self, obj):
		return obj.get_absolute_url()

	def description(self,obj):
		return "Lista de descuentos ingresados para el comercio: %s" % obj.name

	def items(self, obj):
		return Deal.objects.filter((Q(company=obj) | Q(company_ext=obj))).order_by('-created_at')[:10]


'''---------
	Admin
----------'''
class CategoryInline(admin.TabularInline):
    model = CompanyCategory

class CompanyAdmin(admin.ModelAdmin):
    inlines = [CategoryInline,]
    def get_form(self, request, obj=None, **kwargs):
    	self.exclude = ['slug','logo']
    	return super(CompanyAdmin, self).get_form(request, obj, **kwargs)

'''-----------
	Signals
------------'''
@receiver(pre_delete, sender=Branch)
def delete_branch_task(sender, instance, **kwargs):
	deleted = DeletedBranch(name=instance.name, address=instance.address, phone_number=instance.phone_number, mall=instance.mall,
		longitude=instance.longitude, latitude=instance.latitude, company=instance.company)
	deleted.save()

@receiver(post_save, sender=Company)
def company_save(sender, instance, created,**kwargs):
	if created:
		instance.slug = slugify_unique(instance.name, Company)
		instance.logo = str(instance.id)+'.jpg'
		instance.save()

@receiver(post_save, sender=SignupForm)
def signup_email(sender, instance, created, **kwargs):
	if created:
		msg = """
Nombre: %(name)s
Contacto: %(contact)s
Email: %(email)s
Telefono: %(phone)s
Website: %(website)s
Categoria: %(category)s
Otra categoria: %(other_category)s 
""" % {'name': instance.name, 'contact': instance.contact, 'email': instance.email, 'phone': instance.phone_number, 'website': instance.website, 'category': instance.category, 'other_category': instance.other_category}
		send_mail('Nuevo comercio inscrito, id: '+str(instance.id), msg, 'Crupzi.com <no-reply@crupzi.com>', ['socios@crupzi.com'], fail_silently=True)