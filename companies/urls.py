from django.conf.urls import patterns, include, url
from companies.views import ( 
	deal_crud, deal_table, companies_hub, 
	companies_list, companies_home, branch_data, companies_data, branches_info, 
	dashboard_stats, business_dashboard, dashboard_page, password_reset, 
	signup_process, resources_info, company_subscribe, company_unsubscribe

)

urlpatterns = patterns('',
	url(r'^branches/get/', branch_data, name='branch_data'),
	url(r'^comercios/passwordreset/', password_reset, name='password_reset'),
	url(r'^comercios/procesarInscripcion/', signup_process, name='signup_process'),

	url(r'^comercios/branch/(?P<action>\w+)', branches_info, name='branches_info'),
	url(r'^comercios/cards/(?P<action>\w+)', resources_info, name='resources_info'),
	url(r'^comercios/data/(?P<action>\w+)', companies_data, name='companies_data'),
	url(r'^comercios/dashboard/stats/(?P<which>\w+)', dashboard_stats, name='dashboard_stats'),
	url(r'^comercios/dashboard/(?P<which>\w+)', dashboard_page, name='dashboard_page'),
	url(r'^comercios/dashboard/', business_dashboard, name='business_dashboard'),
	
	url(r'^comercios/deals/(?P<action>\w+)', deal_crud, name='deal_crud'),
	url(r'^comercios/deals-table/', deal_table, name='deal_table'),
	url(r'^comercios/lista/', companies_hub, name='companies_hub'),
	url(r'^comercios/perfiles/(?P<slug>[-\w]+)', companies_home, name='companies_home'),
	url(r'^companies_list/get/(?P<category>\w+)', companies_list, name='companies_list'),

	url(r'^comercios/subscribe/', company_subscribe, name='company_subscribe'),
	url(r'^comercios/unsubscribe/', company_unsubscribe, name='company_unsubscribe'),
)