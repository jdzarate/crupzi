# -*- coding: utf-8 -*-
import os
from PIL import Image
from cropresize import crop_resize
from django import forms
from django.conf import settings
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from users.models import Country
from companies.models import Company

class CompanyInfoForm(forms.Form):
	name = forms.CharField(label='Nombre', required=True)
	adddress = forms.CharField(label='Direccion', required=False)
	
class CompanyLogoForm(forms.Form):
	logo = forms.ImageField(required=False)
	def save(self, company, files):
		#display uploads.
		display = files.get('logo')
		if display is not None:
			filename = company.slug+'__'+display.name
			path = os.path.join(settings.UPLOAD_URL_LOGOS,'logos/'+filename)
			destination = open(path,'wb+')
			for chunk in display.chunks():
				destination.write(chunk)
			destination.close()
			try:
				img = Image.open(path)
				resized_img = crop_resize(img,(56,56))
				resized_img.save(os.path.join(settings.UPLOAD_URL_LOGOS,'logos_56/56x56_'+filename))
				setattr(company, 'logo',filename)
				company.save()
			except Exception,e:
				print e
			