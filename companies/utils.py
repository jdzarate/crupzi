import time, re
from companies.models import Branch
from deals.models import Deal

def valid_date(strdate):
	try:
		time.strptime(strdate, '%d/%m/%Y')
		return True
	except ValueError:
		return False

def valid_deal_form(company, form):
	valid = True
	errors = list()
	if form.get('txt-name') == '' or form.get('txt-date_ini') == '' or form.get('hid-schedule')=='':
		valid = False
		errors.append('Faltan que complete datos requeridos.')

	regex = re.compile('^[01]{7}$') #schedule.
	if regex.match(form.get('hid-schedule')) is None:
		errors.append('La especificacion de fechas es incorrecta.')
		valid = False

	if form.get('txt-percent'):
		percentRegex = re.compile('^[0-9]{1,3}$')
		if percentRegex.match(form.get('txt-percent')) is None:
			errors.append('El porcentaje de descuento es incorrecto.')
			valid = False
		else:
			percent = int(form.get('txt-percent'))
			if percent > 100:
				errors.append('El porcentaje de descuento es incorrecto.')	
				valid = False
	#is date
	if not valid_date(form.get('txt-date_ini')):
		valid = False
		errors.append('La fecha inicial no cumple con el formato esperado. (dd/mm/yyyy)')
	if form.get('txt-date_end') != '':
		if not valid_date(form.get('txt-date_end')):
			valid = False
			errors.append('La fecha final no cumple con el formato esperado. (dd/mm/yyyy)')
	branches = form.getlist('sel-branch')
	if branches:
		if len(branches)>0:
			for item in branches:
				branch_id = int(item)
				try:
					branch = Branch.objects.get(id=branch_id, company=company)
					continue
				except Branch.DoesNotExist:
					try:
						branch = Branch.objects.get(id=branch_id, company_id=form.get("sel-company"))
					except Branch.DoesNotExist:
						valid = False
						errors.append('Hubo un error con la sucursales ingresadas.')
					break
	return valid, errors