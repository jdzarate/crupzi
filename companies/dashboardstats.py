# -*- coding: utf-8 -*-
import os, binascii, time, datetime, locale, math
from django.db.models import Sum
from django.http import HttpResponse, Http404
from django.utils import simplejson, timezone
from django.views.decorators.csrf import csrf_exempt
from companies.models import CompanyUser
from deals.models import Deal
from users.models import City
from stats.models import *
from crupzi.utils import last_day_iso_week, first_day_iso_week, iso_week_number, iso_weekday_name
from stats.utils import get_datadiff_percent, get_weekly_general_data

'''
select year, week, sum(prints) as prints, sum(clicks) as clicks
from stats_weeklygeneral
where company_id = 28 and week between 35-10 and 35
group by company_id, year, week;

'''
# Stats
@csrf_exempt
def dashboard_stats(request, which):
	if(request.user.is_authenticated):
		try:
			company_user = CompanyUser.objects.get(user=request.user)
			if request.GET.get('stats_date'):
				today = datetime.datetime.strptime(request.GET.get('stats_date'), '%d/%m/%Y')
			else:
				today = datetime.datetime.today() - datetime.timedelta(days=7)
			
			isocalendar = today.isocalendar()
			isoweek=isocalendar[1]
			year = isocalendar[0]

			company = company_user.company.id

			if not StatsControl.objects.filter(year=year,week=isoweek,company_id=company).exists():
				response = [dict(nodata=True)]
				json = simplejson.dumps(response)
				return HttpResponse(json, content_type='application/javascript')

			if which=='general':
				#General
				#weeklyGral = WeeklyGeneral.objects.filter(company_id=company, year__lte=year, week__lte=isoweek).values('year','week').annotate(prints=Sum('prints'), clicks=Sum('clicks'), saves=Sum('saves'), anonymous=Sum('anonymous')).order_by('-year','-week')[:5]

				resp_gral = list()
				resp_gral.append(('Semana','Impresiones','Clicks','Anónimos'))

				gral_clicks0 = gral_prints0 = gral_saves0 = 0
				gral_clicks1 = gral_prints1 = gral_saves1 = 0

				diffclicks = diffprints = diffsaves = 0;
				now = today.timetuple()
				n = 5 #Numero maximo de meses a mostrar.
				year_month_tuple=[datetime.datetime.fromtimestamp(time.mktime([now.tm_year, now.tm_mon, now.tm_mday-(7*(n)), 0, 0, 0, 0, 0, 0])).isocalendar() for n in range(5)]
				for yearmonth in reversed(year_month_tuple):
					item_year = yearmonth[0]
					item_isoweek = yearmonth[1]
					try:
						stats_data = get_weekly_general_data(company, item_year, item_isoweek)
						resp_gral.append(list((last_day_iso_week(item_year,item_isoweek),stats_data[0],stats_data[1], stats_data[3])))
						if item_isoweek==isoweek:
							gral_prints0+=stats_data[0]
							gral_clicks0+=stats_data[1]
							gral_saves0+=stats_data[2]
							diffprints+=stats_data[0]
							diffclicks+=stats_data[1]						
							diffsaves+=stats_data[1]
						if item_isoweek==isoweek-1:
							gral_prints1+=stats_data[0]
							gral_clicks1+=stats_data[1]
							gral_saves1+=stats_data[2]
							diffprints-=stats_data[0]
							diffclicks-=stats_data[1]
							diffsaves-=stats_data[2]
					except Exception,e:
						print e
						resp_gral.append(list((last_day_iso_week(item_year,item_isoweek),0,0,0)))

				gral_prints_percent = get_datadiff_percent(gral_prints0, gral_prints1)
				gral_clicks_percent = get_datadiff_percent(gral_clicks0, gral_clicks1)
				gral_saves_percent = get_datadiff_percent(gral_saves0, gral_saves1)

				#Gender
				resp_gender = list()
				men_prints = men_clicks = 0
				women_prints = women_clicks = 0
				total_prints = total_clicks = 0
				resp_gender = list()
				resp_gender.append(('Tipo','Hombres','Mujeres'))
				weeklyGender = WeeklyGender.objects.filter(company_id=company, week=isoweek, year=year).values('year','week','gender').annotate(prints=Sum('prints'), clicks=Sum('clicks'))
				for item in weeklyGender:
					if item['gender'] == 'M':
						men_prints = men_prints + int(item['prints'])
						men_clicks = men_clicks + int(item['clicks'])
					else:
						women_prints = women_prints + int(item['prints'])
						women_clicks = women_clicks + int(item['clicks'])
				
				total_prints = men_prints + women_prints
				total_clicks = men_clicks + women_clicks
				
				men_prints = round(float(1.0*men_prints/total_prints),2) if total_prints!=0 else 0
				women_prints = round(float(1.0*women_prints/total_prints),2) if total_prints!=0 else 0
				men_clicks = round(float(1.0*men_clicks/total_clicks),2) if total_clicks!=0 else 0
				women_clicks = round(float(1.0*women_clicks/total_clicks),2) if total_clicks!=0 else 0
				resp_gender.append(('Impresiones', men_prints, women_prints))
				resp_gender.append(('Clicks', men_clicks, women_clicks))

				#Ages
				prints__0_15 = prints__16_20 = prints__21_25 = prints__26_30 = prints__31_40 = prints__41_0 = 0
				clicks__0_15 = clicks__16_20 = clicks__21_25 = clicks__26_30 = clicks__31_40 = clicks__41_0 = 0
				resp_ages_prints = list()
				resp_ages_clicks = list()
				resp_ages_prints.append(('Edad','Impresiones'))
				resp_ages_clicks.append(('Edad','Clicks'))
				weeklyAges = WeeklyAges.objects.filter(company_id=company, week=isoweek, year=year).values('year','week','age').annotate(prints=Sum('prints'), clicks=Sum('clicks'))
				for item in weeklyAges:
					age = item['age']
					if age > 0 and age <=15:
						prints__0_15 = prints__0_15 + int(item['prints'])
						clicks__0_15 = clicks__0_15 + int(item['clicks'])
					if age>15 and age<=20:
						prints__16_20 = prints__16_20 + int(item['prints'])
						clicks__16_20 = clicks__16_20 + int(item['clicks'])
					if age>20 and age<=25:
						prints__21_25 = prints__21_25 + int(item['prints'])
						clicks__21_25 = clicks__21_25 + int(item['clicks'])
					if age>25 and age<=30:
						prints__26_30 = prints__26_30 + int(item['prints'])
						clicks__26_30 = clicks__26_30 + int(item['clicks'])
					if age>30 and age<=40:
						prints__31_40 = prints__31_40 + int(item['prints'])
						clicks__31_40 = clicks__31_40 + int(item['clicks'])
					if age>40:
						prints__41_0 = prints__41_0 + int(item['prints'])
						clicks__41_0 = clicks__41_0 + int(item['clicks'])
				resp_ages_prints.append(('0-15', prints__0_15))
				resp_ages_prints.append(('16-20',prints__16_20))
				resp_ages_prints.append(('21-25',prints__21_25))
				resp_ages_prints.append(('26-30',prints__26_30))
				resp_ages_prints.append(('31-40',prints__31_40))
				resp_ages_prints.append(('41+',  prints__41_0))

				resp_ages_clicks.append(('0-15', clicks__0_15))
				resp_ages_clicks.append(('16-20',clicks__16_20))
				resp_ages_clicks.append(('21-25',clicks__21_25))
				resp_ages_clicks.append(('26-30',clicks__26_30))
				resp_ages_clicks.append(('31-40',clicks__31_40))
				resp_ages_clicks.append(('41+',  clicks__41_0))

				#Locations.
				resp_location_prints = list()
				resp_location_clicks = list()
				resp_location_prints.append(('Ubicación','Impresiones'))
				resp_location_clicks.append(('Ubicación','Clic'))
				weeklyLocations = WeeklyLocations.objects.filter(company_id=company, week=isoweek, year=year).values('year','week','city_id').annotate(prints=Sum('prints'), clicks=Sum('clicks'))
				for item in weeklyLocations:
					city = City.objects.filter(id=item['city_id']).values('name')[0]
					resp_location_clicks.append((city['name'], item['clicks']))
					resp_location_prints.append((city['name'], item['prints']))

				if total_clicks==0:
					resp_ages_clicks = list([('Edad','Clicks')])
					resp_location_clicks = list([('Ubicación','Clicks')])
				if total_prints==0:
					resp_ages_prints = list([('Edad','Impresiones')])
					resp_location_prints = list([('Ubicación','Impresiones')])

				response = [dict(gral=resp_gral, gender=resp_gender, ages_prints=resp_ages_prints, ages_clicks=resp_ages_clicks,
					location_clicks=resp_location_clicks, location_prints=resp_location_prints, diffclicks=diffclicks, 
					diffprints=diffprints, diffsaves=diffsaves, gral_clicks_percent=gral_clicks_percent,
					gral_prints_percent=gral_prints_percent, gral_saves_percent=gral_saves_percent)]



			###########  - Deals - ############
			if which=='deal':
				#General
				deal_id = request.GET.get('deal_id')
				
				dealGral = DailyGeneral.objects.filter(company_id=company, deal_id=deal_id, week=isoweek, year=year).values('year','week','weekday').annotate(prints=Sum('prints'), clicks=Sum('clicks'), anonymous=Sum('anonymous')).order_by('year','week','weekday')
				resp_gral = list()
				resp_gral.append(('Dia','Impresiones','Clicks','Anónimos'))
				for i in range(1,8):
					found = False
					for item in dealGral:
						if(item['weekday']==i):
							found = True
							resp_gral.append(list((iso_weekday_name(item['weekday']),item['prints'],item['clicks'], item['anonymous'])))
					if found is False:
						resp_gral.append(list((iso_weekday_name(i),0,0,0)))

				#Gender
				resp_gender = list()
				men_prints = men_clicks = 0
				women_prints = women_clicks = 0
				total_prints = total_clicks = 0
				resp_gender = list()
				resp_gender.append(('Tipo','Hombres','Mujeres'))
				weeklyGender = WeeklyGender.objects.filter(company_id=company, week=isoweek, year=year, deal_id=deal_id).values('year','week','gender').annotate(prints=Sum('prints'), clicks=Sum('clicks'))
				for item in weeklyGender:
					if item['gender'] == 'M':
						men_prints = men_prints + int(item['prints'])
						men_clicks = men_clicks + int(item['clicks'])
					else:
						women_prints = women_prints + int(item['prints'])
						women_clicks = women_clicks + int(item['clicks'])
				
				total_prints = men_prints + women_prints
				total_clicks = men_clicks + women_clicks
				
				men_prints = round(float(1.0*men_prints/total_prints),2) if total_prints!=0 else 0
				women_prints = round(float(1.0*women_prints/total_prints),2) if total_prints!=0 else 0
				men_clicks = round(float(1.0*men_clicks/total_clicks),2) if total_clicks!=0 else 0
				women_clicks = round(float(1.0*women_clicks/total_clicks),2) if total_clicks!=0 else 0
				resp_gender.append(('Impresiones', men_prints, women_prints))
				resp_gender.append(('Clicks', men_clicks, women_clicks))

				#Ages
				prints__0_15 = prints__16_20 = prints__21_25 = prints__26_30 = prints__31_40 = prints__41_0 = 0
				clicks__0_15 = clicks__16_20 = clicks__21_25 = clicks__26_30 = clicks__31_40 = clicks__41_0 = 0
				resp_ages_prints = list()
				resp_ages_clicks = list()
				resp_ages_prints.append(('Edad','Impresiones'))
				resp_ages_clicks.append(('Edad','Clicks'))
				weeklyAges = WeeklyAges.objects.filter(company_id=company, week=isoweek, year=year, deal_id=deal_id).values('year','week','age').annotate(prints=Sum('prints'), clicks=Sum('clicks'))
				for item in weeklyAges:
					age = item['age']
					if age > 0 and age <=15:
						prints__0_15 = prints__0_15 + int(item['prints'])
						clicks__0_15 = clicks__0_15 + int(item['clicks'])
					if age>15 and age<=20:
						prints__16_20 = prints__16_20 + int(item['prints'])
						clicks__16_20 = clicks__16_20 + int(item['clicks'])
					if age>20 and age<=25:
						prints__21_25 = prints__21_25 + int(item['prints'])
						clicks__21_25 = clicks__21_25 + int(item['clicks'])
					if age>25 and age<=30:
						prints__26_30 = prints__26_30 + int(item['prints'])
						clicks__26_30 = clicks__26_30 + int(item['clicks'])
					if age>30 and age<=40:
						prints__31_40 = prints__31_40 + int(item['prints'])
						clicks__31_40 = clicks__31_40 + int(item['clicks'])
					if age>40:
						prints__41_0 = prints__41_0 + int(item['prints'])
						clicks__41_0 = clicks__41_0 + int(item['clicks'])
				resp_ages_prints.append(('0-15', prints__0_15))
				resp_ages_prints.append(('16-20',prints__16_20))
				resp_ages_prints.append(('21-25',prints__21_25))
				resp_ages_prints.append(('26-30',prints__26_30))
				resp_ages_prints.append(('31-40',prints__31_40))
				resp_ages_prints.append(('41+',  prints__41_0))

				resp_ages_clicks.append(('0-15', clicks__0_15))
				resp_ages_clicks.append(('16-20',clicks__16_20))
				resp_ages_clicks.append(('21-25',clicks__21_25))
				resp_ages_clicks.append(('26-30',clicks__26_30))
				resp_ages_clicks.append(('31-40',clicks__31_40))
				resp_ages_clicks.append(('41+',  clicks__41_0))

				#Locations.
				resp_location_prints = list()
				resp_location_clicks = list()
				resp_location_prints.append(('Ubicación','Impresiones'))
				resp_location_clicks.append(('Ubicación','Clic'))
				weeklyLocations = WeeklyLocations.objects.filter(company_id=company, week=isoweek, year=year, deal_id=deal_id).values('year','week','city_id').annotate(prints=Sum('prints'), clicks=Sum('clicks'))
				for item in weeklyLocations:
					city = City.objects.filter(id=item['city_id']).values('name')[0]
					resp_location_clicks.append((city['name'], item['clicks']))
					resp_location_prints.append((city['name'], item['prints']))

				if total_clicks==0:
					resp_ages_clicks = list([('Edad','Clicks')])
					resp_location_clicks = list([('Ubicación','Clicks')])
				if total_prints==0:
					resp_ages_prints = list([('Edad','Impresiones')])
					resp_location_prints = list([('Ubicación','Impresiones')])

				response = [dict(gral=resp_gral, gender=resp_gender, ages_prints=resp_ages_prints, ages_clicks=resp_ages_clicks,
				location_clicks=resp_location_clicks, location_prints=resp_location_prints)]

		except Exception, e:
			print e
			errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
			response = [dict(error=True, errors=errors)]
		json = simplejson.dumps(response)
		return HttpResponse(json, content_type='application/javascript')