from django.contrib import admin
from companies.models import Branch,Company, CompanyUser, CompanyAdmin

admin.site.register(Branch)
admin.site.register(Company,CompanyAdmin)
admin.site.register(CompanyUser)