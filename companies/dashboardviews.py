# -*- coding: utf-8 -*-
import os, binascii, time, datetime, locale, math
from django.contrib.auth.models import User
from django.db import IntegrityError, connection, transaction
from django.db.models import Sum
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson, timezone
from django.views.decorators.csrf import csrf_exempt
from companies.models import Company, CompanyUser, Branch, Mall, SignupForm
from companies.utils import valid_deal_form
from companies.forms import CompanyLogoForm
from deals.models import Deal, DealBranch, DealResource
from users.models import PasswordRequest
from stats.models import *
from resources.models import Resource, ResourceEntity
from crupzi.utils import slugify_unique, last_day_iso_week, first_day_iso_week
from deals.utils import deal_status
from stats.utils import get_datadiff_percent, get_weekly_general_data
from template_email import TemplateEmail

@csrf_exempt
def deal_crud(request, action):
	if request.user.is_authenticated():
		if request.method=='POST':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				''' Inicio demo'''
				if action != 'get' and request.user.username=='crupzi-demo':
					response = [dict(error=False)]
					json = simplejson.dumps(response)
					return HttpResponse(json, content_type='application/javascript')
				''' Fin demo'''
				if action == 'create' or action == 'update':
					form = request.POST
					valid_form, errors = valid_deal_form(company_user.company, form)
					if valid_form:
						create_branches = True
						create_resources = True
						branches = form.getlist('sel-branch')
						resources = form.getlist('sel-resource')
						company_branch = 'B' if len(branches)>0 else 'C'
						needs_resource = True if len(resources)>0 else False
						error_list = list()
						#name length
						form_title = form.get('txt-name')
						if len(form_title)>50:
							error_list.append('El título del descuento no puede sobrepasar los 50 caracteres.')
						# description length
						form_description = form.get('txt-desc')
						if len(form_description)>160:
							error_list.append('La descripción no puede sobrepasar los 160 caracteres.')
						# restricionts length
						form_restrictions = form.get('txt-restr')
						if form_restrictions:
							if len(form_restrictions)<256:
								restrictions = form_restrictions
								restrictions2 = None
							elif len(form_restrictions)<511:
								restrictions = form_restrictions[:255]
								restrictions2 = form_restrictions[255:]
							else:
								error_list.append('Las restricciones no pueden sobrepasar los 510 caracteres.')
						else:
							restrictions = restrictions2 = None

						if len(error_list)>0:
							errors = list(error_list)
							response = [dict(error=True, errors=errors)]
							json = simplejson.dumps(response)
							return HttpResponse(json, content_type='application/javascript')

						if action =='create':
							isset_end_date = True if form.get('txt-date_end') else False
							isset_percent = True if form.get('txt-percent') else False
							if form.get("sel-company"):
								sel_company = Company.objects.values('name').filter(pk=form.get("sel-company"))[0]
								slug = slugify_unique(sel_company['name']+'-'+form.get('txt-name'), Deal)
							else:
								slug = slugify_unique(company_user.company.name+'-'+form.get('txt-name'), Deal)
							deal = Deal(
									name=form.get('txt-name'), description=form.get('txt-desc'), 
									restrictions = restrictions,
									restrictions2 = restrictions2,
									schedule = form.get('hid-schedule'), company=company_user.company, 
									company_ext_id=form.get("sel-company") or None,
									country = company_user.company.country,
									init_date = timezone.datetime.strptime(form.get('txt-date_ini'), '%d/%m/%Y'), 
									end_date= (timezone.datetime.strptime(form.get('txt-date_end')+str(' 17:59:59'), '%d/%m/%Y %H:%M:%S') if isset_end_date else None),
									verified = True,
									company_branch = company_branch,
									needs_resource = needs_resource,
									percent = (form.get('txt-percent') if isset_percent else None), 
									slug = slug,
									valid=True
								)
							if request.user.username != 'crupzi-demo':
								deal.save()
								deal_id = deal.id
						else:
							try:
								deal_id = form.get('hid-dealid')
								deal = Deal.objects.get(id=form.get('hid-dealid'), company=company_user.company)

								setattr(deal,'name', form.get('txt-name'))
								setattr(deal,'description', form.get('txt-desc'))
								setattr(deal,'restrictions', restrictions)
								setattr(deal,'restrictions2', restrictions2)
								setattr(deal,'schedule', form.get('hid-schedule'))
								setattr(deal,'init_date', datetime.datetime.strptime(form.get('txt-date_ini'), '%d/%m/%Y'))
								if form.get('txt-date_end'):
									setattr(deal,'end_date', datetime.datetime.strptime(form.get('txt-date_end'),'%d/%m/%Y'))
								else:
									setattr(deal,'end_date', None)
								setattr(deal,'percent', form.get('percent'))
								#setattr(deal,'slug', slugify_unique(company_user.company.name+'-'+form.get('txt-name'), Deal))
								setattr(deal,'company_branch', company_branch)
								setattr(deal,'needs_resource', needs_resource)
								setattr(deal,'company_ext_id', form.get("sel-company") or None)
								deal.save()
								current_branches = DealBranch.objects.values_list('branch_id', flat=True).filter(deal=deal)
								current_resources = DealResource.objects.values_list('resource_id', flat=True).filter(deal=deal)
								int_branches = list()
								for item in branches:
									int_branches.append(int(item))
								if list(current_branches) == list(int_branches):
									create_branches = False
								else:
									DealBranch.objects.filter(deal=deal).delete()
								int_resources = list()
								for item in resources:
									int_resources.append(int(item))
								if list(current_resources) == list(int_resources):
									create_resources = False
								else:
									DealResource.objects.filter(deal=deal).delete()
							except Exception,e:
								print e
								if len(error_list)==0:
									errors = list(['El descuento especificado no pudo ser modificado.'])
								else:
									errors = list(error_list)
								response = [dict(error=True, errors=errors)]
								json = simplejson.dumps(response)
								return HttpResponse(json, content_type='application/javascript')
						if create_branches:
							if branches:
								if len(branches)>0:
									for item in branches:
										deal_branch = DealBranch(deal_id=deal.id, branch_id=int(item))
										deal_branch.save()
						if create_resources:
							if resources:
								if len(resources)>0:
									for item in resources:
										deal_resource = DealResource(deal_id=deal.id, resource_id=int(item))
										deal_resource.save()
						response = [dict(error=False, deal_id=deal_id)]
					else:
						response = [dict(error=True, errors=errors)]
					json = simplejson.dumps(response)
					return HttpResponse(json, content_type='application/javascript')

				if action=='delete' or action=='invalidate' or action=='validate':
					deal_id = request.POST.get('deal')
					try:
						deal = Deal.objects.get(id=deal_id, company=company_user.company)
						if action=='invalidate' or action=='validate':
							valid = True if action=='validate' else False
							setattr(deal,'valid',valid)
						else:
							setattr(deal,'deleted',True)
						deal.save()
						response = [dict(error=False)]
					except Deal.DoesNotExist:
						errors = list(('El descuento especificado no fue encontrado en la base de datos.'))
						response = [dict(error=True, errors=errors)]
					json = simplejson.dumps(response)
					return HttpResponse(json, content_type='application/javascript')

				if action == 'get':
					deal_id = request.POST.get('deal')
					try:
						deal = Deal.objects.get(id=deal_id, company=company_user.company)
						branches = DealBranch.objects.values_list('branch_id', flat=True).filter(deal=deal)
						resources = DealResource.objects.values_list('resource_id', flat=True).filter(deal=deal)
						ext_branches = Branch.objects.filter(company=deal.company_ext)
						ext_branches_list = list()
						for item in ext_branches:
							ext_branches_list.append(dict(id=item.id,name=item.name))
						response = [dict(error=False, id=deal.id, name=deal.name,
									schedule=deal.schedule, 
									init_date= deal.init_date.strftime('%d/%m/%Y'),
									end_date=(deal.end_date.strftime('%d/%m/%Y') if deal.end_date else ''), 
									description=deal.description, 
									restrictions=deal.restrictions or '',
									restrictions2=deal.restrictions2 or '',
									company_ext = deal.company_ext_id, ext_branches=ext_branches_list,
									percent=deal.percent, branches=list(branches), resources=list(resources))]
					except Exception, e:
						print e
						errors = list(['El descuento no existe o no tiene acceso a el.'])
						response = [dict(error=True, errors=errors)]
					json = simplejson.dumps(response)
					return HttpResponse(json, content_type='application/javascript')

				if action == 'fbrequest':
					deal_id = request.POST.get('deal')
					try:
						deal = Deal.objects.get(id=deal_id)
						fb_request(deal)
					except Exception, e:
						print e
						pass
					response = [dict(error=False)]
					json = simplejson.dumps(response)
					return HttpResponse(json, content_type='application/javascript')

			except Exception, e:
				print e
				return HttpResponseRedirect('/comercios/login/')
		else:
			if action == 'company_branches':
				try:
					branches = Branch.objects.filter(company_id=request.GET.get('company_id'))
					branches_list = list()
					for item in branches:
						branches_list.append(dict(id=item.id,name=item.name))
					response = [dict(error=False, branches=branches_list)]
				except Exception,e:
					print e
					errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
					response = [dict(error=True, errors=errors)]
				json = simplejson.dumps(response)
				return HttpResponse(json, content_type='application/javascript')
	else:
		return HttpResponseRedirect('/comercios/login/')


def deal_table(request):
	if request.user.is_authenticated():
		try:
			company_user = CompanyUser.objects.get(user=request.user)
			today_date = datetime.date.today()
			deals = Deal.objects.filter(company=company_user.company, deleted=False).order_by('-created_at')
			data = list()
			for deal in deals:
				status = deal_status(deal,True,True)
				end_date = deal.end_date.strftime('%d/%m/%Y') if deal.end_date else ''
				record = list((deal.id, deal.name, status,deal.created_at.strftime('%d/%m/%Y'), end_date, deal.valid, deal.slug))
				data.append(record)
			json = simplejson.dumps(dict(aaData=data))
			return HttpResponse(json, content_type='application/javascript')
		except Exception, e:
			print e
			return HttpResponseRedirect('/comercios/login/')
	else:
		return HttpResponseRedirect('/comercios/login/')


def branch_data(request):
	if request.user.is_authenticated() and request.method=='GET':
		branch_id = request.GET.get('branch_id')
		try:
			branch = Branch.objects.values('name','address','phone_number').get(pk=int(branch_id))
			branch_deals = DealBranch.objects.values_list('deal_id',flat=True).filter(branch_id=int(branch_id))
			branch_deals_str = list()
			for item in branch_deals:
				branch_deals_str.append(str(item))
			branch_deals_str = ', '.join(branch_deals_str)
			if len(branch_deals)>0:
				today_date = datetime.date.today()
				extra_where = [" (('"+str(today_date)+"' between init_date and end_date) or ('"+str(today_date)+"' >= init_date and end_date is null)","id in ("+branch_deals_str+"))"]
				dealList = Deal.objects.values_list('id',flat=True).filter(valid=True, deleted=False).extra(where=extra_where)
				data = [dict(info=branch, deals=list(dealList))]
			else:
				data = [dict(info=branch, deals=False)]
			json = simplejson.dumps(data)
			return HttpResponse(json, content_type='application/javascript')
		except Exception,e:
			print e
			return HttpResponse('', content_type='application/javascript')
	else:
		return HttpResponse('', content_type='application/javascript')


'''
 For dashboard actions...
'''
@csrf_exempt
def companies_data(request,action):
	if request.user.is_authenticated():
		if request.method=='POST':
			if action=='logo':
				try:
					''' Inicio demo '''
					if request.user.username != 'crupzi-demo':
						company_user = CompanyUser.objects.get(user=request.user)
						company = company_user.company
						form = CompanyLogoForm(request.POST)
						if form.is_valid():
							form.save(company,request.FILES)
				except Exception, e:
					print e
				return HttpResponseRedirect('/comercios/dashboard/inicio/')

			else:
				''' Inicio demo '''
				if request.user.username == 'crupzi-demo':
					response = [dict(error=False)]
					json = simplejson.dumps(response)
					return HttpResponse(json, content_type='application/javascript')
				''' Fin demo '''
				if action=='password':
					pwd1 = request.POST.get('txt-businesspwd')
					try:
						user = request.user
						user.set_password(pwd1)
						user.save()
						response = [dict(error=False)]
					except:
						errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
						response = [dict(error=True, errors=errors)]

				if action=='info':
					try:
						company_user = CompanyUser.objects.get(user=request.user)
						company = company_user.company
						setattr(company,'name',request.POST.get('txt-business_name'))
						setattr(company,'address',request.POST.get('txt-business_address'))
						setattr(company,'phone_number',request.POST.get('txt-business_phonenumber'))
						company.save()
						response = [dict(error=False)]
					except:
						errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
						response = [dict(error=True, errors=errors)]

				json = simplejson.dumps(response)
				return HttpResponse(json, content_type='application/javascript')

		if request.method=='GET':
			if action=='info':
				try:
					company_user = CompanyUser.objects.get(user=request.user)
					company = company_user.company
					response = [dict(error=False, name=company.name, phonenumber=company.phone_number, address=company.address)]
				except:
					errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
					response = [dict(error=True, errors=errors)]
				json = simplejson.dumps(response)
				return HttpResponse(json, content_type='application/javascript')

@csrf_exempt
def branches_info(request, action):
	if request.user.is_authenticated():
		''' Inicio demo '''
		if request.user.username == 'crupzi-demo' and action!='table' and action!='info' and action!='list':
			response = [dict(error=False)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')
		''' Fin demo '''
		if action=='table':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				branches = Branch.objects.filter(company=company_user.company).order_by('name')
				data = list()
				for item in branches:
					if item.valid:
						valid_label = '<span class="label label-success">Activa</span>'
					else:
						valid_label = '<span class="label">Inactiva</span>'
					record = list((item.id, item.name, item.valid,valid_label))
					data.append(record)
				json = simplejson.dumps(dict(aaData=data))
				return HttpResponse(json, content_type='application/javascript')
			except Exception, e:
				return HttpResponseRedirect('/comercios/login/')

		if action=='create':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				post = request.POST
				branch = Branch(name=post.get('name'), company=company_user.company,address=post.get('address'), 
					phone_number=post.get('phone_number'), mall_id=post.get('mall') )
				branch.save()
				response = [dict(error=False)]
			except Exception, e:
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='edit':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				post = request.POST
				branch = Branch.objects.get(company=company_user.company, id=post.get('id'))
				setattr(branch,'name',post.get('name'))
				setattr(branch,'address',post.get('address'))
				setattr(branch,'phone_number',post.get('phone_number'))
				setattr(branch,'mall_id', post.get('mall'))
				branch.save()
				response = [dict(error=False)]
			except Exception, e:
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='delete':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				branch_id = request.POST.get('branch_id')
				branch = Branch.objects.get(company=company_user.company, id=branch_id)
				branch.delete()
				response = [dict(error=False)]
			except IntegrityError:
				errors = list(['Ocurrió un problema con su solicitud. Es probable que la sucursal esté asociada a algún descuento.'])
				response = [dict(error=True, errors=errors)]
			except Exception:
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='invalidate' or action=='validate':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				branch_id = request.POST.get('branch_id')
				branch = Branch.objects.get(company=company_user.company, id=branch_id)
				branch.valid = False if action=='invalidate' else True
				branch.save()
				response = [dict(error=False)]
			except Exception:
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')			

		if action=='info':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				branch_id = request.GET.get('branch_id')
				branch = Branch.objects.get(pk=branch_id)
				response = [dict(id=branch.id,name=branch.name, address=branch.address, phonenumber=branch.phone_number, 
					mall=(branch.mall_id or None), error=False)]
			except Exception,e:
				print e
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]

			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='list':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				branches = Branch.objects.filter(company=company_user.company)
				branches_list = list()
				for item in branches:
					branches_list.append(dict(id=item.id,name=item.name))
				response = [dict(error=False, branches=branches_list)]
			except Exception,e:
				print e
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

	else:
		return HttpResponseRedirect('/comercios/login/')


@csrf_exempt
def resources_info(request, action):
	if request.user.is_authenticated():
		''' Inicio demo '''
		if request.user.username == 'crupzi-demo' and action!='table' and action!='info' and action!='list':
			response = [dict(error=False)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')
		''' Fin demo '''
		if action=='table':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				resources = Resource.objects.filter(entity__company_id=company_user.company)
				data = list()
				for item in resources:
					if item.valid:
					 	valid_label = '<span class="label label-success">Activa</span>'
					else:
					 	valid_label = '<span class="label">Inactiva</span>'
					record = list((item.id, item.name, item.valid, valid_label))
					data.append(record)
				json = simplejson.dumps(dict(aaData=data))
				return HttpResponse(json, content_type='application/javascript')
			except Exception, e:
				return HttpResponseRedirect('/comercios/login/')

		if action=='create':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				entity = ResourceEntity.objects.get(company=company_user.company)
				resource = Resource(name=request.POST.get('name'), entity=entity, country_id=company_user.company.country_id)
				resource.save()
				response = [dict(error=False)]
			except Exception, e:
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='edit':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				entity = ResourceEntity.objects.get(company=company_user.company)
				resource = Resource.objects.get(entity=entity, id=request.POST.get('id'))
				setattr(resource,'name',request.POST.get('name'))
				resource.save()
				response = [dict(error=False)]
			except Exception, e:
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='delete':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				entity = ResourceEntity.objects.get(company=company_user.company)
				resource = Resource.objects.get(entity=entity, id=request.POST.get('card_id'))
				resource.delete()
				response = [dict(error=False)]
			except IntegrityError:
				errors = list(['Ocurrió un problema con su solicitud. Es probable que la tarjeta esté asociada a algún descuento.'])
				response = [dict(error=True, errors=errors)]
			except Exception, e:
				print e
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='invalidate' or action=='validate':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				entity = ResourceEntity.objects.get(company=company_user.company)
				resource = Resource.objects.get(entity=entity, id=request.POST.get('card_id'))
				resource.valid = False if action=='invalidate' else True
				resource.save()
				response = [dict(error=False)]
			except Exception:
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')			

		if action=='info':
			try:
				resource = Resource.objects.get(id=request.GET.get('card_id'))
				response = [dict(id=resource.id,name=resource.name, error=False)]
			except Exception,e:
				print e
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]

			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

		if action=='list':
			try:
				company_user = CompanyUser.objects.get(user=request.user)
				entity = ResourceEntity.objects.get(company=company_user.company)
				resources = Resource.objects.filter(entity=entity)
				resources_list = list()
				for item in resources:
					resources_list.append(dict(id=item.id,name=item.name))
				response = [dict(error=False, resources=resources_list)]
			except Exception,e:
				print e
				errors = list(['Ocurrió un problema con su solicitud. Favor intentarlo nuevamente.'])
				response = [dict(error=True, errors=errors)]
			json = simplejson.dumps(response)
			return HttpResponse(json, content_type='application/javascript')

	else:
		return HttpResponseRedirect('/comercios/login/')

def dashboard_page(request, which):
	if request.user.is_authenticated():
		try:
			company = CompanyUser.objects.get(user=request.user)
			try:
				entity = ResourceEntity.objects.get(company=company.company)
			except ResourceEntity.DoesNotExist:
				entity = None
			if which=='inicio':
				today_date = datetime.date.today()
				year, isoweek, isoweekday = (datetime.datetime.today() - datetime.timedelta(days=7)).isocalendar()
				extra_where = [" (('"+str(today_date)+"' between init_date and end_date) or ('"+str(today_date)+"' >= init_date and end_date is null))"]
				current_deals = Deal.objects.filter(company_id=company.company, valid=True, deleted=False).extra(where=extra_where).values('name','init_date','end_date')
				try:
					prints, clicks, saves, anonymous = get_weekly_general_data(company.company, year, isoweek)
				except IndexError:
					prints = clicks = saves = anonymous = 0
				stats_deals = WeeklyGeneral.objects.filter(company=company.company, year=year,week=isoweek).distinct()
				locale.setlocale(locale.LC_TIME,'es_ES')
				first_day_str = (datetime.datetime.strptime(first_day_iso_week(year, isoweek),'%d/%m/%Y')).strftime("%d de %B")
				last_day_str = (datetime.datetime.strptime(last_day_iso_week(year,isoweek),'%d/%m/%Y')).strftime("%d de %B")
				top_stats = {'branches': Branch.objects.filter(valid=True, company=company.company).count(), 
							'deals': len(current_deals), 
							'prints': prints, 'clicks': clicks, 'saves': saves}
				return_dict = {'company': company.company, 'top_stats': top_stats, 'current_deals': current_deals, 
					'stats_deals': stats_deals, 'first_day_str': first_day_str, 'last_day_str': last_day_str, 'entity': entity}
				return render_to_response('dashboard/dashboard-home.html',return_dict, RequestContext(request))
			elif which=='configuracion':
				form = CompanyLogoForm()
				return render_to_response('dashboard/dashboard-settings.html',{'logoForm': form, 'company': company.company, 'entity': entity}, RequestContext(request))				
			elif which=='sucursales':
				branches = Branch.objects.filter(company=company.company)
				malls = Mall.objects.filter(country=company.company.country)
				return render_to_response('dashboard/dashboard-branches.html',{'company': company.company, 'branches': branches, 'malls': malls,'entity': entity}, RequestContext(request))			
			elif which=='descuentos':
				branches = Branch.objects.filter(company=company.company)
				if entity:
					companies = Company.objects.values('id','name').filter(country_id=company.company.country_id, valid=True)
					resources = Resource.objects.filter(country_id=company.company.country_id, entity=entity, valid=True).order_by('entity')
				else:
					companies = None
					resources = Resource.objects.filter(country_id=company.company.country_id, valid=True).order_by('entity')
				deals = Deal.objects.filter(company=company.company, valid=True, deleted=False).order_by('-created_at')
				return render_to_response('dashboard/dashboard-deals.html',{'company': company.company, 'resources': resources, 'deals': deals, 'branches': branches, 'entity': entity, 'companies':companies}, RequestContext(request))
			elif which=='faq':
				return render_to_response('dashboard/dashboard-faq.html',{'company': company.company, 'entity': entity}, RequestContext(request))
			elif which=='tarjetas':
				resources = Resource.objects.filter(entity__company_id=company.company.id, country_id=company.company.country_id)
				return render_to_response('dashboard/dashboard-cards.html',{'company': company.company, 'entity': entity}, RequestContext(request))
			elif which=='estadisticas':
				#search for deals within
				if request.POST.get('stats_date'):
					today = datetime.datetime.strptime(request.POST.get('stats_date'), '%d/%m/%Y')
				else:
					today = datetime.datetime.today() - datetime.timedelta(days=7)
				isocalendar = today.isocalendar()
				isoweek= isocalendar[1]
				year = isocalendar[0]
				try:
					control = StatsControl.objects.filter(company=company.company).order_by('-created_at')[:1][0]
				except:
					control=None
				if control:
					last_year=control.year 
					last_week=control.week
				else:
					last_year=-1 
					last_week=-1
				now = today.timetuple()
				n = 5 #number of previous weeks that are going to be calculated.
				year_month_tuple=[datetime.datetime.fromtimestamp(time.mktime([now.tm_year, now.tm_mon, now.tm_mday-(7*(n+1)), 0, 0, 0, 0, 0, 0])).isocalendar() for n in range(n)]
				if (last_year==year and last_week!=isoweek) or (last_year<year):
					#calculate da ting.
					cursor = connection.cursor()
					for item in year_month_tuple:
						item_year = item[0]
						item_isoweek = item[1]
						try:
							sql = "SELECT update_stats_all(%d, cast(%d as smallint), cast(%d as smallint))" % (company.company.id, item_year, item_isoweek)
							cursor.execute(sql)
							transaction.commit_unless_managed()
						except:
							break
				gral_clicks0 = gral_prints0 = gral_saves0 = 0
				gral_clicks1 = gral_prints1 = gral_saves1 = 0
				year_month_tuple=[datetime.datetime.fromtimestamp(time.mktime([now.tm_year, now.tm_mon, now.tm_mday-(7*(n)), 0, 0, 0, 0, 0, 0])).isocalendar() for n in range(2)]
				for item in year_month_tuple:
					print item
					try:
						stats_data = get_weekly_general_data(company.company, item[0], item[1])
						if item[1]==isoweek:
							gral_prints0+=stats_data[0]
							gral_clicks0+=stats_data[1]
							gral_saves0+=stats_data[2]

						if item[1]==isoweek-1:
							gral_prints1+=stats_data[0]
							gral_clicks1+=stats_data[1]
							gral_saves1+=stats_data[2]
					except Exception, e:
						print e

				gral_prints_percent = get_datadiff_percent(gral_prints0, gral_prints1)
				gral_clicks_percent = get_datadiff_percent(gral_clicks0, gral_clicks1)
				gral_saves_percent = get_datadiff_percent(gral_saves0, gral_saves1)

				gral_prints_max = math.trunc(math.ceil(float(gral_prints_percent)/100)*100)
				gral_clicks_max = math.trunc(math.ceil(float(gral_clicks_percent)/100)*100)
				gral_saves_max = math.trunc(math.ceil(float(gral_saves_percent)/100)*100)
				
				deals_list = WeeklyGeneral.objects.filter(company=company.company, year=year,week=isoweek).values_list('deal_id',flat=True).distinct()
				deals = Deal.objects.filter(id__in=deals_list)
				first_day = first_day_iso_week(year, isoweek)
				last_day = last_day_iso_week(year,isoweek)
				locale.setlocale(locale.LC_TIME,'es_ES')
				lweek = (datetime.datetime.strptime(first_day, '%d/%m/%Y') - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
				nweek = (datetime.datetime.strptime(first_day, '%d/%m/%Y') + datetime.timedelta(days=7)).strftime('%d/%m/%Y')

				first_day_str = (datetime.datetime.strptime(first_day,'%d/%m/%Y')).strftime("%d de %B")
				last_day_str = (datetime.datetime.strptime(last_day,'%d/%m/%Y')).strftime("%d de %B")

				return_dict = {'company': company.company, 'deals':deals, 'first_day': first_day,'first_day_str': first_day_str, 'last_day':last_day, 'last_day_str':last_day_str, 'last_week': lweek, 'next_week':nweek,'gral_prints_percent': gral_prints_percent, 'gral_clicks_percent':gral_clicks_percent, 'gral_saves_percent': gral_saves_percent,'gral_prints_max': gral_prints_max, 'gral_clicks_max':gral_clicks_max, 'gral_saves_max':gral_saves_max, 'entity': entity}

				return render_to_response('dashboard/dashboard-stats.html', return_dict, RequestContext(request))			
		except CompanyUser.DoesNotExist:
			return HttpResponseRedirect('/comercios/login')
	else:
		return HttpResponseRedirect('/comercios/login')


'''
Password reset - december,16th 2012.
'''
@csrf_exempt
def password_reset(request):
	if request.method =='GET':
		uid = request.GET.get('uid')
		key = request.GET.get('key')
		error = message = None
		form = True
		if uid and key:
			form = False
			try:
				password_request = PasswordRequest.objects.get(user_id=uid, key=key)
				if password_request.status == 'P':
					user = User.objects.get(id=uid)
					user.password = password_request.password
					user.save()
					password_request.status='A'
					password_request.save()
					message = "Buenas noticias: la solicitud de cambio de contraseña se ha completado."
				elif password_request.status == 'N':
					error = "Lo sentimos pero esta solicitud de cambio de contraseña no puede ser utilizada. Revisa que no hayas solicitado otro cambio recientemente."
				elif password_request.status == 'A':
					error = "Lo sentimos pero esta solicitd de cambio ya ha sido procesada anteriormente."
			except PasswordRequest.DoesNotExist:
				error = "Lo sentimos, no se encontró ninguna solicitud con las condiciones solicitadas."
		return render_to_response('dashboard/dashboard-passwordreset.html', {'error': error, 'message': message, 'form': form}, RequestContext(request))
	if request.method=='POST':
		#We are supposed to generate the password request and send the email.
		username = request.POST.get('username')
		password = request.POST.get('password')
		errors = None
		try:
			user = User.objects.get(username=username)
			if CompanyUser.objects.filter(user=user).exists():
				if PasswordRequest.objects.filter(user=user, status='P').exists():
					PasswordRequest.objects.filter(user=user, status='P').update(status='N')
				user.set_password(password)
				key = binascii.b2a_hex(os.urandom(32))
				password_request = PasswordRequest(user=user, password=user.password, status='P', key=key)
				password_request.save()
				#send mail.
				context = {'email': user.email, 'key': key, 'uid': user.id}
				email = TemplateEmail(template='mails/password-reset.html', context=context, to = [user])
				email.send()
			else:
				errors = list(['El nombre de usuario ingresado no pertenece a ningún comercio.'])
		except User.DoesNotExist:
			errors = list(['El nombre de usuario ingresado no fue encontrado, revisa que esté digitado correctamente.'])
		if errors:
			response = [dict(error=True, errors=errors)]
		else:
			response = [dict(error=False)]
		json = simplejson.dumps(response)
		return HttpResponse(json, content_type='application/javascript')


@csrf_exempt
def signup_process(request):
	form_data = request.POST
	if request.POST.get('username') == "": #captcha alternative.
		signup = SignupForm()
		for item in form_data:
			setattr(signup,item,request.POST[item])
		try:
			signup.save()
			response = [dict(error=False)]
		except Exception, e:
			print e
			response = [dict(error=True, errors="<li>Se detectaron errores en la petición. Verifica que hayas ingresado los datos correctamente.</li>") ]
	else:
		response = [dict(error=True, errors="<li>&iquest;Eres de verdad o simulado?</li>") ]
	json = simplejson.dumps(response)
	return HttpResponse(json, content_type='application/javascript')
	pass


def business_dashboard(request):
	if request.user.is_authenticated():
		try:
			company = CompanyUser.objects.get(user=request.user)
			resources = Resource.objects.filter(country_id=company.company.country_id, valid=True).order_by('entity')
			branches = Branch.objects.filter(company=company.company)
			malls = Mall.objects.filter(country=company.company.country)
			deals = Deal.objects.filter(company=company.company, valid=True).order_by('-created_at')
			form = CompanyLogoForm()
			return render_to_response('dashboard/dashboard-home.html',{'logoForm': form, 'company': company.company, 'resources': resources, 'branches': branches, 'malls': malls, 'deals': deals}, RequestContext(request))			
		except CompanyUser.DoesNotExist:
			return HttpResponseRedirect('/comercios/login')		
	else:
		return HttpResponseRedirect('/comercios/login')