from django.core.management import setup_environ
from crupzi import settings
from crupzi.utils import slugify_unique
from companies.models import Company

setup_environ(settings)

companies = Company.objects.all()

for company in companies:
	if company.slug is None or deal.slug is '':
		company.slug = slugify_unique(company.name, Company)
		company.save()