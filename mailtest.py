from django.contrib.auth.models import User
from django.core.mail import send_mail
from template_email import TemplateEmail

try:
	user = User.objects.get(pk=17)
	context = {'first_name': user.first_name}
	email = TemplateEmail(template='mails/new-user.html', context=context, to = [user])
	email.send()
except Exception, e:
	print e