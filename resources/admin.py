from django.contrib import admin
from resources.models import ResourceEntityType, ResourceEntity, Resource

admin.site.register(ResourceEntityType)
admin.site.register(ResourceEntity)
admin.site.register(Resource)