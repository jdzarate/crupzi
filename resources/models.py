from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_delete
from users.models import Country
from companies.models import Company
# Create your models here.

class ResourceEntityType(models.Model):
	name = models.CharField(max_length=50)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	def __unicode__(self):
		return self.name

class ResourceEntity(models.Model):
	name = models.CharField(max_length=50)
	type = models.ForeignKey(ResourceEntityType)
	country = models.ForeignKey(Country)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	company = models.ForeignKey(Company, null=True, blank=True)

	def __unicode__(self):
		return self.name

class Resource(models.Model):
	name = models.CharField(max_length=50)
	entity = models.ForeignKey(ResourceEntity)
	logo = models.ImageField(upload_to='/static/', null=True, blank=True)
	country = models.ForeignKey(Country, null=True)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	valid = models.BooleanField(default=True)

	def __unicode__(self):
		return self.entity.name+' - '+self.name


class DeletedResource(models.Model):
	name = models.CharField(max_length=50)
	entity = models.IntegerField()
	country = models.IntegerField()
	created_at = models.DateTimeField(null=True)
	valid = models.BooleanField(default=True)
	deleted_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.entity.name+' - '+self.name

'''-----------
	Signals
------------'''
@receiver(pre_delete, sender=Resource)
def delete_branch_task(sender, instance, **kwargs):
	deleted = DeletedResource(name=instance.name, entity=instance.entity.id, country=instance.entity.country.id, valid=instance.valid,
		created_at=instance.created_at)
	deleted.save()