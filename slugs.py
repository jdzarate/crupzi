from django.core.management import setup_environ
from crupzi import settings
from crupzi.utils import slugify_unique
from deals.models import Deal

setup_environ(settings)

deals = Deal.objects.all()

for deal in deals:
	if deal.slug is None or deal.slug is '':
		deal.slug = slugify_unique(deal.company.name+'-'+deal.name, Deal)
		deal.save()