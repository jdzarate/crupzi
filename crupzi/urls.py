# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

#from users.views import connect, newaccount, invite, invitation_request, complete_registration, account_configuration, account_operations, profile


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'crupzi.views.index', name='index'),
    url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': '/static/images/favicon.ico'}),
    url(r'^apple-touch-icon-precomposed\.png$', 'django.views.generic.simple.redirect_to', {'url': '/static/images/apple-touch-icon-precomposed.png'}),
    url(r'^apple-touch-icon\.png$', 'django.views.generic.simple.redirect_to', {'url': '/static/images/apple-touch-icon.png'}),
    url(r'^testfoursquare6/$', 'crupzi.views.testfoursquare', name='testfoursquare'),
    url(r'^testfoursquare-montesion/$', 'crupzi.views.testfoursquare_montesion', name='testfoursquare_montesion'),
    url(r'^beneficios/$', 'crupzi.views.features', name='features'),
    url(r'^comercios/$', 'crupzi.views.commerce', name='commerce'),
    url(r'^contacto/$', 'crupzi.views.contact', name='contact'),
    url(r'^nosotros/$', 'crupzi.views.us', name='us'),
    url(r'^soporte/$', 'crupzi.views.support', name='support'),
    url(r'^inicio/$', 'crupzi.views.home', name='home'),
    url(r'^tutoriales/$', 'django.views.generic.simple.redirect_to', {'url': 'http://www.youtube.com/playlist?list=PLhaYUqqgOkI3DduxrDZXw4Pt5UlO9guWX'}),
    url(r'^busqueda/$', 'deals.views.mobilesearch', name='mobilesearch'),
    url(r'^in/$', 'crupzi.views.new_user', name='new_user'),
    url(r'^logout/', 'crupzi.views.logout', name='logout'),
    url(r'^send_invitation/', 'crupzi.views.send_invitation', name='send_invitation'),
    url(r'^compartir/', 'crupzi.views.share', name='share'),
    url(r'^mobile/activacion/', 'crupzi.views.mobile_activation', name='mobile_activation'),
    url(r'^mobile/login/', 'crupzi.views.mobile_login', name='mobile_login'),
    url(r'^mobile/(?P<pub>[-\w]+)/solicitud/', 'crupzi.views.mobile_request', name='mobile_request'),
    url(r'^mobile/(?P<pub>[-\w]+)/', 'crupzi.views.mobile_pubs', name='mobile_pubs'),
    url(r'^comercios/connect/', 'crupzi.views.business_connect', name='business_connect'),
    url(r'^comercios/login/', 'crupzi.views.business_login', name='business_login'),
    url(r'^comercios/demo/', 'crupzi.views.business_login_demo', name='business_login_demo'),
    url(r'^comercios/inscripcion/', 'crupzi.views.business_signup', name='business_signup'),
    url(r'^comercios/logout/', 'crupzi.views.business_logout', name='business_logout'),
    url(r'^sendyapi/subscribe/', 'crupzi.views.sendy_subscribe', name='sendy_subscribe'),
    url(r'^sendyapi/unsubscribe/', 'crupzi.views.sendy_unsubscribe', name='sendy_unsubscribe'),
    url(r'^sendynewsletter/subscribe/', 'crupzi.views.sendy_subscribe_newsletter', name='sendy_subscribe_newsletter'),
    url(r'^suscripcion-exitosa-newsletter/', 'django.views.generic.simple.direct_to_template', {'template': 'mails/sendy-subscriptiondoubleopt.html', 'extra_context':  {'page': 'newsletter'} }  ),
    url(r'^suscripcion-exitosa-blog/', 'django.views.generic.simple.direct_to_template', {'template': 'mails/sendy-subscriptiondoubleopt.html', 'extra_context':  {'page': 'blog'}} ),
    url(r'^especiales/(?P<which>[-\w]+)/', 'crupzi.views.specials', name='specials'),
    url(r'', include('social_auth.urls')),
    url(r'', include('fakes.urls')),
    url(r'', include('deals.urls')),
    url(r'', include('companies.urls')),
    url(r'', include('users.urls')),
    url(r'^login/$', 'crupzi.views.login_user', name='crupzi_login'),
    # url(r'^crupzi/', include('crupzi.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
)
