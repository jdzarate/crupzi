# -*- coding: utf-8 -*-
import datetime
import locale
import zlib
import urllib, urllib2
from mobileesp import mdetect
from django.contrib.auth import logout as auth_logout, authenticate, login
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
from users.models import Country, Profile, MobileRequest
from companies.models import Company, CompanyCategory, CompanyUser, Branch, Mall
from companies.forms import CompanyLogoForm
from resources.models import Resource, ResourceEntity
from deals.models import Deal, DealResource, DealBranch
from crupzi.utils import get_schedule, sendy_api_request, sendy_double_opt_subscribe, sendy_api
from template_email import TemplateEmail
from django_mobile import get_flavour


def index(request):
	if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
		return HttpResponseRedirect('/inicio/')
	else:
		#get 3 random deals.
		today_date = datetime.date.today()
		deals_random = Deal.objects.raw("select * from  (select d.id, d.name, random() from deals_deal as d, companies_company as c where d.company_id = c.id and c.valid=true and d.valid=true and d.deleted=false and ( ('"+str(today_date)+"' between init_date and end_date) or ('"+str(today_date)+"' >= init_date and end_date is null)) ) d order by 3 limit 3")
		deal_list = list()
		for d in deals_random:
			deal = Deal.objects.get(id=d.id)
			schedule = get_schedule(deal.schedule)
			schedule_str = ', '.join(schedule)
			deal_resource = DealResource.objects.filter(deal=deal)
			deal_category = CompanyCategory.objects.filter(company=deal.company)
			deal_branch = DealBranch.objects.filter(deal=deal)
			locale.setlocale(locale.LC_TIME,'es_ES')
			if deal.end_date:
				end_date = deal.end_date.strftime("%d de %B de %Y")
			else:
				end_date = None
			deal_list.append(dict(deal=deal, end_date=end_date, schedule=schedule_str, resources=deal_resource, categories=deal_category, branches=deal_branch))
		malls = Mall.objects.filter(country_id=1).order_by('name')
		companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
		resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
		return render_to_response('landing/index.html',{'deals': deal_list,'malls':malls,'companies':companies, 'resources': resources}, RequestContext(request))

def login_user(request):
	if request.user.is_authenticated():
		if CompanyUser.objects.filter(user=request.user).exists():
			return HttpResponseRedirect('/comercios/dashboard/inicio')
		return HttpResponseRedirect('/inicio/')
	else:
		today_date = datetime.date.today()
		print today_date
		deals_random = Deal.objects.raw("select * from  (select id, name, random() from deals_deal where valid=true and deleted=false and (('"+str(today_date)+"' between init_date and end_date) or ('"+str(today_date)+"' >= init_date and end_date is null))) d order by 3 limit 3")
		deal_list = list()
		for d in deals_random:
			deal = Deal.objects.get(id=d.id)
			schedule = get_schedule(deal.schedule)
			schedule_str = ', '.join(schedule)
			deal_resource = DealResource.objects.filter(deal=deal)
			deal_category = CompanyCategory.objects.filter(company=deal.company)
			deal_branch = DealBranch.objects.filter(deal=deal)
			locale.setlocale(locale.LC_TIME,'es_ES')
			if deal.end_date:
				end_date = deal.end_date.strftime("%d de %B de %Y")
			else:
				end_date = None
			deal_list.append(dict(deal=deal, end_date=end_date, schedule=schedule_str, resources=deal_resource, categories=deal_category, branches=deal_branch))
		return render_to_response('landing/login.html',{'deals':deal_list}, RequestContext(request))

def features(request):
	return render_to_response('landing/features.html',{}, RequestContext(request))

def commerce(request):
	return render_to_response('landing/companies.html',{}, RequestContext(request))

def contact(request):
	return render_to_response('landing/contact.html',{'inverse_social':True}, RequestContext(request))

def us(request):
	return render_to_response('landing/us.html',{'inverse_social':True}, RequestContext(request))

def support(request):
	return render_to_response('landing/support.html',{}, RequestContext(request))

def home(request, new_user_modal=False):
	if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
		country_id, display = Profile.objects.values_list('country_id','display').filter(user=request.user)[0]
		notification = False
		user_profile = True
	else:
		user_profile = False
		country_id = 1
		display = None
		new_user_modal = False
		notification = False
	if get_flavour(request)=='mobile' and request.GET.get('query')=='1':
		return HttpResponseRedirect('/busqueda/?'+request.META['QUERY_STRING'])
	if not request.user.is_authenticated():
		if (request.is_mobile or request.is_phone) and not request.is_tablet and request.META['QUERY_STRING'] == "":
			return mobile_login(request)
	countries = Country.objects.values('id','name')
	today_date = datetime.date.today()
	extra_where = [" (('"+str(today_date)+"' between init_date and end_date) or ('"+str(today_date)+"' >= init_date and end_date is null))"]
	deals = Deal.objects.values('id','name','schedule', 'company_id').filter(country_id=country_id, valid=True, deleted=False).extra(where=extra_where).order_by('-saves_count','-click_count')[:5]
	favs = list()
	for deal in deals:
		company = Company.objects.values('name','slug','logo').filter(pk=deal['company_id'])[0]
		category = CompanyCategory.objects.values_list('category_id', flat=True).filter(company_id=deal['company_id'])
		schedule = get_schedule(deal['schedule'])
		schedule_str = ', '.join(schedule)
		favs.append(
			dict([('id',deal['id']),('name',deal['name']),('schedule',schedule_str),
				('company',company.get('name')),('category',category),('company_slug',company.get('slug')), ('company_logo', company.get('logo')) ])
		)
	malls = Mall.objects.filter(country_id=1).order_by('name')
	companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
	resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
	return render_to_response('app/app-home.html',{'countries':countries,'user_country':country_id,'display': display, 'favs': favs, 'is_tablet': request.is_tablet or False,
		'malls':malls,'companies':companies, 'resources': resources, 'new_user': new_user_modal, 'notification': notification, 'user_profile': user_profile, 'page':'home'}, RequestContext(request))

def new_user(request, backend=None):
	return home(request, True)

def business_login(request):
	if request.user.is_authenticated():
		if CompanyUser.objects.filter(user=request.user).exists():
			if request.user.username!='crupzi-demo':
				return HttpResponseRedirect('/comercios/dashboard/inicio')
			else:
				auth_logout(request)
	return render_to_response('dashboard/dashboard-login.html',{}, RequestContext(request))

def business_login_demo(request):
	if request.user.is_authenticated():
		if CompanyUser.objects.filter(user=request.user).exists():
			return HttpResponseRedirect('/comercios/dashboard/inicio')
	return render_to_response('dashboard/dashboard-login.html',{'demo': True}, RequestContext(request))	

def business_signup(request):
	return render_to_response('landing/companies-signup.html',{}, RequestContext(request))	

def business_logout(request):
	logout_url = '/comercios/demo' if request.user.username=='crupzi-demo' else '/comercios/login'
	auth_logout(request)
	return HttpResponseRedirect(logout_url)

def business_connect(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				try:
					company = CompanyUser.objects.get(user=user)
					login(request, user)
					return HttpResponseRedirect('/comercios/dashboard/inicio')
				except CompanyUser.DoesNotExist:
					error = 'No existe una compania asociada su cuenta de usuario.'
			else:
				error = 'Su cuenta de usuario se encuentra desactivada'
		else:
			error = 'La combinacion usuario/contrasena ingresada no existe.'
	return render_to_response('dashboard/dashboard-login.html',{'username': username, 'error': error}, RequestContext(request))

def business_dashboard_stats(request):
	return render_to_response('dashboard/dashboard-stats-gral.html',{}, RequestContext(request))

def logout(request):
	auth_logout(request)
	return HttpResponseRedirect('/')

@csrf_exempt
def send_invitation(request):
	if request.user.is_authenticated():
		if request.POST.get('email-url') or request.POST.get('email-url')!='':
			errors = list(['Se encontraron errores en la solicitud.'])
			response = [dict(error=True, errors=errors)]
		else:
			try:
				email_from = "Crupzi.com <info@crupzi.com>"#request.user.email
				email_to = request.POST.get('email-to').split(',')
				email_msg = request.POST.get('email-msg')
				context = {'from_user': request.user.first_name+' '+request.user.last_name, 'user_message': email_msg}
				email = TemplateEmail(template='mails/email-share.html', context=context, to=email_to, from_email=email_from)
				email.send()
				response = [dict(error=False)]
			except Exception,e:
				print e
				errors = list(['Se encontraron errores en la solicitud.'])
				response = [dict(error=True, errors=errors)]
		json = simplejson.dumps(response)
		return HttpResponse(json, content_type='application/javascript')


def testfoursquare(request):
	return render_to_response('pruebafoursquare.html',{}, RequestContext(request))

def testfoursquare_montesion(request):
	return render_to_response('pruebafoursquare2.html',{}, RequestContext(request))


# def mobilehome(request):
# 	if request.user.is_authenticated():
# 		if CompanyUser.objects.filter(user=request.user).exists():
# 			return HttpResponseRedirect('/comercios/dashboard/inicio')
# 		countries = Country.objects.values('id','name')
# 		malls = Mall.objects.filter(country_id=1).order_by('name')
# 		companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
# 		resources = Resource.objects.filter(country_id=1).order_by('entity')
# 		return render_to_response('mobile/home2.html',{'page':'home','countries':countries, 'malls':malls,'companies':companies, 'resources': resources}, RequestContext(request))
# 	else:
# 		return HttpResponseRedirect('/')

@csrf_exempt
def sendy_subscribe(request):
	if request.POST.get('email') and request.POST.get('list'):
		response = sendy_api_request('subscribe', request.POST.get('email'), request.POST.get('list'))
	else:
		response= 'false'
	return HttpResponse(response)

@csrf_exempt
def sendy_unsubscribe(request):
	if request.POST.get('email') and request.POST.get('list'):
		response = sendy_api_request('unsubscribe', request.POST.get('email'), request.POST.get('list'))
	else:
		response = 'false'
	return HttpResponse(response)

@csrf_exempt
def sendy_subscribe_newsletter(request):
	if request.POST.get('email'):
		if request.POST.get('message') == "": #captcha alternative.
			response = sendy_double_opt_subscribe(request.POST.get('username'), request.POST.get('email'), 6)
			response = 'false' if str(response) is not '1' else response
		else:
			response = 'false'
	else:
		response = 'false'
	return HttpResponse(response)

def share(request):
	return render_to_response('landing/share.html',{}, RequestContext(request))


def specials(request, which):
	if which=='mothersday':
		return render_to_response('specials/mothersday.html',{}, RequestContext(request))

def mobile_pubs(request, pub):
	pub_list = ['economista','elmundo','mediolleno','ella','eldiariodehoy','laprensagrafica','communitiesdna','punto105','invitaciones', 'vip']
	if pub in pub_list:
		user_agent = request.META.get("HTTP_USER_AGENT")
		http_accept = request.META.get("HTTP_ACCEPT")
		is_tablet = is_phone = is_mobile = False
		if user_agent and http_accept:
			agent = mdetect.UAgentInfo(userAgent=user_agent, httpAccept=http_accept)
			is_tablet = agent.detectTierTablet()
			is_phone = agent.detectTierIphone()
			is_mobile = is_tablet or is_phone or agent.detectMobileQuick()
		if (is_phone or is_mobile) and not is_tablet and not request.GET.get('desktop'):
			template = 'landing/mobile_invitation_mobile.html'
		else:
			template = 'landing/mobile_invitation.html'
		return render_to_response(template,{'pub_slug': pub, 'gral_invitation': (pub=='invitaciones' or pub=='vip')}, RequestContext(request))
	else:
		raise Http404

@csrf_exempt
def mobile_request(request, pub):
	msg = errors = ''
	email = request.POST.get('email').lower()
	try:
		validate_email(email)
		try:
			if not MobileRequest.objects.filter(email=email).exists():
				request = MobileRequest()
				request.email = email
				if pub != 'invitaciones':
					request.pub = pub
				request.checksum = str(zlib.adler32(email))
				request.save()
				post_data = [('name', email),('email',email),('list',str(8)), ('boolean','true'), 
					('Checksum', request.checksum)]
				result = urllib2.urlopen('http://www.crupzi.com/sendy/subscribe', urllib.urlencode(post_data))
				content = result.read()
			msg = "Ahora solo te resta ingresar a Crupzi.com desde tu teléfono celular."
		except Exception, e:
			print e
			errors =  "Lo sentimos. Error no Identificado. Inténtalo nuevamente."
	except Exception:
		errors = "Me temo que debes ingresar una dirección de correo con formato adecuado."
	is_error = True if errors!='' else False
	json = simplejson.dumps([dict(error=is_error, errors=errors, msg=msg)])
	return HttpResponse(json, content_type='application/javascript')

def mobile_login(request, activation=False, valid_user=True):
	template = 'landing/mobile_signin.html'
	return render_to_response(template,{'activation': activation, 'valid_user': valid_user}, RequestContext(request))

def mobile_activation(request):
	user_agent = request.META.get("HTTP_USER_AGENT")
	http_accept = request.META.get("HTTP_ACCEPT")
	is_tablet = is_phone = is_mobile = False
	if user_agent and http_accept:
		agent = mdetect.UAgentInfo(userAgent=user_agent, httpAccept=http_accept)
		is_tablet = agent.detectTierTablet()
		is_phone = agent.detectTierIphone()
		is_mobile = is_tablet or is_phone or agent.detectMobileQuick()
	checksum = request.GET.get('key')
	valid_user = MobileRequest.objects.filter(checksum=checksum).exists()
	valid_device = (is_phone or is_mobile) and not is_tablet
	if valid_user and valid_device:
		MobileRequest.objects.filter(checksum=checksum, active=False).update(active=True)
		if request.GET.get('email'):
			try:
				user = User.objects.get(email=request.GET.get('email'), password="!")
				params = [('email', user.email), ('list',9), ('name', user.first_name.encode('utf-8'))]
				sendy_api("http://www.crupzi.com/sendy/subscribe", params)
			except Exception, e:
				print e
				pass
	if not valid_device:
		return render_to_response('landing/mobile_invitation.html',{'pub_slug': False, 'login_page': True, 'valid_user': True}, RequestContext(request))
	else:
		if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
			return HttpResponseRedirect('/inicio/')
		else:
			return mobile_login(request, True, valid_user)