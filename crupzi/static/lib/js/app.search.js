/*Form Search*/
var serializedQuery;

function searchAction(){
    if(validateForm($('#form-search'))){
        if(selectedTab!='3' || selectedCategory!='0'){
            $('a[href="#cat-0"]', '#ul-categories').click();
            $('a[href="#tab-3"]', '#ul-deals').click();
        }
        getResults(1,false);
    }
    else
        alert('Debe especificar al menos un filtro.');
}

$('#btn-search').click(function(){
	searchAction();
});

$('#btn-search-user').click(function(){
    //$('#form-search').submit();
    if(validateForm($('#form-search'))){
        if(selectedTab!='3' || selectedCategory!='0'){
            $('a[href="#cat-0"]', '#ul-categories').click();
            $('a[href="#tab-3"]', '#ul-deals').click();
        }
        getResults(1,false,true);
    }
    else
        alert('Debe especificar al menos un filtro.');
});

$('#btn-clear').click(function(){
    $('#li-search').hide();
    $('a[href="#cat-0"]', '#ul-categories').click();
    $('a[href="#tab-1"]', '#ul-deals').click();
	clearForm($('#form-search'));
});

function clearForm(form){
	$(form).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            	$(this).val('-1');
            	break;
            case 'select-one':
            	$(this).val('-1');
            	break;
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
    $("#search-resource").trigger("liszt:updated");
    $("#search-category").trigger("liszt:updated");
    $("#search-company").trigger("liszt:updated");
    $("#search-mall").trigger("liszt:updated");
}

function validateForm(form){
	var filter = false;
	$(form).find(':input:not(.default)').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                if(jQuery.trim($(this).val())!='')
                	filter=true;
                break;
        }
    });
    return filter;
}

function getResults(page, append, user){
	if(user)
        $('#hid-user').val('1');
    if(!append)
		serializedQuery = $('#form-search').serialize();
	jQuery.ajax({
  		type: "GET",
  		url: "/search/get/"+page+"/",
  		data: serializedQuery,
		beforeSend: function(){
	  		$('button').attr('disabled',true);
  		},
	  	success: function(html){
	  		$('button').attr('disabled',false);
	  		if(html){
	  			if(append){
	  				$('button[name^="btn-next-deals-"]', '#timeline-tab-3').hide();
	  				$('.timeline-container','#timeline-tab-3').append(html);
	  			}
	  			else{
	  				$('.timeline-container','#timeline-tab-3').html(html);
                    $('#li-search').show();
                }

	  		}
	  		$('.deal-social-fake','.deal-social', '#timeline-tab-3').on("click hover",function(){
                var isthis = $(this).parent();
                jQuery.ajax({
                    type: "GET",
                    url: "/deals/get/social/"+$(this).parent().attr("data-id")+"/",
                    beforeSend: function(){
                        $('button').attr('disabled',true);
                        $('#loader').show();
                    },
                    success: function(html){
                        $('button').attr('disabled',false);
                        $('#loader').hide();
                        if(html){
                            isthis.html(html);
                            twttr.widgets.load();
                            FB.XFBML.parse(isthis[0]);
                        }
                    }
                });
            });
	  	}
	});
}

$("#text-filter").keyup(function () {
    var filter = $(this).val(), count = 0;
    $(".deal-container","#timeline-tab-3").each(function () {
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).addClass("hidden");
        } else {
            $(this).removeClass("hidden");
            count++;
        }
    });
    $("#filter-count").text(count);
});