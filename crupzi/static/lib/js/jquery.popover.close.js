var isVisible = false;
var clickedAway = false;

$('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'manual'
    }).click(function(e) {
        $(this).popover('show');
        clickedAway = false
        isVisible = true
        e.preventDefault()
          $('.popover').bind('click',function() {
              clickedAway = false
          });
    });

$(document).click(function(e) {
  if(isVisible && clickedAway)
  {
    $('[data-toggle="popover"]').popover('hide')
    isVisible = clickedAway = false
  }
  else
  {
    clickedAway = true
  }
});