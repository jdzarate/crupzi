$.extend({
  getUrlVars: function(){
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for(var i = 0; i < hashes.length; i++)
      {
        hash = hashes[i].split('=');
        if(vars[hash[0]]){
          if( Object.prototype.toString.call( vars[hash[0]] ) === '[object Array]' ) 
            vars[hash[0]].push(hash[1]);
          else
            vars[hash[0]] = new Array(vars[hash[0]],hash[1]);
        }
        else{
          vars.push(hash[0]);
          vars[hash[0]] = hash[1];
        }
      }
      return vars;
    },
    getUrlVar: function(name){
      return $.getUrlVars()[name];
    }
});