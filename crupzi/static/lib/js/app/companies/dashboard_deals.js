var dealTable, mod_create = true;
var selectedDealStats;
var html_branches = $("#sel-branch").html();

$('#modal-deal').modal({
        backdrop: true,
        keyboard: true,
        show: false
    }).css({
       'width': function () { 
           return ($(document).width() * .7) + 'px';  
       },
       'margin-left': function () { 
           return -($(this).width() / 2); 
       }
});


$('#txt-date_ini,#txt-date_end').val(today());
$('#sel-branch, #sel-resource').chosen({allow_single_deselect: true, width:"50%"});

function loadBranchCombo(options){
	for(var i=0; i<options.length;i++)
    	$('#sel-branch').append("<option value='"+options[i].id+"'>"+options[i].name+"</option>");
}

if($('#sel-company')[0].type!="hidden"){
	$('#sel-company').chosen({allow_single_deselect: true, width:"20%"});
	$("#sel-company").chosen().change(function(){
		if($("#sel-company").val()==""){
			$("#sel-branch").html(html_branches);
			$("#sel-branch").trigger("liszt:updated");
		}
		else{
			jQuery.ajax({
		  		type: "GET",
		  		url: "/comercios/deals/company_branches/",
		  		data: 'company_id='+$("#sel-company").val(),
				beforeSend: function(){
			  		$('button').attr('disabled',true);
			  		$('#loader').show();
		  		},
			  	success: function(json){
			  		$('#loader').hide();
			  		$('button').attr('disabled',false);
			  		var data = eval(json);
			  		$("#sel-branch").html("");
			  		if (data[0].error==false){
			  			options = data[0].branches;
			  			loadBranchCombo(options);
			  		}
			  		$("#sel-branch").trigger("liszt:updated");
			  	}
			});
		}
	});
}


$('#txt-name').charCount({allowed: 50, limit:50});
$('#txt-desc').charCount({allowed: 160, limit:160});
$('#txt-restr').charCount({allowed: 510, limit:510});




dealTable = $('#table-deals').dataTable({
	//"sDom": "<'row-fluid'<'span6'l><'span6'f>r><'toolbar'>t<'row-fluid'<'span12'i><'span12 center'p>>",
	//"sDom": "<<l>r>t<'row-fluid'<'span12'i><'span12 center'p>>",
	"sDom": "<'row-fluid'<'span6'<'toolbar'>><'span6'f>r>t<'row-fluid'<'span12'i><'span12 center'p>>",
	'bProcessing': true,
	'bAutoWidth':false,
	"oLanguage": {
        "sUrl": "/static/lib/utils/dataTables.spanish.txt"
    },
    "aaSorting": [[3,'desc']],
	"sPaginationType": "full_numbers",
	"bJQueryUI": true,
	'bFilter': true,
	"bInfo": false,
	'sAjaxSource': '/comercios/deals-table/',
	'aoColumns': [ 
			/* id */   { "bSearchable": false,  "bVisible":    false },
			/* name */  null,
			/* status */  null,
			/* created at */  { "sType": "date-eu"  },
			/* end date */  { "sType": "date-eu" },
			/* valid */   { "bSearchable": false,  "bVisible":    false },
			/* slug */   { "bSearchable": false,  "bVisible":    false },
		],
	'fnInitComplete': function(oSettings, json) {
		$("#table-deals tbody tr").click( function( e ) {
	        if ( $(this).hasClass('row_selected') ) {
	            $(this).removeClass('row_selected');
	            //$('#div-options-selected').hide();
	            $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').addClass('button-inactive');
	            $("#deal-additionalinfo").hide();
	        }
	        else {
	            dealTable.$('tr.row_selected','#table-deals').removeClass('row_selected');
	            $(this).addClass('row_selected');
	            //$('#div-options-selected').show();
	            $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').removeClass('button-inactive');
	            var anSelected = fnGetSelected( dealTable );
	            var is_valid = dealTable.fnGetData(anSelected[0],5); //5: is_valid column (boolean)
	            if(is_valid){
	            	$('#btn-inactivate').show();
	            	$('#btn-activate').hide();
	            }
	            else{
	            	$('#btn-inactivate').hide();
	            	$('#btn-activate').show();	
	            }
	            if(dealTable.fnGetData(anSelected[0],6)!=""){
		            $("#deal-additionalinfo").show();
		            $("#dealinfo-slug").html("http://www.crupzi.com/descuentos/"+dealTable.fnGetData(anSelected[0],6));
		            $("#dealinfo-href").attr('href',"http://www.crupzi.com/descuentos/"+dealTable.fnGetData(anSelected[0],6));
	            }
	        }
	    });

		$('.dataTables_filter input').attr("placeholder", "Buscar").css('width','218px');
		$("#table-deals_filter label").contents().first().remove();

	    var html = fnCreateSelect(['Vigente','Expirado','Inactivado']);
	  	$("div.toolbar").html(html).css('margin-top','10px');
	  	$("div.toolbar").html(html).css('margin-left','10px');

		$('select', "div.toolbar").change( function () {
			dealTable.fnFilter( $(this).val(), 2 );
			if($(this).val() == "") $(this).addClass("empty");
    		else $(this).removeClass("empty")
		});
		$('select', "div.toolbar").uniform();
		$('#table-deals_filter').css('margin-right','0');
	}
});


$.extend( $.fn.dataTableExt.oStdClasses, {
	    "sWrapper": "dataTables_wrapper form-inline"
	} );

function fnCreateSelect( aData ){
	var r='<select id="deal-state-filter" style=""><option value="">Filtrar por estado</option>', i, iLen=aData.length;
	for ( i=0 ; i<iLen ; i++ )
	{
		r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
	}
	return r+'</select>';
}

/*$("tfoot th").each( function ( i ) {
	if (i==1){
		this.innerHTML = fnCreateSelect(['Vigente','Expirado','Inactivado']);
		$('select', this).change( function () {
			dealTable.fnFilter( $(this).val(), 2 );
		} );
	}
	
} );*/

function fnGetSelected(table){
	return table.$('tr.row_selected');
}

function today(){
	var currentTime = new Date();
	var month = ("0" + (currentTime.getMonth() + 1)).slice(-2);
	var day = ("0" + (currentTime.getDate())).slice(-2);
	var year = currentTime.getFullYear();
	return day + "/" + month + "/" + year;
}

function validateDealForm(){
	var intRegex = /^\d{1,3}$/;
	var errors='';
	if(jQuery.trim($('#txt-name').val())=='')
		errors+="<li>El nombre del descuento es requerido.</li>";
	if($('input[id^="chk-"]:checked').length==0)
		errors+="<li>Debe calendarizar al menos un d&iacute;a de la semana.</li>";
	if(jQuery.trim($('#txt-date_ini').val())=='')
		errors+="<li>La fecha de inicio es requerida.</li>";
	if(jQuery.trim($('#txt-percent').val())!='')
		if($('#txt-percent').val().search(intRegex)==-1)
			errors+="<li>El porcentaje de descuento debe ser un n&uacute;mero entre 0 y 100.</li>";
		else
			if(parseInt($('#txt-percent').val()) >100)
				errors+="<li>El porcentaje de descuento debe ser un n&uacute;mero entre 0 y 100.</li>";
	return errors;
	//Validar que ingrese fechas validas
	//Validar que si ingresa fecha final, esta sea mayor que la inicial.
}

function clearDealForm(){
	$('#form-deal').find(':input').each(function() {
        switch(this.type) {
            case 'select-multiple':
            	$(this).val('-1');
            	break;
            case 'select-one':
            	$(this).val('-1');
            	break;
            case 'password':
            case 'text':
            case 'textarea':
            case 'hidden':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
    $("#sel-branch").html(html_branches);
    $("#sel-branch").trigger("liszt:updated");
    if($('#sel-company')[0].type!="hidden")
    	$("#sel-company").trigger("liszt:updated");
    $("#sel-resource").trigger("liszt:updated");
    $.uniform.update('input[id^="chk-"]');
    $('#header-dealForm').html('<i class="fa-icon-plus"></i><span class="break"></span>&nbsp;&nbsp;Nuevo descuento');
    $('#msg-errors').html('').hide();
}

$('#btn-new').click(function(){
	newDeal();
});

$('#btn-save').click(function(){
	var errors = validateDealForm();
	$('#msg-errors').hide();
	if(jQuery.trim(errors)==''){
		$('#hid-schedule').val(getSchedule());
		saveDeal((mod_create?'create':'update'));
	}
	else{
		$('#msg-errors').html(errors).show();
	}
});

$('#btn-clear').click(function(){
	clearDealForm();
});

$('#btn-cancel').click(function(){
	cancelDealForm();
});

function cancelDealForm(){
	clearDealForm();
	//$('#modal-deal').modal('hide');
	//$('#deal-form-container').hide();
	//$('#deal-left').show();
	$('#box-deals-form').hide();
	$('#box-deals-container').show();
	
}

function newDeal(){
	clearDealForm();
	//$('#deal-left').hide();
	//$('#deal-form-container').show();
	//$('#modal-deal').modal('show');
	$('#box-deals-container').hide();
	$('#box-deals-form').show();
	mod_create = true;
}

function getSchedule(){
	var days = new Array('mon','tue','wed','thu','fri','sat','sun');
	var schedule ='';
	for (var i=0;i<7;i++){
		if($('#chk-'+days[i]).is(':checked'))
			schedule+='1';
		else
			schedule+='0';
	}
	return schedule;
}

function distributeSchedule(schedule){
	var days = new Array('mon','tue','wed','thu','fri','sat','sun');
	var chr;
	for (var i=0;i<7;i++){
		chr = schedule.charAt(i);
		if (chr == '1') $('#chk-'+days[i]).attr('checked', true);
	}
	$.uniform.update('input[id^="chk-"]');
}

function saveDeal(action){
	var formData = $('#form-deal').serialize();
	jQuery.ajax({
  		type: "POST",
  		url: "/comercios/deals/"+action+"/",
  		data: formData,
		beforeSend: function(){
	  		$('button').attr('disabled',true);
	  		$('#loader').show();
  		},
	  	success: function(json){
	  		$('button').attr('disabled',false);
	  		$('#loader').hide();
	  		var data = eval(json);
	  		if (data[0].error == true){
	  			var html = '';
	  			var errors = data[0].errors;
	  			for (key in errors){
	  				html+="<li>"+errors[key]+"</li>";
	  				$('#msg-errors').html(html).show();
	  			}
	  		}
	  		else{
	  			$('#msg-success').html('El registro fue guardado correctamente.').show();
	  			setTimeout("$('#msg-success').hide('slow');", 4000);
	  			cancelDealForm();
	  			if(action=='create'){
	  				fbDealRequest(data[0].deal_id);		
	  			}
	  			else
	  				dealTable.fnReloadAjax(null,reloadTableCallback);
	  		}
	  	}
	});
}

function fbDealRequest(deal){
	if(deal){
		jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/deals/fbrequest/",
	  		data: 'deal='+deal,
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		$('#loader').hide();
		  		dealTable.fnReloadAjax(null,reloadTableCallback);
		  		$('button').attr('disabled',false);
		  	}
		});
	}
}


function invalidateDeal(){
	$('#modal-invalidateConfirm').modal('hide');
	dealOperation('invalidate','El descuento fue invalidado correctamente');
}

function validateDeal(){
	$('#modal-validateConfirm').modal('hide');
	dealOperation('validate','El descuento fue validado correctamente');
}

function deleteDeal(){
	$('#modal-deleteConfirm').modal('hide');
	dealOperation('delete','El descuento fue eliminado correctamente.');
}

function dealOperation(action, message){
	var anSelected = fnGetSelected( dealTable );
    if ( anSelected.length !== 0 ) {
    	 var deal = dealTable.fnGetData(anSelected[0],0);
    	 jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/deals/"+action+"/",
	  		data: 'deal='+deal,
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		$('#loader').hide();
		  		$('button').attr('disabled',false);
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#msg-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			$('#msg-success').html(message).show();
	  				setTimeout("$('#msg-success').hide('slow');", 4000);
	  				dealTable.fnReloadAjax(null,reloadTableCallback);
	  				//$('#div-options-selected').hide();
		  			cancelDealForm();
		  		}
		  	}
		});
    }
}

function reloadTableCallback(){
	$("#table-deals tbody tr").click( function( e ) {
	    if ( $(this).hasClass('row_selected') ) {
	            $(this).removeClass('row_selected');
	            //$('#div-options-selected').hide();
	            $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').addClass('button-inactive');
	            $("#deal-additionalinfo").hide();
        }
        else {
            dealTable.$('tr.row_selected','#table-deals').removeClass('row_selected');
            $(this).addClass('row_selected');
            //$('#div-options-selected').show();
            $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').removeClass('button-inactive');
            var anSelected = fnGetSelected( dealTable );
            var is_valid = dealTable.fnGetData(anSelected[0],5); //5: is_valid column (boolean)
            if(is_valid){
            	$('#btn-inactivate').show();
            	$('#btn-activate').hide();
            }
            else{
            	$('#btn-inactivate').hide();
            	$('#btn-activate').show();	
            }
            if(dealTable.fnGetData(anSelected[0],6)!=""){
	            $("#deal-additionalinfo").show();
	            $("#dealinfo-slug").html(dealTable.fnGetData(anSelected[0],6));
	            $("#dealinfo-href").attr('href',dealTable.fnGetData(anSelected[0],6));
        	}
        }
	});
}

function loadEditForm(){
	var anSelected = fnGetSelected( dealTable );
    if ( anSelected.length !== 0 ) {
    	var deal = dealTable.fnGetData(anSelected[0],0);
    	jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/deals/get/",
	  		data: 'deal='+deal,
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			html+="<li>"+errors+"</li>";
		  			$('#msg-errors').html(html).show();
		  		}
		  		else{
		  			clearDealForm();
		  			mod_create = false;
		  			//Complete form.
		  			$('#hid-dealid').val(data[0].id);
		  			$('#txt-name').val(data[0].name);
		  			$('#txt-desc').val(data[0].description);
		  			$('#txt-restr').val(data[0].restrictions+data[0].restrictions2);
		  			$('#txt-date_ini').val(data[0].init_date);
		  			$('#txt-date_end').val(data[0].end_date);
		  			$('#txt-percent').val(data[0].percent);
		  			$("#sel-resource").val(data[0].resources);
		  			$("#sel-branch").val(data[0].branches);
		  			$("#sel-company").val(data[0].company_ext);
		  			if($('#sel-company')[0].type!="hidden"){
		  				if($("#sel-company").val()==""){
							$("#sel-branch").html(html_branches);
							$("#sel-branch").trigger("liszt:updated");
						}
						else{
			  				$("#sel-branch").html("");
				  			options = data[0].ext_branches;
				  			loadBranchCombo(options);
				  			$("#sel-branch").val(data[0].branches);
			  				$("#sel-company").trigger("liszt:updated");	
		  				}
		  			}
		  			distributeSchedule(data[0].schedule);
					$('#header-dealForm').html('<i class="fa-icon-edit"></i><span class="break"></span>&nbsp;&nbsp;'+data[0].name);
					$("#sel-branch").trigger("liszt:updated");
    				$("#sel-resource").trigger("liszt:updated");
					//$('#deal-left').hide();
					//$('#modal-deal').modal('show');
					$('#box-deals-container').hide();
					$('#box-deals-form').show();

		  		}
		  	}
		});
    }
    else{
    	noSelectionMessage();
    }
}

function confirmInvalidate(){
	var anSelected = fnGetSelected( dealTable );
    if ( anSelected.length !== 0 ) {
    	$('#modal-invalidateConfirm').modal('show');
    }
    else noSelectionMessage();
}

function confirmValidate(){
	var anSelected = fnGetSelected( dealTable );
    if ( anSelected.length !== 0 ) {
    	$('#modal-validateConfirm').modal('show');
    }
    else noSelectionMessage();
}

function confirmDelete(){
	var anSelected = fnGetSelected( dealTable );
    if ( anSelected.length !== 0 ) {
    	$('#modal-deleteConfirm').modal('show');
    }
    else noSelectionMessage();

}

function noSelectionMessage(){
	$('#msg-block').show('slow');
	setTimeout("$('#msg-block').hide('slow');", 4000);
}

/*var datatable_update_size = function() {
	$(dealTable).css({ width: $('#content').parent().width()});
	dealTable.fnAdjustColumnSizing();  
}

*/

$(window).resize(function() {
	var width = $('#dealform-thirdcolumn').width();
});