/*
	Info branches from business dashboard.
*/
$('#sel-branch_mall').chosen({width:'90%'});

function branch_newBranch(){
	$('#h3-branch').html('Nueva sucursal');
	$('#branch-formcontainer').show();
	$('#branch-info').hide();
}

$('#btn-branch-cancel').click(function(){
	//clearform
	cancelBranchInfo();
});

function clearBranchForm(){
	$('#form-branches').find(':input').each(function() {
		$(this).val('');
	});
	$('#branch-formtitle').html('Nueva sucursal');
}

function cancelBranchInfo(){
	$('#branch-formcontainer').hide();
	$('#branch-info').show();
	$('#branch-form-errors').html('').hide();
	$('#msg-branch-errors').html('').hide();
	clearBranchForm();
}

function getFormBranchAction(){
	var action='create';
	if($('#txt-branch_id').val()!='')
		action = 'edit';
	return action;
}

$('#btn-branch-save').click(function(){
	if(validateBranchForm()){
		action = getFormBranchAction();
		var formData = $('#form-branches').serialize();
		jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/branch/"+action+"/",
	  		data: formData,
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#branch-form-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			$('#msg-branch-success').html('La informaci&oacute;n se guard&oacute; correctamente.').show();
		  			setTimeout("$('#msg-branch-success').hide('slow');", 4000);
		  			branchesTable.fnReloadAjax(null,reloadBranchTableCallback);
		  			setBranchOptions();
		  			cancelBranchInfo();
		  		}
		  	}
		});
	}
});

function reloadBranchTableCallback(){
	//$('#branch-edit-container').hide();
	$("#table-branches tbody tr").click( function( e ) {
	    if ( $(this).hasClass('row_selected') ) {
	        $(this).removeClass('row_selected');
	        //$('#branch-edit-container').hide();
	        $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').addClass('button-inactive');
	    }
	    else {
	        branchesTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        //$('#branch-edit-container').show();
	        $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').removeClass('button-inactive');
	        var anSelected = fnGetSelected( branchesTable );
            var is_valid = branchesTable.fnGetData(anSelected[0],2); //5: is_valid column (boolean)
            if(is_valid){
            	$('#btn-inactivate').show();
            	$('#btn-activate').hide();
            }
            else{
            	$('#btn-inactivate').hide();
            	$('#btn-activate').show();	
            }
	    }
	});
}

function branchTableSelection(anSelected){

}

function validateBranchForm(){
	var errors='';
	if(jQuery.trim($('#txt-branch_name').val())==''){
		errors += '<li>El nombre es requerido.</li>';
	}
	if(errors.length!=0){
		$('#branch-form-errors').html(errors).show();
		return false;
	}
	else
		return true;
}

function getSelectedBranch(){
	var branch, anSelected = fnGetSelected( branchesTable );
    if ( anSelected.length !== 0 )
    	 branch = branchesTable.fnGetData(anSelected[0],0);
    return branch;
}

function editBranch(){
	var branch = getSelectedBranch();
    if(branch){
		jQuery.ajax({
	  		type: "GET",
	  		url: "/comercios/branch/info/",
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
	  		data: 'branch_id='+branch,
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		clearBranchForm();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#branch-form-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			for(key in data[0]){
		  				$('#form-branches').find(':input').each(function() {
		  					if($(this).attr('id'))
								if ($(this).attr('id').indexOf(key) >= 0)
									if(data[0][key])
										$(this).val(data[0][key])
						});
		  			}
		  			$('#branch-formtitle').html(data[0]['name']);
		  			$("#sel-branch_mall").trigger("liszt:updated");
		  		}
		  		$('#branch-formcontainer').show();
		  		$('#branch-info').hide();
				$('#branch-form-errors').html('').hide();
		  	}
		});
	}
	else{
		$('#msg-branch-block').show('slow');
		setTimeout("$('#msg-branch-block').hide('slow');", 4000);
	}
}


function noSelectionMessage(){
	$('#msg-branch-block').show('slow');
	setTimeout("$('#msg-branch-block').hide('slow');", 4000);
}

function confirmDeleteBranch(){
	if(getSelectedBranch())
		$('#modal-deleteConfirm').modal('show');
	else noSelectionMessage();
	
}

function deleteBranch(){
	branchAction('delete','La sucursal se borr&oacute; correctamente.');
}

function confirmInactivateBranch(){
	if(getSelectedBranch())
		$('#modal-invalidateConfirm').modal('show');
	else
		noSelectionMessage();
}

function invalidateBranch(){
	$('#modal-invalidateConfirm').modal('hide');
	branchAction('invalidate','La sucursal se inactiv&oacute; correctamente.');
}

function validateBranch(){
	if(getSelectedBranch())
		branchAction('validate','La sucursal se activ&oacute; correctamente.');
	else noSelectionMessage();
}

function branchAction(action,message){
	$('#modal-deleteConfirm').modal('hide');
	var branch = getSelectedBranch();
    if(branch){
		jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/branch/"+action+"/",
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
	  		data: 'branch_id='+branch,
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		clearBranchForm();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#msg-branch-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			$('#msg-branch-success').html(message).show();
		  			setTimeout("$('#msg-branch-success').hide('slow');", 4000);
		  			branchesTable.fnReloadAjax(null,reloadBranchTableCallback);
		  			setBranchOptions();
		  			cancelBranchInfo();
		  		}
		  		
		  	}
		});
	}
}



function setBranchOptions(){
	jQuery.ajax({
  		type: "GET",
  		url: "/comercios/branch/list/",
		beforeSend: function(){
	  		$('button').attr('disabled',true);
  		},
	  	success: function(json){
	  		$('button').attr('disabled',false);
	  		
	  		var data = eval(json);
	  		var html ='';
	  		if (data[0].error == false){
	  			for(key in data[0]['branches']){
	  				html+="<option value='"+data[0]['branches'][key].id+"'>"+data[0]['branches'][key].name+"</option>";
	  			}
	  		}
	  		$("#sel-branch").html(html);
  		 	$("#sel-branch").trigger("liszt:updated");
	  	}
	});
}