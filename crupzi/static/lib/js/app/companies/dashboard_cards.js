/*
	Info cards from business dashboard.
*/

function newCard(){
	$('#h3-card').html('Nueva sucursal');
	$('#cards-formcontainer').show();
	$('#cards-info').hide();
}

$('#btn-card-cancel').click(function(){
	//clearform
	cancelCardsInfo();
});

function clearCardsForm(){
	$('#form-cards').find(':input').each(function() {
		$(this).val('');
	});
	$('#cards-formtitle').html('Nueva sucursal');
}

function cancelCardsInfo(){
	$('#cards-formcontainer').hide();
	$('#cards-info').show();
	$('#cards-form-errors').html('').hide();
	$('#msg-cards-errors').html('').hide();
	clearCardsForm();
}

function getFormCardsAction(){
	var action='create';
	if($('#txt-card_id').val()!='')
		action = 'edit';
	return action;
}

$('#btn-card-save').click(function(){
	if(validateCardsForm()){
		action = getFormCardsAction();
		var formData = $('#form-cards').serialize();
		jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/cards/"+action+"/",
	  		data: formData,
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#cards-form-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			$('#msg-cards-errors-success').html('La informaci&oacute;n se guard&oacute; correctamente.').show();
		  			setTimeout("$('#msg-cards-success').hide('slow');", 4000);
		  			cardsTable.fnReloadAjax(null,reloadCardsTableCallback);
		  			setCardsOptions();
		  			cancelCardsInfo();
		  		}
		  	}
		});
	}
});

function reloadCardsTableCallback(){
	$("#table-cards tbody tr").click( function( e ) {
	    if ( $(this).hasClass('row_selected') ) {
	        $(this).removeClass('row_selected');
	        $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').addClass('button-inactive');
	    }
	    else {
	        cardsTable.$('tr.row_selected').removeClass('row_selected');
	        $(this).addClass('row_selected');
	        $('#btn-edit,#btn-delete,#btn-activate,#btn-inactivate').removeClass('button-inactive');
	        var anSelected = fnGetSelected( cardsTable );
            var is_valid = cardsTable.fnGetData(anSelected[0],2); //5: is_valid column (boolean)
            if(is_valid){
            	$('#btn-inactivate').show();
            	$('#btn-activate').hide();
            }
            else{
            	$('#btn-inactivate').hide();
            	$('#btn-activate').show();	
            }
	    }
	});
}

function validateCardsForm(){
	var errors='';
	if(jQuery.trim($('#txt-card_name').val())==''){
		errors += '<li>El nombre es requerido.</li>';
	}
	if(errors.length!=0){
		$('#cards-form-errors').html(errors).show();
		return false;
	}
	else
		return true;
}

function getSelectedCard(){
	var card, anSelected = fnGetSelected( cardsTable );
    if ( anSelected.length !== 0 )
    	 card = cardsTable.fnGetData(anSelected[0],0);
    return card;
}

function editCard(){
	var card = getSelectedCard();
    if(card){
		jQuery.ajax({
	  		type: "GET",
	  		url: "/comercios/cards/info/",
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
	  		data: 'card_id='+card,
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		clearCardsForm();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#cards-form-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			for(key in data[0]){
		  				$('#form-cards').find(':input').each(function() {
		  					if($(this).attr('id'))
								if ($(this).attr('id').indexOf(key) >= 0)
									if(data[0][key])
										$(this).val(data[0][key])
						});
		  			}
		  			$('#cards-formtitle').html(data[0]['name']);
		  		}
		  		$('#cards-formcontainer').show();
		  		$('#cards-info').hide();
				$('#cards-form-errors').html('').hide();
		  	}
		});
	}
	else{
		$('#msg-cards-block').show('slow');
		setTimeout("$('#msg-cards-block').hide('slow');", 4000);
	}
}


function noSelectionMessage(){
	$('#msg-cards-block').show('slow');
	setTimeout("$('#msg-cards-block').hide('slow');", 4000);
}

function confirmDeleteCard(){
	if(getSelectedCard())
		$('#modal-deleteConfirm').modal('show');
	else noSelectionMessage();
	
}

function deleteCard(){
	cardAction('delete','La tarjeta se borr&oacute; correctamente.');
}

function confirmInactivateCard(){
	if(getSelectedCard())
		$('#modal-invalidateConfirm').modal('show');
	else
		noSelectionMessage();
}

function invalidateCard(){
	$('#modal-invalidateConfirm').modal('hide');
	cardAction('invalidate','La tarjeta se inactiv&oacute; correctamente.');
}

function validateCard(){
	if(getSelectedCard())
		cardAction('validate','La tarjeta se activ&oacute; correctamente.');
	else noSelectionMessage();
}

function cardAction(action,message){
	$('#modal-deleteConfirm').modal('hide');
	var card = getSelectedCard();
    if(card){
		jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/cards/"+action+"/",
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
	  		data: 'card_id='+card,
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		clearCardsForm();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#msg-cards-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			$('#msg-cards-success').html(message).show();
		  			setTimeout("$('#msg-cards-success').hide('slow');", 4000);
		  			cardsTable.fnReloadAjax(null,reloadCardsTableCallback);
		  			setCardsOptions();
		  			cancelCardsInfo();
		  		}
		  		
		  	}
		});
	}
}



function setCardsOptions(){
	jQuery.ajax({
  		type: "GET",
  		url: "/comercios/cards/list/",
		beforeSend: function(){
	  		$('button').attr('disabled',true);
  		},
	  	success: function(json){
	  		$('button').attr('disabled',false);
	  		
	  		var data = eval(json);
	  		var html ='';
	  		if (data[0].error == false){
	  			for(key in data[0]['branches']){
	  				html+="<option value='"+data[0]['branches'][key].id+"'>"+data[0]['branches'][key].name+"</option>";
	  			}
	  		}
	  		$("#sel-branch").html(html);
  		 	$("#sel-branch").trigger("liszt:updated");
	  	}
	});
}