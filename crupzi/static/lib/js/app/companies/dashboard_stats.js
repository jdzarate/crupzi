$('#select_dealchose').uniform();
$('#select_chart_div').uniform();

/*$('a[data-toggle="tab"]', '#ul-deal-stats').on('shown', function (e) {
	var target = e.target.toString();
	var tab_arr = target.substr(target.indexOf('#')+1).split('-');
	selectedDealStats = tab_arr[1];
	drawDealCharts();
});*/

$('#select_dealchose').change(function(){
	var value = $(this).val();
	$('[name^="stats_deal-"]').hide();
	if(value=='0'){
		$('#stats_dealall').show();
	}
	else{
		$('#stats_dealall').hide();
		selectedDealStats = $(this).val();
		if($.inArray(selectedDealStats, loadedDealStats)==-1)
			drawDealCharts();	
		else
			$('[name="stats_deal-'+selectedDealStats+'"]').show();
	}
	
});

$('#btn_changestats').click(function(){
	var weekdate = $('#txt_weekpick').val();
	$('#stats_date').val(weekdate);
	$('#form_changestats').submit();
});

/*$('#txt_weekpick').datePicker({
	selectWeek:true,
	closeOnSelect:false,
	startDate: '01/10/2012'
});*/

$('#btn_lastweek').click(function(){
	$('#stats_date').val(lastWeekDay);
	$('#form_changestats').submit();
});

$('#btn_nextweek').click(function(){
	$('#stats_date').val(nextWeekDay);
	$('#form_changestats').submit();
});


var largechartwidth, smallchartwidth, generalChartsData;
var titleFontSize = 20, titleFontColor='#646464', titleFontName='Open Sans';

//              lightblue, mediumblue, darkblue,   lilac,     plum,    yellow,    orange,     cyan,   green,    red
//var palette = ['#aad7ea', '#67a0be', '#50769b', '#6C5E6D', '#444444','#F0C22E', '#FF8330','#39C5AA','#63A822', '#DB3535'];

var palette = ['#67a0be','#63A822','#FF8330','#6C5E6D','#39C5AA'];
setStatsWidth();

function drawCharts(){	
	jQuery.ajax({
  		type: "GET",
  		url: "/comercios/dashboard/stats/general",
  		data:  { stats_date: $('#txt_weekpick').val()},
		beforeSend: function(){
	  		$('button').attr('disabled',true);
	  		$('#loader').show();
  		},
	  	success: function(json){
	  		$('button').attr('disabled',false);
	  		$('#loader').hide();
	  		var data = eval(json);
	  		generalChartsData = data.slice(0);
		  	if (data[0].error == true) return;
		  	else{

		  		if (data[0].nodata== true){
		  			$('#message-nodata').show();
		  			$('#dashboard_chart_div,#stats_lastweek').hide();
		  			return;
		  		}
		  		var plusimg = '/static/images/icons/dashboard/cloud-plus.gif';
		  		var minusimg = '/static/images/icons/dashboard/cloud-minus.gif';
		  		var evenimg = '/static/images/icons/dashboard/cloud-0.gif';

		  		var diffprints = data[0].diffprints;
		  		var diffsaves = data[0].diffsaves;
		  		var diffclicks = data[0].diffclicks;

		  		$('#diff_prints_value').html((diffprints<0?' ':(diffprints>0?'+':' '))+diffprints);
		  		$('#diff_clicks_value').html((diffclicks<0?' ':(diffclicks>0?'+':' '))+diffclicks);
		  		$('#diff_saves_value').html((diffsaves<0?' ':(diffsaves>0?'+':' '))+diffsaves);

		  		$('#diff-prints-sign').html((data[0].gral_prints_percent<0?' ':(data[0].gral_prints_percent>0?'+':' '))+"&nbsp;&nbsp;" );
//		  		$('#diff-prints-value').val(Math.abs(data[0].gral_prints_percent));

		  		$('#diff-clicks-sign').html((data[0].gral_clicks_percent<0?' ':(data[0].gral_clicks_percent>0?'+':' '))+"&nbsp;&nbsp;" );
//		  		$('#diff-clicks-value').val(Math.abs(data[0].gral_clicks_percent));

		  		$('#diff-saves-sign').html((data[0].gral_saves_percent<0?' ':(data[0].gral_saves_percent>0?'+':' '))+"&nbsp;&nbsp;" );
//		  		$('#diff-saves-value').val(Math.abs(data[0].gral_saves_percent));
		  		
		  		//General

		  		$('#select_chart_div').show();
			  	
		  		drawChartsAux(data);

		  		$("#diff-prints-value").attr('data-max',Math.ceil(data[0].gral_prints_percent/100)*100);
/*
				$("#diff-prints-value").knob({
			        'min':0,
			        'max': Math.ceil(data[0].gral_prints_percent/100)*100,
			        'value': 90,
			        'readOnly': true,
			        'width': 120,
			        'height': 120,
			        'fgColor': '#08C4D3',
			        'dynamicDraw': true,
			        'thickness': 0.1,
			        'tickColorizeValues': true,
					'skin':'tron',
			    });

			    $("#diff-clicks-value").knob({
			        'min':0,
			        'max':Math.ceil(data[0].gral_clicks_percent/100)*100,
			        'readOnly': true,
			        'width': 120,
			        'height': 120,
			        'fgColor': '#85CA55',
			        'dynamicDraw': true,
			        'thickness': 0.1,
			        'tickColorizeValues': true,
					'skin':'tron'
			    });

			    $("#diff-saves-value").knob({
			        'min':0,
			        'max':Math.ceil(data[0].gral_saves_percent/100)*100,
			        'readOnly': true,
			        'width': 120,
			        'height': 120,
			        'fgColor': '#E69A0A',
			        'dynamicDraw': true,
			        'thickness': 0.1,
			        'tickColorizeValues': true,
					'skin':'tron'
			    });*/
		  	}
	  	}
	});
}

google.setOnLoadCallback(drawCharts);


function drawChartsAux(data){
	var data_gral_area = google.visualization.arrayToDataTable(data[0].gral);
	view_data_gral_area = new google.visualization.DataView(data_gral_area);
	options_gral_area = {
		'title':'Impresiones-Clicks',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'backgroundColor': 'none',
		'colors': palette,
		'width': largechartwidth,
		'height': 300,
	}

	chart_gral_area = new google.visualization.AreaChart(document.getElementById('chart_div'));
	//chart_gral_area.draw(data_gral_area, options_gral_area);
	chart_gral_area.draw(view_data_gral_area, options_gral_area);

	$('#select_chart_div').change(function(){
		var value = $(this).val();
		switch(value){
			case '0':
				view_data_gral_area.setColumns([0,1,2,3]);
				break;
			case '1':
				view_data_gral_area.setColumns([0,1]);
				break;
			case '2':
				view_data_gral_area.setColumns([0,2]);
				break;
			case '3':
				view_data_gral_area.setColumns([0,3]);
				break;
		}
		chart_gral_area.draw(view_data_gral_area,options_gral_area);
	});

	//Genero
	var data_gral_gender = google.visualization.arrayToDataTable(data[0].gender);
	var options_gral_gender = {
		'title': 'Género',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'isStacked':true,
		'hAxis':  {format:'#.##%'},
		'backgroundColor': 'none',
		'colors': palette,
		'width': largechartwidth,
		'height': 300,
	}
	var chart_gral_gender = new google.visualization.BarChart(document.getElementById('chart_div_gender'));
	chart_gral_gender.draw(data_gral_gender, options_gral_gender);

	//Ages-clicks
	var data_gral_ages = google.visualization.arrayToDataTable(data[0].ages_clicks);
	var options_gral_ages = {
		'title': 'Clicks por edad',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'pieHole': 0.4,
		'backgroundColor': 'none',
		'colors': palette,
		'sliceVisibilityThreshold':0,
		'width': smallchartwidth,
		'height': 300,
	}
	var chart_gral_ages = new google.visualization.PieChart(document.getElementById('chart_div_ages'));
	chart_gral_ages.draw(data_gral_ages, options_gral_ages);

	//Ages-prints
	var data_gral_ages_prints = google.visualization.arrayToDataTable(data[0].ages_prints);
	var options_gral_ages_prints = {
		'title': 'Impresiones por edad',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'pieHole': 0.4,
		'backgroundColor': 'none',
		'colors': palette,
		'sliceVisibilityThreshold':0,
		'width': smallchartwidth,
		'height': 300,
	}
	var chart_gral_ages_prints = new google.visualization.PieChart(document.getElementById('chart_div_ages_prints'));
	chart_gral_ages_prints.draw(data_gral_ages_prints, options_gral_ages_prints);

	//Location-clicks
	var data_gral_location_clicks = google.visualization.arrayToDataTable(data[0].location_clicks);
	var options_gral_location_clicks = {
		'title': 'Clicks por ubicación',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'pieHole': 0.4,
		'backgroundColor': 'none',
		'colors': palette,
		'width': smallchartwidth,
		'height': 300,
	}
	var chart_gral_location_clicks = new google.visualization.PieChart(document.getElementById('chart_div_location_clicks'));
	chart_gral_location_clicks.draw(data_gral_location_clicks, options_gral_location_clicks);

	//Location-prints
	var data_gral_location_prints = google.visualization.arrayToDataTable(data[0].location_prints);
	var options_gral_location_prints = {
		'title': 'Impresiones por ubicación',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'pieHole': 0.4,
		'backgroundColor': 'none',
		'colors': palette,
		'width': smallchartwidth,
		'height': 300,
	}
	var chart_gral_location_prints = new google.visualization.PieChart(document.getElementById('chart_div_location_prints'));
	chart_gral_location_prints.draw(data_gral_location_prints, options_gral_location_prints);
}

function drawDealCharts(){
	if(selectedDealStats){
		jQuery.ajax({
	  		type: "GET",
	  		url: "/comercios/dashboard/stats/deal",
	  		data: "deal_id="+selectedDealStats+"&stats_date="+$('#txt_weekpick').val(),
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		//General
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		var data = eval(json);
		  		if (data[0].error == true) return;
		  		if (data[0].nodata== true){
		  			return;
		  		}
		  		window['dealStatsData-'+selectedDealStats] = data.slice(0);

		  		drawDealChartsAux(selectedDealStats, data);

				$('[name="stats_deal-'+selectedDealStats+'"]').show();
				loadedDealStats.push(selectedDealStats);
		  	}
		});
	}
}

function drawDealChartsAux(dealId,data){
	var data_gral_area = google.visualization.arrayToDataTable(data[0].gral);
	window['view_data_gral_area-'+dealId] = new google.visualization.DataView(data_gral_area);
	window['options_gral_area-'+dealId] = {
		'title':'Impresiones-Clicks',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'backgroundColor': 'none',
		'width': largechartwidth,
		'height': 300,
		'colors': palette
	}
	window['chart_gral_area-'+dealId] = new google.visualization.AreaChart(document.getElementById('chart_div-'+dealId));
	window['chart_gral_area-'+dealId].draw(data_gral_area, window['options_gral_area-'+dealId]);
	$('#select_chart_div-'+dealId).show().uniform();
	$('#select_chart_div-'+dealId).change(function(){
		var value = $(this).val();
		switch(value){
			case '0':
				window['view_data_gral_area-'+dealId].setColumns([0,1,2,3]);
				break;
			case '1':
				window['view_data_gral_area-'+dealId].setColumns([0,1]);
				break;
			case '2':
				window['view_data_gral_area-'+dealId].setColumns([0,2]);
				break;
			case '3':
				window['view_data_gral_area-'+dealId].setColumns([0,3]);
				break;
		}
		window['chart_gral_area-'+dealId].draw(window['view_data_gral_area-'+dealId],window['options_gral_area-'+dealId]);
	});

	//Genero
	var data_gral_gender = google.visualization.arrayToDataTable(data[0].gender);
	var options_gral_gender = {
		'title': 'Género',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'isStacked':true,
		'backgroundColor': 'none',
		'hAxis':  {format:'#.##%'},
		'width': largechartwidth,
		'height': 300,
		'colors': palette
	}
	var chart_gral_gender = new google.visualization.BarChart(document.getElementById('chart_div_gender-'+dealId));
	chart_gral_gender.draw(data_gral_gender, options_gral_gender);

	//Ages-clicks
	var data_gral_ages = google.visualization.arrayToDataTable(data[0].ages_clicks);
	var options_gral_ages = {
		'title': 'Clicks por edad',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'backgroundColor': 'none',
		'pieHole': 0.4,
		'width': smallchartwidth,
		'height': 300,
		'colors': palette

	}
	var chart_gral_ages = new google.visualization.PieChart(document.getElementById('chart_div_ages-'+dealId));
	chart_gral_ages.draw(data_gral_ages, options_gral_ages);

	//Ages-prints
	var data_gral_ages_prints = google.visualization.arrayToDataTable(data[0].ages_prints);
	var options_gral_ages_prints = {
		'title': 'Impresiones por edad',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'backgroundColor': 'none',
		'pieHole': 0.4,
		'width': smallchartwidth,
		'height': 300,
		'colors': palette
	}
	var chart_gral_ages_prints = new google.visualization.PieChart(document.getElementById('chart_div_ages_prints-'+dealId));
	chart_gral_ages_prints.draw(data_gral_ages_prints, options_gral_ages_prints);

	//Location-clicks
	var data_gral_location_clicks = google.visualization.arrayToDataTable(data[0].location_clicks);
	var options_gral_location_clicks = {
		'title': 'Clicks por ubicación',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'backgroundColor': 'none',
		'pieHole': 0.4,
		'width': smallchartwidth,
		'height': 300,
		'colors': palette
	}
	var chart_gral_location_clicks = new google.visualization.PieChart(document.getElementById('chart_div_location_clicks-'+dealId));
	chart_gral_location_clicks.draw(data_gral_location_clicks, options_gral_location_clicks);

	//Location-prints
	var data_gral_location_prints = google.visualization.arrayToDataTable(data[0].location_prints);
	var options_gral_location_prints = {
		'title': 'Impresiones por ubicación',
		'titleTextStyle': {fontSize: titleFontSize, color: titleFontColor, fontName: titleFontName},
		'backgroundColor': 'none',
		'pieHole': 0.4,
		'width': smallchartwidth,
		'height': 300,
		'colors': palette
	}
	var chart_gral_location_prints = new google.visualization.PieChart(document.getElementById('chart_div_location_prints-'+dealId));
	chart_gral_location_prints.draw(data_gral_location_prints, options_gral_location_prints);
}

var statsResize = function() {
	 //Resize general stats and loaded deals.
	 setStatsWidth();
	 drawChartsAux(generalChartsData);
	 resizeLoadedDealStats();
}

function resizeLoadedDealStats(){
	$.each(loadedDealStats, function(index, value) { 
  		drawDealChartsAux(value, window['dealStatsData-'+value]);
	});
}

function setStatsWidth(){
	largechartwidth=Math.floor($('#dashboard_chart_div').width()*.95);
	smallchartwidth=Math.floor($('#dashboard_chart_div').width()*.95/2);
	smallchartwidth=largechartwidth<500?largechartwidth:smallchartwidth;
}


$(window).resize(function() {
	clearTimeout(window.refresh_size);
	window.refresh_size = setTimeout(function() { statsResize();}, 250);
});

