/*
	Info tab from business dashboard.
*/

/*Password stuff*/

var pwdLevel;
$('#txt-businesspwd').password_strength({
		'onCheck': function(level){
			pwdLevel = level;	
		}
	});

function info_changePwd(){
	$('#info-container').hide();
	$('#info-form-password').show();
	$('#info-pwd-errors').html('').hide();
}

$('#btn-info_changepwd').click(function(){
	$('#modal-passwordConfirm').modal('show');
});

function changePassword(){
	if(validatePasswords($('#txt-businesspwd').val())){
		var formData = $('#form-changepwd').serialize();
		jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/data/password/",
	  		data: formData,
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		$('#loader').hide();
		  		$('#modal-passwordConfirm').modal('hide');
		  		$('button').attr('disabled',false);
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#msg-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			$('#info-pwd-msg').html('La contrase&ntilde;a se cambi&oacute; correctamente.').show();
		  			setTimeout("$('#info-pwd-msg').hide('slow');", 4000);
		  			cancelChangePassword();
		  		}
		  	}
		});
	}
}

function validatePasswords(pwd1){
	var errors='';
	if(jQuery.trim(pwd1)=='')
		errors+='<li>La contrase&ntilde;a no puede quedar vac&iacute;a.</li>';
	else
		if(pwdLevel<3)
			errors+='<li>Favor introducir una contrase&ntilde;a m&aacute;s fuerte.</li>';
	if(errors.length!=0){
		$('#modal-passwordConfirm').modal('hide');
		$('#info-pwd-errors').html(errors).show();
		return false;
	}
	else
		return true;
}

$('#btn-info_cancelchangepwd').click(function(){
	cancelChangePassword();
});

function cancelChangePassword(){
	$('#info-form-password').hide();
	$('#info-container').show();
	clearChangePwdForm();
}

function clearChangePwdForm(){
	$('#form-changepwd').find(':input').each(function() {
		$(this).val('');
	});
	$('.password_strength').attr('class','password_bar').html('');
	$("#info-pwd-errors").html('').hide();
}


/*Info stuff*/

function info_changeInfo(){
	$('#info-container').hide();
	jQuery.ajax({
  		type: "GET",
  		url: "/comercios/data/info/",
		beforeSend: function(){
	  		$('button').attr('disabled',true);
	  		$('#loader').show();
  		},
	  	success: function(json){
	  		$('#loader').hide();
	  		$('button').attr('disabled',false);
	  		var data = eval(json);
	  		if (data[0].error == true){
	  			var html = '';
	  			var errors = data[0].errors;
	  			for (key in errors){
	  				html+="<li>"+errors[key]+"</li>";
	  				$('#msg-errors').html(html).show();
	  			}
	  		}
	  		else{
	  			for(key in data[0]){
	  				$('#form-changeinfo').find(':input').each(function() {
						if ($(this).attr('id').indexOf(key) >= 0)
							if(data[0][key])
								$(this).val(data[0][key])
					});
	  			}
	  		}
	  		$('#info-form-container').show();
			$('#info-form-errors').html('').hide();
	  	}
	});
}

$('#btn-info_cancelchangeinfo').click(function(){
	cancelChangeInfo();
});

function cancelChangeInfo(){
	$('#info-form-container').hide();
	$('#info-container').show();
	clearChangeInfoForm();
}

function clearChangeInfoForm(){
	$('#form-changeinfo').find(':input').each(function() {
		$(this).val('');
	});
}

$('#btn-info_changeinfo').click(function(){
	if(validateInfoForm()){
		var formData = $('#form-changeinfo').serialize();
		jQuery.ajax({
	  		type: "POST",
	  		url: "/comercios/data/info/",
	  		data: formData,
			beforeSend: function(){
		  		$('button').attr('disabled',true);
		  		$('#loader').show();
	  		},
		  	success: function(json){
		  		$('button').attr('disabled',false);
		  		$('#loader').hide();
		  		var data = eval(json);
		  		if (data[0].error == true){
		  			var html = '';
		  			var errors = data[0].errors;
		  			for (key in errors){
		  				html+="<li>"+errors[key]+"</li>";
		  				$('#msg-errors').html(html).show();
		  			}
		  		}
		  		else{
		  			$('#msg-info-success').html('La informaci&oacute;n se actualiz&oacute; correctamente.').show();
		  			setTimeout("$('#msg-info-success').hide('slow');", 4000);
		  			cancelChangeInfo();
		  		}
		  	}
		});
	}
});

function validateInfoForm(){
	var errors='';
	if(jQuery.trim($('#txt-business_name').val())==''){
		errors += '<li>El nombre es requerido.</li>';
	}
	if(errors.length!=0){
		$('#info-form-errors').html(errors).show();
		return false;
	}
	else
		return true;
}


$('#btn-changeimg').click(function(){
	$('#form-logo').submit();
})