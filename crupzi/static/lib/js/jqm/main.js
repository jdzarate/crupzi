function validateForm(form){
	var filter = false;
	$(form).find(':input:not(.default)').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                if(jQuery.trim($(this).val())!='')
                	filter=true;
                break;
        }
    });
    return filter;
}
$("#mini-search").click(function(){ $("#div-search-form").slideToggle(); });
$("#search-desc").focus(function(){ $("#search-rest, #search-buttons").show(); });
$("#btn-cancel-search").click(function(){ clearForm($('#form-search')); $("#search-rest, #search-buttons").hide(); });
function clearForm(){
    $("#form-search").find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
                $(this).val('-1');
                break;
            case 'select-one':
                $(this).val('-1');
                break;
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
    $('#search-company').select2('data', null); $('#search-category').select2('data', null); $('#search-resource').select2('data', null); $('#search-mall').select2('data', null);
}
$('#search-company').select2({allowClear: true, minimumInputLength: 2});
$('#search-category').select2({minimumInputLength: 1});
$('#search-resource').select2({minimumInputLength: 2});
$('#search-mall').select2({allowClear: true, minimumInputLength: 1});
$('.select2-container').each(function (index) {
  $(this).css({ 'min-width': '100%', 'max-width': '100%' });
});
$(".search-field div").removeClass();
$('#btn-search').click(function(){
    if(validateForm($('#form-search'))) location.href = "/busqueda/?query=1&"+$('#form-search').serialize();
});
$('input','#form-search').keypress(function(evt){
    if (evt.which === 13 && $(this).parent().parent().next('.chzn-drop').css('left')!='0px') {
        $('#btn-search').click(); return evt.preventDefault();
    }
});
$('body').on('keydown', '.chzn-search input', function(evt) {
    if (evt.which === 13 && $(this).closest('.chzn-container').find('.chzn-single-with-drop').length === 0) {
        $('#btn-search').click(); return evt.preventDefault();
    }
});
$('body').on('keydown', '.search-field input', function(evt) {
    if (evt.which === 13 && $(this).parent().parent().next('.chzn-drop').css('left')!='0px'){
        $('#btn-search').click(); return evt.preventDefault();
    }
});
function saveDeal(dealId){
    jQuery.ajax({
        type: "GET",
        url: "/deals/save/"+dealId+"/",
        beforeSend: function(){
            $.mobile.loading( 'show' );
        },
        success: function(html){
            $.mobile.loading( 'hide' );
            if(html){
                $('p','#popupError').html(html);
                $( "#popupError" ).popup();
                $( "#popupError" ).show();
                $( "#popupError" ).popup( "open", {'positionTo':'window'} );
                window.setTimeout(function(){$( "#popupError" ).popup( "close" ); $("#popupError").hide(); }, 4000);
            }
            else{
                $( "#popupSuccess" ).popup();
                $( "#popupSuccess" ).show();
                $( "#popupSuccess" ).popup( "open", {'positionTo':'window'} );
                window.setTimeout(function(){$( "#popupSuccess" ).popup( "close" ); $("#popupSuccess").hide(); }, 4000);
            }
        }
    });
}
function removeDeal(dealId){
    jQuery.ajax({
        type: "GET",
        url: "/deals/remove/"+dealId+"/",
        beforeSend: function(){
            $.mobile.loading( 'show' );
        },
        success: function(html){
            $.mobile.loading( 'hide' );
            if(html){
                $('p','#popupError').html(html);
                $( "#popupError" ).popup();
                $( "#popupError" ).show();
                $( "#popupError" ).popup( "open", {'positionTo':'window'} );
                window.setTimeout(function(){$( "#popupError" ).popup( "close" ); $("#popupError").hide(); }, 4000);
            }
            else{
                $( "#popupSuccess" ).popup();
                $( "#popupSuccess" ).show();
                $( "#popupSuccess" ).popup( "open", {'positionTo':'window'} );
                window.setTimeout(function(){$( "#popupSuccess" ).popup( "close" ); $("#popupSuccess").hide(); }, 4000);
                $('[name="deal-'+dealId+'"]').remove();
            }
        }
    });
}
$("#panel-left" ).on( "panelclose", function() { $("#search-rest, #search-buttons").hide(); });
$("#panel-left" ).on( "panelopen", function() { clearForm(); });
function handleClick(e) {
    var target = $(e.target).closest('a');
    if( target ) {
        e.preventDefault(); e.stopPropagation();
        window.location = target.attr('href');
    }
}
$("#btn-cancel").click(function(){ clearForm(); $("#search-rest, #search-buttons").hide(); });