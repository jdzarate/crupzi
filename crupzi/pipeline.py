from django.http import HttpResponseRedirect
from users.models import Profile, Country, City
from social_auth.backends.exceptions import StopPipeline
from PIL import Image
from cropresize import crop_resize

def redirect_to_form(*args, **kwargs):
	try:
		if not kwargs['request'].session.get('saved_username') and kwargs.get('user') is None:
			return HttpResponseRedirect('/completar_registro/')
	except StopPipeline:
		return HttpResponseRedirect('/')

def username(request, *args, **kwargs):
	if kwargs.get('user'):
		username = kwargs['user'].username
	else:
		username = request.session.get('saved_username')
	return {'username': username}
	
def update_details(request, user, is_new=False,*args, **kwargs):
	if is_new and user is not None:
		try:
			first_name = request.session.get('saved_firstname')
			last_name = request.session.get('saved_lastname')
			gender = request.session.get('saved_gender')
			birthday = request.session.get('saved_birthday')
			email = request.session.get('saved_email')
			country = Country.objects.get(pk=request.session.get('saved_country'))
			city = City.objects.get(pk=request.session.get('saved_city'))
			# studies = request.session.get('saved_studies')
			# relationship = request.session.get('saved_relationship')
			setattr(user, 'first_name', first_name)
			setattr(user, 'last_name', last_name)
			setattr(user, 'email', email)
			user.save()
			display = None
			try:
				if user.social_auth.get(provider='twitter'):
					display = user.social_auth.get(provider='twitter').extra_data['profile_image_url']
			except:
				try:
					if user.social_auth.get(provider='facebook'):
						display = "http://graph.facebook.com/%s/picture?" % user.social_auth.get(provider='facebook').uid
				except:
					display =None
			#Create profile.
			profile = Profile(user=user, birthday=birthday, gender=gender, country=country, city=city, newsletter=True, notice=True, display=display)
			profile.save()
		except Exception, e:
			raise e