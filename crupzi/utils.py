# -*- coding: utf-8 -*-
import re
import datetime
import urllib, urllib2
from django.utils import simplejson
from django.template.defaultfilters import slugify

def in_list(item, list):
	try:
		index = list.index(item)
		return True
	except:
		return False


def get_schedule(schedule_string):
	days_array = [m.start() for m in re.finditer('1', schedule_string)]
	day_list = list()
	for day in days_array:
		day_name = {
			0:'lunes',
			1:'martes',
			2:'miércoles',
			3:'jueves',
			4:'viernes',
			5:'sábado',
			6:'domingo'
		}[day]
		day_list.append(day_name)
	if in_list(0,days_array) and in_list(1,days_array) and in_list(2,days_array) and in_list(3,days_array) and in_list(4,days_array):
		list_abv = 'lunes a viernes'
		if in_list(5,days_array):
			list_abv = 'lunes a sábado'
			if in_list(6,days_array):
				list_abv = 'lunes a domingo'
		day_list = list()
		day_list.append(list_abv)
	return day_list

def common_list(listA, listB):
	return list(set([a for a in listA if a  in listB] + [b for b in listB if b in listA]))


def slugify_unique(value, model, slugfield="slug"):
	suffix = 0
	potential = base = slugify(value)
	while True:
		if suffix:
			potential = "-".join([base, str(suffix)])
		if not model.objects.filter(**{slugfield: potential}).count():
			return potential
            # we hit a conflicting slug, so bump the suffix & try again
		suffix += 1


def calculate_age(born):
	today = datetime.date.today()
	try: # raised when birth date is February 29 and the current year is not a leap year
		birthday = born.replace(year=today.year)
	except ValueError:
		birthday = born.replace(year=today.year, day=born.day-1)
	if birthday > today:
		return today.year - born.year - 1
	else:
		return today.year - born.year


def iso_week_number(date):
	try:
		iso = date.isocalendar()[1]
		return iso
	except:
		return -1


def last_day_iso_week(year, week):
	ret = datetime.datetime.strptime('%04d-%02d-0' % (year, week), '%Y-%W-%w')
	if datetime.date(year,1,4).isoweekday()>4:
	 	ret -= datetime.timedelta(days=7)
	return ret.strftime('%d/%m/%Y')

def first_day_iso_week(year,week):
	ret = datetime.datetime.strptime('%04d-%02d-1' % (year, week), '%Y-%W-%w')
	if datetime.date(year,1,4).isoweekday()>4:
	 	ret -= datetime.timedelta(days=7)
	return ret.strftime('%d/%m/%Y')

def iso_weekday_name(isoweekday):
	day_name = {
			1:'lunes',
			2:'martes',
			3:'miércoles',
			4:'jueves',
			5:'viernes',
			6:'sábado',
			7:'domingo'
		}[isoweekday]
	return day_name


'''
json_valid_form
----------------
El metodo valida un formulario y en caso de errores genera un string json con los dichos.
'''
def json_valid_form(form):
	clean = form.is_valid()
	rdict = {'bad':'false'}
	if not clean:
		rdict.update({'bad':'true'})
		d={}
		for e in form.errors.iteritems():
			d.update({e[0]:unicode(e[1])}) # e[0] is the id, unicode(e[1]) is the error HTML.
		rdict.update({'errs':d})
	json = simplejson.dumps(rdict, ensure_ascii=False)
	return json


'''
sendy api
'''
def sendy_api(url, params, apikey=False):
	if apikey:
		params.append(('api_key','bd2c3a534dfe4ea74a71'))
	result = urllib2.urlopen(url, urllib.urlencode(params))
	content = result.read()
	return content

'''
sendy api request
'''
def sendy_api_request(action, email, list_id):
	#validate that the email exists
	content = sendy_api("http://www.crupzi.com/sendy/api/subscribers/subscription-status.php", [('email',email),('list_id',list_id)], True)
	if str(content) != "Email does not exist in list":
		content = sendy_api('http://www.crupzi.com/sendy/'+action, [('email',email),('list',list_id), ('boolean','true')])
	return str(content)

'''
sendy double opt
'''
def sendy_double_opt_subscribe(name, email, list_double_opt_id):
	#check common list
	list_id = 2
	content = sendy_api("http://www.crupzi.com/sendy/api/subscribers/subscription-status.php", [('email',email),('list_id',list_id)], True)
	if content == "Email does not exist in list":
		content = sendy_api("http://www.crupzi.com/sendy/api/subscribers/subscription-status.php", [('email',email),('list_id',list_double_opt_id)], True)
		if content == "Email does not exist in list":
			content = sendy_api('http://www.crupzi.com/sendy/subscribe', [('name',name.encode('utf-8')),('email',email),('FirstName', name.encode('utf-8')),('list',list_double_opt_id), ('boolean','true')])
	return content