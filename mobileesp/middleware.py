import re
from mobileesp import mdetect
from django_mobile import set_flavour
from django_mobile.conf import settings
from companies.models import CompanyUser
from users.models import MobileRequest

class MobileDetectionMiddleware(object):
    """
    Useful middleware to detect if the user is
    on a mobile device.
    """
    def process_request(self, request):
        is_mobile = is_tablet = is_phone=False
        is_authenticated_user = False
        if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
            #is_authenticated_user = (MobileRequest.objects.filter(email=request.user.email, active=True).exists() or request.user.username =='oscaroarevalo')
            is_authenticated_user = True
        user_agent = request.META.get("HTTP_USER_AGENT")
        http_accept = request.META.get("HTTP_ACCEPT")
        if user_agent and http_accept:
            agent = mdetect.UAgentInfo(userAgent=user_agent, httpAccept=http_accept)
            is_tablet = agent.detectTierTablet()
            is_phone = agent.detectTierIphone()
            is_mobile = is_tablet or is_phone or agent.detectMobileQuick()
        request.is_mobile = is_mobile
        request.is_tablet = is_tablet
        request.is_phone = is_phone
        if (is_mobile or is_phone) and not is_tablet and is_authenticated_user: 
            set_flavour(settings.DEFAULT_MOBILE_FLAVOUR, request)
        else:
            set_flavour(settings.FLAVOURS[0], request)
        #set_flavour(settings.DEFAULT_MOBILE_FLAVOUR, request)