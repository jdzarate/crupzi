import os,inspect, sys
from django.core.management import setup_environ
from crupzi import settings
from companies.models import Company

path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))+'/crupzi/static/images/companies/logos_56/'
extensions = list(['png','PNG','jpg','JPG','jpeg','JPEG','gif','GIF'])

companies = Company.objects.all()

for compa in companies:
	for ext in extensions:
		try:
			f = open(path+'56x56_'+str(compa.id)+'.'+ext, 'r')
			compa.logo = str(compa.id)+'.'+ext
			compa.save()
			break
		except Exception, e:
			continue
