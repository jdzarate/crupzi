from django.conf.urls import patterns, include, url
from fakes.views import single_fake_deal

urlpatterns = patterns('',
	url(r'^descuentos/f/(?P<slug>[-\w]+)/', single_fake_deal, name='single_fake_deal'),
)