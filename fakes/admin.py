from django.contrib import admin
from fakes.models import FakeDeal,FakeDealAdmin, FakeResource

admin.site.register(FakeDeal,FakeDealAdmin)
admin.site.register(FakeResource)