import locale
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from fakes.models import FakeDeal,FakeDealResource
from companies.models import Company, Mall, CompanyUser
from resources.models import Resource
from users.models import Profile
from crupzi.utils import get_schedule

def single_fake_deal(request, slug):
	try:
		if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
			profile = Profile.objects.get(user=request.user)
			display = profile.display
			user_profile = True
		else:
			display = None
			user_profile = False
		deal = FakeDeal.objects.get(slug=slug)
		deal_resources = FakeDealResource.objects.filter(deal=deal)
		schedule = get_schedule(deal.schedule)
		schedule_str = ', '.join(schedule)
		malls = Mall.objects.filter(country_id=1).order_by('name')
		companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
		resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
		end_date = None
		if deal.end_date:
			locale.setlocale(locale.LC_TIME,'es_ES')
			end_date = deal.end_date.strftime("%d de %B de %Y")
		return render_to_response('app/deals/fake-deal-page.html',{'deal':deal,'end_date':end_date,
				'schedule': schedule_str,'deal_resources': deal_resources, 
				'malls':malls,'companies':companies, 'resources': resources,
				'user_profile':user_profile,'display':display,}, RequestContext(request))
	except Exception,e:
		print e
		raise Http404