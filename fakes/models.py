# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.contrib import admin
from crupzi.utils import slugify_unique
from companies.models import Category
from users.models import Country

class FakeDeal(models.Model):
	title = models.CharField(max_length=60)
	description = models.CharField(max_length=200)
	schedule = models.CharField(max_length=7)
	company = models.CharField(max_length=100)
	end_date = models.DateTimeField(null=True, blank=True)
	restrictions = models.CharField(max_length=255, null=True, blank=True)
	restrictions2 = models.CharField(max_length=255, null=True, blank=True)
	country = models.ForeignKey(Country)
	slug = models.CharField(max_length=200, null=True, blank=True)
	needs_resource = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	author = models.CharField(max_length=50, null=True, blank=True)
	company_logo = models.ImageField(upload_to=settings.UPLOAD_URL_LOGOS_FAKES, null=True, blank=True)
	category = models.ForeignKey(Category)
	deal_background = models.CharField(max_length=100, null=True, blank=True)
	deal_color = models.CharField(max_length=7, null=True, blank=True)
	deal_opacity  = models.DecimalField(max_digits=3, decimal_places=0, null=True, blank=True)

	def save(self, *args, **kwargs):
		if not self.restrictions:
			self.restrictions = None
		if self.slug is None or self.slug is '':
			slug = slugify_unique(self.company+'-'+self.title, FakeDeal)
			self.slug = slug
		super(FakeDeal,self).save(*args,**kwargs)

	def __unicode__(self):
		return self.title +' ('+self.company+')'


class FakeResource(models.Model):
	name = models.CharField(max_length=50)
	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.name


class FakeDealResource(models.Model):
	deal = models.ForeignKey(FakeDeal)
	resource = models.ForeignKey(FakeResource)


'''---------
	Admin
----------'''
class FakeResourceInline(admin.TabularInline):
    model = FakeDealResource

class FakeDealAdmin(admin.ModelAdmin):
    inlines = [FakeResourceInline]
    list_filter = ('company',)
    search_fields = ['title','description','company']
    def get_form(self, request, obj=None, **kwargs):
    	self.exclude = ['slug']
    	return super(FakeDealAdmin, self).get_form(request, obj, **kwargs)