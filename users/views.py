# Create your views here.
import datetime
import operator
from django.db.models import Q
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson, timezone
from django.core import serializers
from social_auth.utils import setting
from users.forms import CompleteRegistrationForm, ProfileForm
from users.models import City, Country, Profile
from deals.models import Deal, UserDeal
from companies.models import Company, CompanyCategory, Mall, CompanyUser, CompanySubscriber
from resources.models import Resource
from crupzi.utils import get_schedule, json_valid_form
from deals.utils import save_print

def complete_registration(request, backend=None):
	name = setting('SOCIAL_AUTH_PARTIAL_PIPELINE_KEY', 'partial_pipeline')
	details = dict()
	if request.method == 'POST' and request.POST.get('username'):
		backend = request.session[name]['backend']
		form = CompleteRegistrationForm(request.POST)
		if form.is_valid():
			request.session['saved_username'] = form.cleaned_data['username']
			request.session['saved_email'] = form.cleaned_data['email']
			request.session['saved_firstname'] = form.cleaned_data['first_name']
			request.session['saved_lastname'] = form.cleaned_data['last_name']
			request.session['saved_gender'] = form.cleaned_data['gender']
			request.session['saved_birthday'] = form.cleaned_data['birthday']
			request.session['saved_country'] = form.cleaned_data['country']
			request.session['saved_city'] = form.cleaned_data['city']
			# request.session['saved_studies'] = form.cleaned_data['studies']
			# request.session['saved_relationship'] = form.cleaned_data['relationship']
			return redirect('socialauth_complete', backend=backend)
	else:
		if name in request.session:
			details = request.session[name]['kwargs']['details']
		form = CompleteRegistrationForm(initial={'username': details.get('username'), 'email': details.get('email'),'first_name': details.get('first_name'), 'last_name': details.get('last_name')})
			
	return render_to_response('landing/complete_registration.html',{'form':form,}, RequestContext(request))

@csrf_exempt
def validate_registration(request):
	form = CompleteRegistrationForm(request.POST)
	if form.is_valid():
		json = simplejson.dumps({'bad':'false'}, ensure_ascii=False)
	else:
		json = json_valid_form(form)
	return HttpResponse(json, content_type='application/javascript')


@csrf_exempt
def users_get(request, what):
	country_id = request.GET.get('country_id')
	cities = City.objects.filter(country_id=country_id)
	#json = simplejson.dumps(cities)
	json = serializers.serialize("json", cities)
	return HttpResponse(json, content_type='application/javascript')


def user_home(request):
	if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
		countries = Country.objects.values('id','name')
		profile = Profile.objects.values('display','country_id').filter(user=request.user)[0]
		malls = Mall.objects.filter(country_id=1).order_by('name')
		companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
		resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
		return render_to_response('app/user-home.html',{'countries':countries,'user_country': profile.get('country_id'), 'display': profile.get('display'), 
			'malls':malls,'companies':companies, 'resources': resources, 'user_profile': True, 'page':'userhome', 'is_tablet': request.is_tablet}, RequestContext(request))
	else:
		return HttpResponseRedirect('/')


@csrf_exempt
def user_deals(request, category, tab):
	if request.user.is_authenticated():
		paginator_limit = 10
		user_country = request.GET.get('sel_country')
		deals_list = list()
		today_init = timezone.now().strftime("%Y-%m-%d %H:%M")
		today_end = (timezone.now() - datetime.timedelta(hours=6)).strftime("%Y-%m-%d %H:%M")
		next_page = None
		if category!='0':
			category_companies = CompanyCategory.objects.values_list('company_id',flat=True).filter(category_id=category)

		if tab=='1': #timeline.
			extra_where = ['deals_userdeal.deal_id = deals_deal.id'," ((init_date<='"+str(today_init)+"' and end_date>='"+str(today_end)+"') or ('"+str(today_init)+"' >= init_date and end_date is null))"]
			extra_tables = ['deals_deal']
			if category=='0':
				deals = UserDeal.objects.filter(country_id=user_country,user=request.user, deal__company__valid=True).extra(where=extra_where,tables=extra_tables).order_by('-created_at')
			else:
				deals = UserDeal.objects.filter((Q(deal__company_id__in=category_companies) | Q(deal__company_ext_id__in=category_companies)), country_id=user_country, user=request.user,deal__company__valid=True).extra(where=extra_where,tables=extra_tables).order_by('-created_at')
		if tab=='2': #Today
			today_day = datetime.date.today().isoweekday() #monday is 1, sunday is 7.
			extra_where = ['deals_userdeal.deal_id = deals_deal.id', "substring(deals_deal.schedule,"+str(today_day)+",1) = '1'"," ((init_date<='"+str(today_init)+"' and end_date>='"+str(today_end)+"') or ('"+str(today_init)+"' >= init_date and end_date is null))"]
			extra_tables = ['deals_deal']
			if category=='0':
				deals = UserDeal.objects.filter(country_id=user_country,user=request.user,deal__company__valid=True).extra(where=extra_where,tables=extra_tables).order_by('-created_at')
			else:
				deals = UserDeal.objects.filter((Q(deal__company_id__in=category_companies) | Q(deal__company_ext_id__in=category_companies)), country_id=user_country,user=request.user,deal__company__valid=True).extra(where=extra_where,tables=extra_tables).order_by('-created_at')

		if deals:
			#paginator.
			paginator = Paginator(deals,paginator_limit)
			try:
				next_page = int(request.GET.get('page'))
				page_deals = paginator.page(next_page)
				next_page = operator.add(next_page,1) if page_deals.has_next() else False
				count = len(page_deals)
				i = 0
				for user_deal in page_deals:
					try:
						deal = Deal.objects.get(pk=user_deal.deal_id, valid=True)
						set_company = deal.company_ext_id if deal.company_ext_id else deal.company_id
						company = Company.objects.values('name','logo','slug').filter(pk=set_company)[0]
						category = CompanyCategory.objects.values_list('category_id', flat=True).filter(company_id=set_company)
						schedule = get_schedule(deal.schedule)
						schedule_str = ', '.join(schedule)
						deal_next = next_page if i == (count-1) else False
						deals_list.append(
							dict([('id',deal.id),('name',deal.name), ('slug', deal.slug),
								('description',deal.description),('schedule',schedule_str),
								('company',company.get('name')),('category',category),
								('date',user_deal.created_at),('company_logo',company.get('logo')),('company_slug',company.get('slug')),
								('next_page',deal_next)])
						)
						i = i +1
						save_print(request,deal.id, deal.company_id)
					except:
						pass
			except (ValueError, EmptyPage, InvalidPage):
				pass
		return render_to_response('app/deals/deal-card.html',{'deals':deals_list, 'next_page': next_page, 'page':'userhome', 'user_deal':True}, RequestContext(request))
	else:
		return HttpResponseRedirect('/')


def users_account(request, what):
	if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
		companies = list()
		subscriptions = CompanySubscriber.objects.filter(user=request.user, unsubscribed=False)
		for item in subscriptions:
			company_data = Company.objects.get(id=item.company.id)
			categories = CompanyCategory.objects.values_list("category__name",flat=True).filter(company=item.company).order_by('-id')
			companies.append({"id":company_data.id, "name": company_data.name, "logo": company_data.logo, "categories": categories})		
		# [x["id"] for x in companies] list of subscriptions ids.
		nonsub_companies = list()
		non_subscriptions = Company.objects.filter(country=1, valid=True).exclude(id__in=[x["id"] for x in companies]).order_by("?")[:7]
		for item in non_subscriptions:
			categories = CompanyCategory.objects.values_list("category__name",flat=True).filter(company=item).order_by('-id')
			nonsub_companies.append({"id":item.id, "name": item.name, "logo": item.logo, "categories": categories})
		user = request.user
		profile = None
		if what == 'perfil':
			template = 'app/user-profile.html'
			form = None
			if request.method == 'POST':
				form = ProfileForm(request.POST)
				if form.is_valid():
					form.save(request.user, request.FILES)
					profile = Profile.objects.get(user=user)
			else:
				profile = Profile.objects.get(user=user)
				form = ProfileForm(initial={'username': user.username, 'first_name': user.first_name, 'last_name':user.last_name,
											'birthday': profile.birthday.strftime("%d/%m/%Y"), 'gender':profile.gender,
											'country': profile.country_id, 'city': profile.city_id, 'email': user.email, 'newsletter': profile.newsletter,
											'notice': profile.notice})
		return render_to_response(template, {'form':form, 'profile': profile, 'display': profile.display, 'user_profile': True, 'subscriptions': companies, 'non_subscriptions':nonsub_companies}, RequestContext(request))
	else:
		return HttpResponseRedirect('/')


def user_subscriptions(request):
	if request.user.is_authenticated() and not CompanyUser.objects.filter(user=request.user).exists():
		malls = Mall.objects.filter(country_id=1).order_by('name')
		companies = Company.objects.filter(country_id=1, valid=True).order_by('name')
		resources = Resource.objects.filter(country_id=1, valid=True).order_by('entity')
		companies = list()
		subscriptions = CompanySubscriber.objects.filter(user=request.user, unsubscribed=False)
		for item in subscriptions:
			company_data = Company.objects.get(id=item.company.id)
			categories = CompanyCategory.objects.values_list("category__name",flat=True).filter(company=item.company).order_by('-id')
			companies.append({"id":company_data.id, "name": company_data.name, "logo": company_data.logo, "categories": categories})
		nonsub_companies = list()
		non_subscriptions = Company.objects.filter(country=1, valid=True).exclude(id__in=[x["id"] for x in companies]).order_by("?")[:5]
		for item in non_subscriptions:
			categories = CompanyCategory.objects.values_list("category__name",flat=True).filter(company=item).order_by('-id')
			nonsub_companies.append({"id":item.id, "name": item.name, "logo": item.logo, "categories": categories})
		return render_to_response('app/user-subscriptions.html',{'malls':malls,'companies':companies, 'resources': resources, 'user_profile': True, 'page':'usersubscriptions', 'is_tablet': request.is_tablet,'subscriptions': companies,'non_subscriptions':nonsub_companies}, RequestContext(request))
	else:
		return HttpResponseRedirect('/')