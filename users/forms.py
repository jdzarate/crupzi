# -*- coding: utf-8 -*-
import datetime, os, urllib, urllib2
from PIL import Image
from cropresize import crop_resize
from django import forms
from django.forms import ModelForm
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from users.models import Country, City, Profile

'''--------------------
 Complete registration
--------------------'''

countries = list()
countries.append(tuple(('','')))
query_set = Country.objects.values('id','name').filter(id__gte=1)
print query_set
for country in query_set:
	countries.append(tuple((str(country.get('id')), str(country.get('name')))))
cities = list()
query_set = City.objects.values('id','name')
for city in query_set:
	cities.append(tuple((str(city.get('id')), city.get('name').encode('utf-8'))))

class CompleteRegistrationForm(forms.Form):
	GENDER_CHOICES = (('',''),('M','Masculino'),('F','Femenino'))
	STUDIES_CHOICES = (('P','Primaria'),('S','Secundaria'),('U','Universitaria'),('M','Maestría'),('D','Doctorado'))
	RELATIONSHIP_CHOICES = (('S','Soltero'),('R','En una relación'),('C','Casado'))
	countries = list()
	countries.append(tuple(('','')))
	query_set = Country.objects.values('id','name').filter(id__gte=1)
	for country in query_set:
		countries.append(tuple((str(country.get('id')), str(country.get('name')))))
	cities = list()
	query_set = City.objects.values('id','name')
	for city in query_set:
		cities.append(tuple((str(city.get('id')), city.get('name').encode('utf-8'))))
	username = forms.CharField(label='Usuario', required=True, error_messages={'required': 'Ingresa tu nombre de usuario'},widget=forms.TextInput({ "placeholder": "Nombre de usuario" }))
	email = forms.CharField(label='Correo electrónico', required=True, error_messages={'required': 'Ingresa tu correo electrónico'},widget=forms.TextInput({ "placeholder": "usuario@dominio" }))
	first_name = forms.CharField(label='Nombres', required=True, error_messages={'required': 'Ingresa tu nombre'},widget=forms.TextInput({ "placeholder": "Nombres" }))
	last_name = forms.CharField(label='Apellidos', required=True, error_messages={'required': 'Ingresa tus apellidos'},widget=forms.TextInput({ "placeholder": "Apellidos" }))
	birthday = forms.DateField(label='Fecha de Nacimiento', input_formats=["%d/%m/%Y"], required=True, error_messages={'required': 'Ingresa tu fecha de nacimiento'},widget=forms.TextInput({ "placeholder": "dd/mm/AAAA" }))
	gender = forms.ChoiceField(label='Género', choices=GENDER_CHOICES, required=True, error_messages={'required': 'Ingresa tu género'}, widget=forms.Select({'style':'width:295px;'}))
	country = forms.ChoiceField(label='País', choices=countries, required=True, error_messages={'required': 'Ingresa tu país'}, widget=forms.Select({'style':'width:295px;'}))
	city = forms.ChoiceField(label='Departamento', required=True, choices=cities, error_messages={'required': 'Ingresa tu departamento'}, widget=forms.Select({'style':'width:295px;'}))

	def clean_username(self):
		username = self.cleaned_data.get('username', '')
		email = self.cleaned_data.get('email','')
		haystack = ['OSCAR','AREVALO','JOSEDANIEL','ZARATE']
		needle = username.upper()
		if needle in haystack:
			print 'ValidationError'
			raise forms.ValidationError("El nombre de usuario ya ha sido registrado.")
		try:
			user = User.objects.get(username=username)
			if user is not None:
				raise forms.ValidationError("El nombre de usuario ya ha sido registrado.")
		except User.DoesNotExist:
			pass
		return username

	def clean_email(self):
		email = self.cleaned_data.get('email','')
		try:
			user = User.objects.get(email=email)
			if user is not None:
				raise forms.ValidationError("El email ya ha sido registrado")
		except User.DoesNotExist:
			pass
		return email

	def clean_birthday(self):
		birthday = self.cleaned_data.get('birthday')
		try:
			bday = birthday.strftime('%d/%m/%Y')
			datetime.datetime.strptime(bday, '%d/%m/%Y')
			return birthday
		except Exception,e:
			raise forms.ValidationError("La fecha ingresada no es correcta.")

'''--------------------------
 Account (profile and stuff)
--------------------------'''
class ProfileForm(forms.Form):

	#username = forms.CharField(label='Nombre de usuario', required=True)
	first_name = forms.CharField(label='Nombres', required=True, error_messages={'required': 'Ingresa tu nombre'})
	last_name = forms.CharField(label='Apellidos', required=True,error_messages={'required': 'Ingresa tu apellido'})
	#email = forms.EmailField(label='Email', required=True, error_messages={'required':'Ingresa tu email', 'invalid': 'Ingresa un email valido.'})
	birthday = forms.DateField(label='Fecha de Nacimiento', input_formats=["%d/%m/%Y"], required=True, error_messages={'required': 'Ingresa tu fecha de nacimiento', 'invalid':'Ingresa una fecha valida.'})
	gender = forms.ChoiceField(label='Género', choices=(('M','Masculino'),('F','Femenino')))
	country = forms.ChoiceField(label='País', choices=countries, required=True, error_messages={'required': 'Ingresa tu país'})
	city = forms.ChoiceField(label='Departamento', required=True, choices=cities, error_messages={'required': 'Ingresa tu departamento'})
	display = forms.ImageField(label='Display', required=False)
	newsletter = forms.BooleanField(label='Incluirme en lista de correos', required=False)
	notice = forms.BooleanField(label='Incluirme en envío de comunicados', required=False)

	'''
	def clean_username(self):
		username = self.cleaned_data.get('username', '')
		try:
			user = User.objects.get(username=username)
			if user is not None:
				raise forms.ValidationError("El nombre de usuario ya ha sido registrado.")
		except User.DoesNotExist:
			pass
		return username
	'''

	def clean_birthday(self):
		birthday = self.cleaned_data.get('birthday')
		try:
			bday = birthday.strftime('%d/%m/%Y')
			datetime.datetime.strptime(bday, '%d/%m/%Y')
			return birthday
		except Exception,e:
			print e
			raise forms.ValidationError("La fecha ingresada no es correcta.")


	def save(self, user, files):
		username = user.username
		profile = Profile.objects.get(user=user)
		#display uploads.
		display = files.get('display')
		if display is not None:
			path = os.path.join(settings.UPLOAD_URL,display.name)
			destination = open(path,'wb+')
			for chunk in display.chunks():
				destination.write(chunk)
			destination.close()
			try:
				img = Image.open(path)
				resized_img = crop_resize(img,(60,60))
				resized_img.save(os.path.join(settings.UPLOAD_URL,'60x60_'+display.name))
				setattr(profile, 'display','/static/uploads/displays/60x60_'+display.name)
				company.save()
			except Exception,e:
				print e
		#setattr(user, 'username', self.cleaned_data['username']
		setattr(user, 'first_name', self.cleaned_data['first_name'])
		setattr(user, 'last_name', self.cleaned_data['last_name'])
		#setattr(user, 'email', self.cleaned_data['email'])
		setattr(profile, 'birthday', self.cleaned_data['birthday'])
		setattr(profile, 'country_id', self.cleaned_data['country'])
		setattr(profile, 'city_id', self.cleaned_data['city'])
		form_newsletter = self.cleaned_data['newsletter']
		form_notice = self.cleaned_data['notice']
		if profile.newsletter != form_newsletter:
			setattr(profile, 'newsletter', form_newsletter)
			post_data = [('email',user.email),('list','2'), ('boolean','true')]
			action = 'subscribe' if form_newsletter else 'unsubscribe'
			result = urllib2.urlopen('http://www.crupzi.com/sendy/'+action, urllib.urlencode(post_data))
			content = result.read()
		if profile.notice != form_notice:
			setattr(profile, 'notice', form_notice)
			post_data = [('email',user.email),('list','5'), ('boolean','true')]
			action = 'subscribe' if form_notice else 'unsubscribe'
			result = urllib2.urlopen('http://www.crupzi.com/sendy/'+action, urllib.urlencode(post_data))
			content = result.read()
		user.save()
		profile.save()