# -*- coding: utf-8 -*-
import urllib, urllib2, datetime, zlib
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from template_email import TemplateEmail
# Create your models here.

class Country(models.Model):
	name = models.CharField(max_length=100)
	added_at = models.DateTimeField(auto_now_add=True, null=True)
	def __unicode__(self):
		return self.name


class City(models.Model):
	name = models.CharField(max_length=100)
	country = models.ForeignKey(Country)
	added_at = models.DateTimeField(auto_now_add=True, null=True)


class Profile(models.Model):
	GENDER_CHOICES = (('M','Masculino'),('F','Femenino'))
	STUDIES_CHOICES = (('P','Primaria'),('S','Secundaria'),('U','Universitaria'),('M','Maestría'),('D','Doctorado'))
	RELATIONSHIP_CHOICES = (('S','Soltero'),('R','En una relación'),('C','Casado'))
	user = models.ForeignKey(User)
	birthday = models.DateField()
	gender = models.CharField(max_length=1, choices = GENDER_CHOICES)
	country = models.ForeignKey(Country)
	city = models.ForeignKey(City)
	studies = models.CharField(max_length=1,choices = STUDIES_CHOICES, null=True)
	relationship_status = models.CharField(max_length=1,choices = RELATIONSHIP_CHOICES, null=True)
	display = models.CharField(null=True, max_length=150)
	newsletter = models.BooleanField()
	notice = models.BooleanField()


class PasswordRequest(models.Model):
	user = models.ForeignKey(User)
	key = models.CharField(max_length=64)
	STATUS_CHOICES = (('A','Activated'),('P','Pending'),('N','Nullified'))
	status = models.CharField(max_length=1, choices=STATUS_CHOICES)
	password = models.CharField(max_length=128)
	created_at = models.DateTimeField(auto_now_add=True)


class MobileRequest(models.Model):
	email = models.EmailField()
	pub = models.CharField(max_length=100, null=True)
	checksum = models.CharField(max_length=16)
	active = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)


'''-----------
	Signals
------------'''
@receiver(post_save, sender=Profile)
def user_profile_task(sender, instance, created, **kwargs):
	if created:
		try:
			user = instance.user
			#context = {'first_name': user.first_name}
			#email = TemplateEmail(template='mails/new-user.html', context=context, to = [user])
			#email.send()
			#sendy suscribe
			user_name = str((user.first_name+' '+user.last_name).encode('utf-8'))
			user_email = str(user.email.encode('utf-8'))
			#user_birthday = datetime.datetime.combine(instance.birthday, datetime.time(0,0,0))+datetime.timedelta(hours=24)

			for item in [2,5]: #2, 5 are the list numbers
				post_data = [('name', user_name),('email',user_email),('list',str(item)), ('boolean','true'), 
					('FirstName', str(user.first_name.encode('utf-8'))), ('LastName', str(user.last_name.encode('utf-8'))), ('Birthday', instance.birthday),
					('Gender', instance.gender), ('Country', str(instance.country.name.encode('utf-8'))), ('City',str(instance.city.name.encode('utf-8')))]
				if item == 5:
					post_data.append(('Checksum',zlib.adler32(user_email)))
				result = urllib2.urlopen('http://www.crupzi.com/sendy/subscribe', urllib.urlencode(post_data))
				content = result.read()

		except Exception, e:
			print e