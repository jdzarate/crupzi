from django.contrib import admin
from users.models import Country, City

admin.site.register(Country)
admin.site.register(City)