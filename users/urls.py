from django.conf.urls import patterns, include, url
from users.views import complete_registration, users_get, user_home, user_deals, users_account, validate_registration, user_subscriptions

urlpatterns = patterns('',
	url(r'^completar_registro/', complete_registration, name='socialauth_complete_registration'),
	url(r'^validar-registro/', validate_registration, name='validate_registration'),
	url(r'^cuenta/(?P<what>[^/]+)/$', users_account, name='users_account'),
	url(r'^users/get/(?P<what>[^/]+)/$', users_get, name='users_get'),
	url(r'^user-deals/get/(?P<category>\w+)/(?P<tab>\w+)/', user_deals, name='user_deals'),
	url(r'^mis-descuentos/', user_home, name='user_home'),
	url(r'^suscripciones/', user_subscriptions, name='user_subscriptions'),
)