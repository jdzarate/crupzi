from django.db.models import Sum
from stats.models import WeeklyGeneral

def get_datadiff_percent(data0, data1):
	try:
		percent = int(float(data0 - data1) / float(data1) * 100)
		percent = 0 if percent<0 else percent
	except ZeroDivisionError, e:
		percent = 0 if data0==0 else 100
	return percent

def get_weekly_general_data(company, year, isoweek):
	data = WeeklyGeneral.objects.filter(company_id=company, year=year, week=isoweek).values('year','week').annotate(prints=Sum('prints'), clicks=Sum('clicks'), saves=Sum('saves'), anonymous=Sum('anonymous'))
	return data[0]['prints'], data[0]['clicks'], data[0]['saves'], data[0]['anonymous']
	
