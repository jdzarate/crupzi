from django.db import models
from companies.models import Company
from deals.models import Deal
from users.models import City

class WeeklyGeneral(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.IntegerField()
	week = models.IntegerField()
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	anonymous = models.IntegerField()
	deals = models.IntegerField()

class WeeklyGender(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.IntegerField()
	week = models.IntegerField()
	gender = models.CharField(max_length=1)
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	deals = models.IntegerField()

class WeeklyAges(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.IntegerField()
	week = models.IntegerField()
	age = models.IntegerField(max_length=1)
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	deals = models.IntegerField()

class WeeklyLocations(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.IntegerField()
	week = models.IntegerField()
	city = models.ForeignKey(City)
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	deals = models.IntegerField()

#Daily
class DailyGeneral(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	anonymous = models.IntegerField()
	deals = models.IntegerField()

class DailyGender(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	gender = models.CharField(max_length=1)
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	deals = models.IntegerField()

class DailyAges(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	age = models.IntegerField(max_length=1)
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	deals = models.IntegerField()

class DailyLocations(models.Model):
	company = models.ForeignKey(Company)
	deal = models.ForeignKey(Deal)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	weekday = models.SmallIntegerField()
	city = models.ForeignKey(City)
	prints = models.IntegerField()
	clicks = models.IntegerField()
	saves = models.IntegerField()
	deals = models.IntegerField()

class StatsControl(models.Model):
	company = models.ForeignKey(Company)
	year = models.SmallIntegerField()
	week = models.SmallIntegerField()
	created_at = models.DateTimeField()


# class WeeklySocial(models.Model):
# 	company = models.ForeignKey(Company)
# 	deal = models.ForeignKey(Deal)
# 	year = models.IntegerField()
# 	week = models.IntegerField()
# 	city = models.ForeignKey(City)
# 	likes = models.IntegerField()
# 	tweets = models.IntegerField()
